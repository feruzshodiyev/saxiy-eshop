import React from "react";
import {Button, Modal, Form, Input, Radio, Row, Col, Switch, Select} from 'antd';
import MaskedInput from 'antd-mask-input'

const { Option } = Select;
const ShopModalForm = Form.create({name: 'form_in_modal'})(
    // eslint-disable-next-line
    class extends React.Component {



        render() {
            const {visible, onCancel, onCreate, form, confirmLoading, isEditing, editRow} = this.props;
            const {getFieldDecorator} = form;
            return (
                <Modal
                    visible={visible}
                    title={isEditing ? "Дўконни таҳрирлаш" : "Янги дўкон қўшиш"}
                    okText="Сақлаш"
                    cancelText="Бекор қилиш"
                    confirmLoading={confirmLoading}
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <Row gutter={[8, 8]}>

                            {isEditing ?   <Form.Item style={{display: "none"}}>
                                {getFieldDecorator('id', {
                                    initialValue: editRow.id
                                })(<Input/>)}
                            </Form.Item> : ""}

                            <Col span={12}>
                                <Form.Item label="Телефон рақами">
                                    {getFieldDecorator('ownPhoneNumber', {
                                        initialValue: isEditing ? editRow.ownPhoneNumber : '',
                                        rules: [{required: true, message: 'Телефон рақамини киритинг!'}]
                                    })(<MaskedInput
                                        mask="(11) 111 11 11"
                                    />)}
                                </Form.Item>
                            </Col>


                                <Col span={12}>
                                    <Form.Item label="Парол">
                                        {getFieldDecorator('ownPassword', {
                                            rules: [{required:true, message: 'Парол киритинг!'}]
                                        })(<Input/>)}
                                    </Form.Item>
                                </Col>

                        </Row>
                        <Row gutter={[8, 8]}>
                            <Col span={12}>
                                <Form.Item label="Номи">
                                    {getFieldDecorator('name', {
                                        initialValue: isEditing ? editRow.name : '',
                                        rules: [{required: true, message: 'Дўкон номини киритинг!'}],
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label="Эгасининг исми">
                                    {getFieldDecorator('ownFullName', {
                                        initialValue: isEditing ? editRow.ownFullName : '',
                                        rules: [{required: true, message: 'Дўкон эгасини киритинг!'}],
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={[8, 8]}>
                            <Col span={12}>
                                <Form.Item label="merchantId">
                                    {getFieldDecorator('merchantId', {
                                        initialValue: isEditing ? editRow.merchantId : '',
                                        // rules: [{required: true, message: 'merchantId киритинг!'}],
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label="merchantUserId">
                                    {getFieldDecorator('merchantUserId', {
                                        initialValue: isEditing ? editRow.merchantUserId : '',
                                        // rules: [{required: true, message: 'merchantUserId киритинг!'}],
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={[8, 8]}>
                            <Col span={12}>
                                <Form.Item label="secretKey">
                                    {getFieldDecorator('secretKey', {
                                        initialValue: isEditing ? editRow.secretKey : '',
                                        // rules: [{required: true, message: 'secretKey киритинг!'}],
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label="serviceId">
                                    {getFieldDecorator('serviceId', {
                                        initialValue: isEditing ? editRow.serviceId : '',
                                        // rules: [{required: true, message: 'serviceId киритинг!'}],
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={[8, 8]}>
                            <Col span={12}>

                            </Col>
                            <Col span={12}>
                                <Form.Item label="Ҳолати">
                                    {getFieldDecorator('marketActive', {
                                        initialValue: isEditing ? (editRow.marketActive ? "true" : "false") : "true",
                                        rules: [{ required: true}],
                                    })(
                                        <Select
                                            onChange={this.handleSelectChange}
                                        >
                                            <Option value="true">Актив</Option>
                                            <Option value="false">Ўчирилган</Option>
                                        </Select>,
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
            );
        }
    },
);

export default ShopModalForm;