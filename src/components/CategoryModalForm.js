import React, {Component} from 'react';
import {Col, Form, Icon, Input, message, Modal, Row, Select, Upload} from "antd";

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        message.error('Вы можете загрузить только JPG / PNG файл!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Изображение должно быть меньше 2 МБ!');
    }
    return isJpgOrPng && isLt2M;
}

const CategoryModalForm = Form.create({ name: 'form_in_modal' })(
    class  extends Component {
        constructor(props){
            super(props);
            this.state={
                imageUrl: null,
                loading: false,
                fileList: [],
                checked: false,
            }
        }


        clearState=()=>{
            this.setState({
                imageUrl: null,
                loading: false,
                fileList: [],
                checked: false,
            })
        };

        componentDidMount() {


        }


        handlePicChange = info => {
            const file = info.file;

            const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
            const isLt2M = file.size / 1024 / 1024 < 2;
            if (isJpgOrPng && isLt2M) {
                getBase64(file.originFileObj, imageUrl => {
                        this.setState({
                            imageUrl,
                        });
                    }
                );
            }

        };

        normFile = e => {
            console.log('Upload event:', e);
            if (Array.isArray(e)) {
                return e;
            }
            return e && e.fileList;
        };



        render() {
            const {visible, onCancel, onCreate, form, confirmLoading, isEditing, editRow, isSubCategory, parentId, isSub} = this.props;
            const {getFieldDecorator} = form;
            const {imageUrl} = this.state;

            const uploadButton = (
                <div>
                    <Icon type={'picture'}/>
                    <div className="ant-upload-text">Расм юкланг</div>
                </div>
            );

            return (
                <Modal
                    afterClose={this.clearState}
                    destroyOnClose={true}
                    visible={visible}
                    title={isEditing ? "Категорияни таҳрирлаш" : "Янги категория қўшиш"}
                    okText="Сақлаш"
                    cancelText="Бекор қилиш"
                    confirmLoading={confirmLoading}
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <Row gutter={[8, 8]}>

                            {isEditing ?   <Form.Item style={{display: "none"}} >
                                {getFieldDecorator('id', {
                                    initialValue: editRow.id
                                })(<Input/>)}
                            </Form.Item> : ""}

                            <Form.Item style={{display: "none"}} >
                                {getFieldDecorator('parentId', {
                                    initialValue: isSubCategory ? parentId : isEditing ? editRow.parentId : ""
                                })(<Input/>)}
                            </Form.Item>

                            <Col span={12}>
                                <Form.Item label="Номи (русча)">
                                    {getFieldDecorator('nameRu', {
                                        initialValue: isEditing ? editRow.nameRu : '',
                                        rules: [{required: true, message: 'Русча ном киритинг!'}]
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>


                            <Col span={12}>
                                <Form.Item label="Номи (ўзбекча)">
                                    {getFieldDecorator('nameUz', {
                                        initialValue: isEditing ? editRow.nameUz : '',
                                        rules: [{required:true, message: 'Ўзбекча ном киритинг!'}]
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>

                        </Row>
                        <Row gutter={[8, 8]}>
                            <Col span={12}>
                                {/*<Form.Item label="Иконка">*/}
                                {/*    {getFieldDecorator('icon', {*/}
                                {/*        initialValue: isEditing ? editRow.icon : this.state.icon,*/}
                                {/*    })(<Input onChange={event => this.setState({icon: event.target.value, iconEditing: true})}/>)}*/}
                                {/*</Form.Item>*/}
                            </Col>
                            <Col span={12}>

                                <Form.Item className="form-item-upload">
                                    {getFieldDecorator('photo', {
                                        valuePropName: 'photo',
                                        getValueFromEvent: this.normFile,
                                        rules: [{required: !isEditing || !isSub, message: 'Илтимос, расм юкланг!'}]
                                    })(
                                        <Upload
                                            name="avatar"
                                            listType="picture-card"
                                            className="avatar-uploader"
                                            beforeUpload={beforeUpload}
                                            showUploadList={false}
                                            onChange={this.handlePicChange}
                                        >
                                            {imageUrl || isEditing ? <img src={imageUrl ? imageUrl : editRow.icon} alt=""
                                                                          style={{width: '100%'}}/> : uploadButton}

                                        </Upload>
                                    )}

                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
            );
        }
    }
);

export default CategoryModalForm;