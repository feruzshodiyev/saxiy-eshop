import React, {Component} from 'react';
import {Button, Icon, notification, Popconfirm, Row, Table} from "antd";
import axios from "axios";
import {ACCESS_TOKEN, API_BASE_URL} from "../constants";
import CategoryModalForm from "./CategoryModalForm";

const ButtonGroup = Button.Group;

class ExpandTable extends Component {

    constructor(props) {
        super(props);
        this.state={
            data:[],
            editRow: {},
            isEditing: false,
            visible: false,
            confirmLoading: false,
            loading: false
        }
    }


    componentDidMount() {
        this.getSubCategories();
    }

    getSubCategories=()=>{
        this.setState({
            loading: true
        });
        const record = this.props.record;

        axios.get(API_BASE_URL+"category/ap", {
            params: {parentId: record.id},
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res=>{
            console.log(res);
            this.setState({
                data: res.data,
                loading: false
            })
        }).catch(err=>{
            console.log(err);
            this.setState({
                loading: false
            });
        });
    };

    handleCreate = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);
            console.log('token: ', localStorage.getItem(ACCESS_TOKEN));

            this.setState({
                confirmLoading: true
            });

            let formData = new FormData();

            formData.append("nameRu", values.nameRu);
            formData.append("nameUz", values.nameUz);
            formData.append("parentId", values.parentId);
            formData.append("photo", values.photo[values.photo.length-1].originFileObj);

            axios.post(API_BASE_URL + "category", formData, {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
                }
            }).then(res => {

                this.setState({
                    confirmLoading: false,
                    visible: false
                });
                console.log("res: ", res);

                form.resetFields();
                notification.success({
                    message: "Категория қўшиш амалга оширилди!"
                });

                this.getSubCategories()

            }).catch(err => {
                this.setState({
                    confirmLoading: false
                });
                notification.error({
                    message: "Категория қўшишда хатолик юз берди!"
                });
                console.log("err: ", err)
            });


        });
    };

    deleteOneById = (id) => {
        axios.delete(API_BASE_URL + "category/" + id, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            this.getSubCategories();
            notification.success({
                message: "Категорияни ўчириш амалга оширилди!"
            });
        }).catch(err => {
            notification.error({
                message: "Категорияни ўчиришда хатолик юз берди!"
            });
            console.log(err)
        })
    };

    onEditModal = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            this.setState({
                confirmLoading: true
            });

            if (err) {
                this.setState({
                    confirmLoading: false
                });
                return;
            }

            this.handleEdit(values);

        });

        // form.resetFields();
    };

    handleEdit = (values) => {

        const {form} = this.formRef.props;

        console.log('Received values of form: ', values);

        let formData = new FormData();

        formData.append("nameRu", values.nameRu);
        formData.append("nameUz", values.nameUz);

        if (values.parentId){
            formData.append("parentId", values.parentId);
        }
        if (values.photo){
            formData.append("photo", values.photo[values.photo.length-1].originFileObj);
        }

        axios.put(API_BASE_URL + "category/" + values.id, formData, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {

            this.setState({
                confirmLoading: false,
                visible: false
            });
            console.log("res: ", res);
            this.setState({
                editRow: {},
                isEditing: false,
            });
            notification.success({
                message: "Категория таҳрирлаш амалга оширилди!"
            });

            this.getSubCategories();
            form.resetFields();
        }).catch(err => {
            this.setState({
                confirmLoading: false
            });
            notification.error({
                message: "Категория таҳрирлашда хатолик юз берди!"
            });
            console.log("err: ", err)
        });

    };

    showModalForEdit = (row) => {
        this.setState({
            visible: true,
            editRow: row,
            isEditing: true
        });
    };

    handleCancel = () => {

        this.setState({
            editRow: {},
            isEditing: false,
            visible: false
        });

    };

    showModal = () => {
        this.setState({visible: true});
    };

    saveFormRef = formRef => {
        this.formRef = formRef;
    };

    render() {

        const record = this.props.record;

        const columns = [
            { title: 'ID', dataIndex: 'id', key: 'id' },
            { title: ()=><div><p>Номи (русча)</p></div>, dataIndex: 'nameRu', key: 'nameRu'},
            { title: 'Номи (ўзбекча)', dataIndex: 'nameUz', key: 'nameUz' },
            { title: 'Иконка', dataIndex: 'icon', key: 'icon', render: (icon, record) => (<Icon type={icon}/> )  },
            { title: 'Операция',       key: 'id',
                dataIndex: 'id',
                render: (id, record) => (
                    <div>
                        <ButtonGroup>
                            <Button size={"small"} onClick={() => this.showModalForEdit(record)} type="primary" icon="edit"/>
                            <Popconfirm placement="top"
                                        title="Категорияни ўчирасизми?"
                                        onConfirm={() => this.deleteOneById(id)} okText="Ҳа" cancelText="Йўқ">
                                <Button
                                    size={"small"}
                                    type="danger" icon="delete"/>
                            </Popconfirm>
                        </ButtonGroup>
                    </div>) },
        ];
        return (
            <div>
                <Button size={"small"} style={{marginBottom: 5}} onClick={this.showModal}  icon="plus">Янги подкатегория қўшиш</Button>
                <Table
                    loading={this.state.loading}
                    rowKey="id"
                    showHeader={false}
                    size={"small"}
                    columns={columns}
                    expandedRowRender={((record, index, indent, expanded) => <ExpandTable2
                        index={index}
                        indent={indent}
                        record={record}
                    />)}
                    dataSource={this.state.data}
                    pagination={false}/>

                <CategoryModalForm
                    isSub={true}
                    parentId={record.id}
                    isSubCategory={true}
                    isEditing={this.state.isEditing}
                    editRow={this.state.editRow}
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.state.isEditing ? this.onEditModal : this.handleCreate}
                    confirmLoading={this.state.confirmLoading}/>
            </div>
        );
    }
}

export default ExpandTable;



class ExpandTable2 extends Component {

    constructor(props) {
        super(props);
        this.state={
            data:[],
            editRow: {},
            isEditing: false,
            visible: false,
            confirmLoading: false,
            loading: false
        }
    }


    componentDidMount() {
        this.getSubCategories();
    }

    getSubCategories=()=>{

        this.setState({
            loading: true
        });

        const record = this.props.record;

        axios.get(API_BASE_URL+"category/ap", {
            params: {parentId: record.id},
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res=>{
            console.log(res);
            this.setState({
                data: res.data,
                loading: false
            })

        }).catch(err=>{
            console.log(err);
            this.setState({
                loading: false
            });
        });
    };

    handleCreate = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            let formData = new FormData();

            formData.append("nameRu", values.nameRu);
            formData.append("nameUz", values.nameUz);
            formData.append("parentId", values.parentId);
            formData.append("photo", values.photo[values.photo.length-1].originFileObj);

            this.setState({
                confirmLoading: true
            });
            axios.post(API_BASE_URL + "category", formData, {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
                }
            }).then(res => {

                this.setState({
                    confirmLoading: false,
                    visible: false
                });
                console.log("res: ", res);

                form.resetFields();
                notification.success({
                    message: "Категория қўшиш амалга оширилди!"
                });

                this.getSubCategories()

            }).catch(err => {
                this.setState({
                    confirmLoading: false
                });
                notification.error({
                    message: "Категория қўшишда хатолик юз берди!"
                });
                console.log("err: ", err)
            });


        });
    };

    deleteOneById = (id) => {
        axios.delete(API_BASE_URL + "category/" + id, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            this.getSubCategories();
            notification.success({
                message: "Категорияни ўчириш амалга оширилди!"
            });
        }).catch(err => {
            notification.error({
                message: "Категорияни ўчиришда хатолик юз берди!"
            });
            console.log(err)
        })
    };

    onEditModal = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            this.setState({
                confirmLoading: true
            });

            if (err) {
                this.setState({
                    confirmLoading: false
                });
                return;
            }

            this.handleEdit(values);

        });

        // form.resetFields();
    };

    handleEdit = (values) => {

        const {form} = this.formRef.props;

        console.log('Received values of form: ', values);

        let formData = new FormData();

        formData.append("nameRu", values.nameRu);
        formData.append("nameUz", values.nameUz);

        if (values.parentId){
            formData.append("parentId", values.parentId);
        }
        if (values.photo){
            formData.append("photo", values.photo[values.photo.length-1].originFileObj);
        }

        axios.put(API_BASE_URL + "category/" + values.id, formData, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {

            this.setState({
                confirmLoading: false,
                visible: false
            });
            console.log("res: ", res);
            this.setState({
                editRow: {},
                isEditing: false,
            });
            notification.success({
                message: "Категория таҳрирлаш амалга оширилди!"
            });

            this.getSubCategories();
            form.resetFields();
        }).catch(err => {
            this.setState({
                confirmLoading: false
            });
            notification.error({
                message: "Категория таҳрирлашда хатолик юз берди!"
            });
            console.log("err: ", err)
        });

    };

    showModalForEdit = (row) => {
        this.setState({
            visible: true,
            editRow: row,
            isEditing: true
        });
    };

    handleCancel = () => {

        this.setState({
            editRow: {},
            isEditing: false,
            visible: false
        });

    };

    showModal = () => {
        this.setState({visible: true});
    };

    saveFormRef = formRef => {
        this.formRef = formRef;
    };

    render() {

        const record = this.props.record;

        const columns = [
            { title: 'ID', dataIndex: 'id', key: 'id' },
            { title: ()=><div><p>Номи (русча)</p></div>, dataIndex: 'nameRu', key: 'nameRu'},
            { title: 'Номи (ўзбекча)', dataIndex: 'nameUz', key: 'nameUz' },
            { title: 'Иконка', dataIndex: 'icon', key: 'icon', render: (icon, record) => (<Icon type={icon}/> )  },
            { title: 'Операция',       key: 'id',
                dataIndex: 'id',
                render: (id, record) => (
                    <div>
                        <ButtonGroup>
                            <Button size={"small"} onClick={() => this.showModalForEdit(record)} type="primary" icon="edit"/>
                            <Popconfirm placement="top"
                                        title="Категорияни ўчирасизми?"
                                        onConfirm={() => this.deleteOneById(id)} okText="Ҳа" cancelText="Йўқ">
                                <Button
                                    size={"small"}
                                    type="danger" icon="delete"/>
                            </Popconfirm>
                        </ButtonGroup>
                    </div>) },
        ];
        return (
            <div>
                <Button size={"small"} style={{marginBottom: 5}} onClick={this.showModal}  icon="plus">Янги подкатегория қўшиш</Button>
                <Table
                    loading={this.state.loading}
                    rowKey="id"
                    showHeader={false}
                    size={"small"}
                    columns={columns}
                    dataSource={this.state.data}
                    pagination={false}/>

                <CategoryModalForm
                    isSub={true}
                    parentId={record.id}
                    isSubCategory={true}
                    isEditing={this.state.isEditing}
                    editRow={this.state.editRow}
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.state.isEditing ? this.onEditModal : this.handleCreate}
                    confirmLoading={this.state.confirmLoading}/>
            </div>
        );
    }
}