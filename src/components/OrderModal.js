import React, {Component} from 'react';
import {Avatar, Card, Col, Icon, Input, List, Modal, Row} from "antd";
import NumberFormat from "react-number-format";

class OrderModal extends Component {
    render() {
        const {visible, handleClose, products} = this.props;


        return (
            <div>
                <Modal
                    title="Буюртма таркиби"
                    visible={visible}
                    onCancel={handleClose}
                    footer={null}
                >
                    <List
                        dataSource={products}
                        renderItem={item => (
                            <List.Item key={item.id}>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar size="large" shape="square" src={item.product.img}/>
                                    }
                                    title={<div>
                                        <p><span>{item.product.name}</span></p>

                                    </div>}
                                    description={<div>

                                        <Row>
                                            <Col span={8}>
                                                <NumberFormat style={{marginLeft: 10}}
                                                              value={item.product.price}
                                                              displayType={'text'}
                                                              thousandSeparator={' '} suffix={" Сум"}/>
                                            </Col>
                                            <Col span={8}>
                                                <p>{item.amount} dona</p>
                                            </Col>
                                            <Col span={8}>
                                                <p>ID: {item.product.productKey}</p>
                                            </Col>
                                        </Row>

                                    </div>}
                                />

                            </List.Item>
                        )}
                    >
                    </List>
                </Modal>
            </div>
        );
    }
}

export default OrderModal;