import React, {Component} from 'react';
import {Col, Form, Input, Modal, Row} from "antd";

const { TextArea } = Input;

const DeliveryPointModalForm = Form.create({ name: 'form_in_modal' })(
class  extends Component {
    render() {
        const {visible, onCancel, onCreate, form, confirmLoading, isEditing, editRow} = this.props;
        const {getFieldDecorator} = form;
        return (
            <Modal
                visible={visible}
                title={isEditing ? "Етказиш нуқтасини таҳрирлаш" : "Янги Етказиш нуқтаси қўшиш"}
                okText="Сақлаш"
                cancelText="Бекор қилиш"
                confirmLoading={confirmLoading}
                onCancel={onCancel}
                onOk={onCreate}
            >
                <Form layout="vertical">
                    <Row gutter={[8, 8]}>

                        {isEditing ?   <Form.Item style={{display: "none"}}>
                            {getFieldDecorator('id', {
                                initialValue: editRow.id
                            })(<Input/>)}
                        </Form.Item> : ""}

                        <Col span={12}>
                            <Form.Item label="Номи (русча)">
                                {getFieldDecorator('nameRu', {
                                    initialValue: isEditing ? editRow.nameRu : '',
                                    rules: [{required: true, message: 'Русча ном киритинг!'}]
                                })(<Input/>)}
                            </Form.Item>
                        </Col>


                        <Col span={12}>
                            <Form.Item label="Номи (ўзбекча)">
                                {getFieldDecorator('nameUz', {
                                    initialValue: isEditing ? editRow.nameRu : '',
                                    rules: [{required: true, message: 'Ўзбекча ном киритинг!'}]
                                })(<Input/>)}
                            </Form.Item>
                        </Col>

                    </Row>
                    <Row gutter={[8, 8]}>
                        <Col span={12}>
                            <Form.Item label="Масофа (км)">
                                {getFieldDecorator('distance', {
                                    initialValue: isEditing ? editRow.distance : '',
                                    rules: [{required: true, message: 'Масофани киритинг!'}],
                                })(<Input/>)}
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item label="Етказиш нархи">
                                {getFieldDecorator('price', {
                                    initialValue: isEditing ? editRow.price : '',
                                    rules: [{required: true, message: 'Етказиш нархини киритинг!'}],
                                })(<Input/>)}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row >
                        <Col span={24}>
                            <Form.Item label="Хабар (русча)">
                                {getFieldDecorator('deliveryMessageRu', {
                                    initialValue: isEditing ? editRow.deliveryMessageRu : '',
                                    rules: [{required: true, message: 'Русча хабарни киритинг!'}],
                                })(<TextArea allowClear autoSize={{ minRows: 2, maxRows: 6 }}/>)}
                            </Form.Item>
                        </Col>

                    </Row>
                        <Row>
                            <Col span={24}>
                            <Form.Item label="Хабар (ўзбекча)">
                                {getFieldDecorator('deliveryMessageUz', {
                                    initialValue: isEditing ? editRow.deliveryMessageUz : '',
                                    rules: [{required: true, message: 'Ўзбекча хабарни киритинг!'}],
                                })(<TextArea allowClear autoSize={{ minRows: 2, maxRows: 6 }}/>)}
                            </Form.Item>
                            </Col>
                        </Row>
                </Form>
            </Modal>
        );
    }
}
);

export default DeliveryPointModalForm;