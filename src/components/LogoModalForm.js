import React, {Component} from 'react';
import {Col, Form, Modal, Row, Upload, Icon, message, Cascader, Input} from "antd";
import axios from "axios";
import {API_BASE_URL} from "../constants";


import './ComponentsStyles.scss'


function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        message.error('Вы можете загрузить только JPG / PNG файл!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Изображение должно быть меньше 2 МБ!');
    }
    return isJpgOrPng && isLt2M;
}


const LogoModalForm = Form.create({name: 'form_in_modal'})(
    class extends Component {
        constructor(props) {
            super(props);
            this.state = {
                imageUrl: null,
                fileList: [],
                categories: [],
                categoryName: ""
            }
        }

        componentDidMount() {

        }



        clearState=()=>{
            this.setState({
                imageUrl: null,
                fileList: [],
                categories: [],
                categoryName: ""
            })
        };

        handlePicChange = info => {
            const file = info.file;

            const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
            const isLt2M = file.size / 1024 / 1024 < 2;
            if (isJpgOrPng && isLt2M) {
                getBase64(file.originFileObj, imageUrl => {
                        this.setState({
                            imageUrl,
                        });
                    }
                );
            }

        };

// Just show the latest item.
        displayRender = (label) => {
            return label[label.length - 1];
        };

        normFile = e => {
            console.log('Upload event:', e);
            if (Array.isArray(e)) {
                return e;
            }
            return e && e.fileList;
        };

        render() {
            const {visible, onCancel, onCreate, form, confirmLoading} = this.props;
            const {getFieldDecorator} = form;
            const {imageUrl} = this.state;

            const uploadButton = (
                <div>
                    <Icon type={'picture'}/>
                    <div className="ant-upload-text">Расм юкланг <br/> 90x50 pixels <br/> ўлчам тавсия этилади</div>
                </div>
            );

            return (
                <Modal
                    visible={visible}
                    title={"Янги лого қўшиш"}
                    okText="Сақлаш"
                    cancelText="Бекор қилиш"
                    confirmLoading={confirmLoading}
                    onCancel={()=>{
                        this.clearState();
                        onCancel()}}
                    onOk={()=>{
                        this.clearState();
                        onCreate()}}
                >
                    <Form className="slider-form" layout="vertical">


                        <Form.Item className="form-item-upload">
                            {getFieldDecorator('photo', {
                                valuePropName: 'photo',
                                getValueFromEvent: this.normFile,
                                rules: [{required: true, message: 'Илтимос, расм юкланг!'}]
                            })(
                                <Upload
                                    name="avatar"
                                    listType="picture-card"
                                    className="slide-uploader"
                                    beforeUpload={beforeUpload}
                                    showUploadList={false}
                                    onChange={this.handlePicChange}
                                >
                                    {imageUrl ? <img src={imageUrl} alt=""
                                                     style={{width: '100%'}}/> : uploadButton}

                                </Upload>
                            )}
                        </Form.Item>

                    </Form>
                </Modal>
            );
        }
    }
);

export default LogoModalForm;