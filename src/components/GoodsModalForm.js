import React, {Component} from 'react';
import {Col, Form, Input, Modal, Row, InputNumber, Upload, Icon, message, Checkbox, Select} from "antd";

import './ComponentsStyles.scss'

const {Option} = Select;
const {TextArea} = Input;

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        message.error('Вы можете загрузить только JPG / PNG файл!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Изображение должно быть меньше 2 МБ!');
    }
    return isJpgOrPng && isLt2M;
}


const GoodsModalForm = Form.create({name: 'form_in_modal'})(
    class extends Component {
        constructor(props) {
            super(props);
            this.state = {
                imageUrl: null,
                loading: false,
                uploadSuccess: false,
                uploadError: false,
                fileList: [],
                checked: false,
            }
        }

        componentDidMount() {
            const {isEditing, editRow} = this.props;
            if (isEditing) {
                this.setState({
                    checked: editRow.discount
                })
            }
        }

        clearState=()=>{
            this.setState({
                imageUrl: null,
                loading: false,
                uploadSuccess: false,
                uploadError: false,
                fileList: [],
                checked: false,
            })
        };


        handlePicChange = info => {
            const file = info.file;

            const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
            const isLt2M = file.size / 1024 / 1024 < 2;
            if (isJpgOrPng && isLt2M) {
                getBase64(file.originFileObj, imageUrl => {
                        this.setState({
                            imageUrl,
                        });
                    }
                );
            }

        };

        onChange = e => {
            console.log('checked = ', e.target.checked);
            this.setState({
                checked: e.target.checked,
            });
        };


        normFile = e => {
            console.log('Upload event:', e);
            if (Array.isArray(e)) {
                return e;
            }
            return e && e.fileList;
        };

        render() {
            const {visible, onCancel, onCreate, form, confirmLoading, isEditing, editRow} = this.props;
            const {getFieldDecorator} = form;
            const {imageUrl} = this.state;

            const uploadButton = (
                <div>
                    <Icon type={'picture'}/>
                    <div className="ant-upload-text">Расм юкланг</div>
                </div>
            );

            return (
                <Modal
                    className={"product-modal-form"}
                    afterClose={this.clearState}
                    visible={visible}
                    title={isEditing ? "Товарни таҳрирлаш" : "Янги товар қўшиш"}
                    okText="Сақлаш"
                    cancelText="Бекор қилиш"
                    confirmLoading={confirmLoading}
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">

                        <Row gutter={[8, 8]}>

                            {isEditing ? <Form.Item style={{display: "none"}}>
                                {getFieldDecorator('id', {
                                    initialValue: editRow.id
                                })(<Input/>)}
                            </Form.Item> : ""}

                            <Col span={12}>
                                <Form.Item label="Номи (русча)">
                                    {getFieldDecorator('nameRu', {
                                        initialValue: isEditing ? editRow.nameRu : '',
                                        rules: [{required: true, message: 'Русча ном киритинг!'}]
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>


                            <Col span={12}>
                                <Form.Item label="Номи (ўзбекча)">
                                    {getFieldDecorator('nameUz', {
                                        initialValue: isEditing ? editRow.nameUz : '',
                                        rules: [{required: true, message: 'Ўзбекча ном киритинг!'}]
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>

                        </Row>
                        <Row>
                            <Col span={12}>
                                <Form.Item style={{marginRight:10}} label="Инфо (ўзбекча)">
                                    {getFieldDecorator('infoUz', {
                                        initialValue: isEditing ? editRow.infoUz : '',
                                        // rules: [{required: true, message: ''}]
                                    })(<TextArea rows={4}/>)}
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label="Инфо (русча)">
                                    {getFieldDecorator('infoRu', {
                                        initialValue: isEditing ? editRow.infoRu : '',
                                        // rules: [{required: true, message: ''}]
                                    })(<TextArea rows={4}/>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row >
                            <Col span={3}>
                                <Form.Item label="Сони">
                                    {getFieldDecorator('amount', {
                                        initialValue: isEditing ? editRow.amount : '1',
                                        rules: [{required: true, message: 'Мажбурий!'}],
                                    })(<InputNumber min={1}/>)}
                                </Form.Item>
                            </Col>
                            <Col span={4}>
                                <Form.Item label="Етказилиш муддати(кун)">
                                    {getFieldDecorator('deliveryDay', {
                                        initialValue: isEditing ? editRow.deliveryDay : '1',
                                        rules: [{required: true, message: 'Мажбурий!'}]
                                    })(<InputNumber min={1}/>)}
                                </Form.Item>
                            </Col>
                            <Col span={4}>
                                <Form.Item label="Етказилиш нархи(км/сўм)">
                                    {getFieldDecorator('deliveryPrice', {
                                        initialValue: isEditing ? editRow.deliveryPrice : '1',
                                        rules: [{required: true, message: 'Мажбурий!'}]
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>
                            <Col span={3} offset={1}>
                                <Form.Item label="Узунлиги">
                                    {getFieldDecorator('length', {
                                        initialValue: isEditing ? editRow.length : '1',
                                        rules: [{required: true, message: 'Мажбурий!'}],
                                    })(<InputNumber min={1}/>)}
                                </Form.Item>
                            </Col>
                            <Col span={3}>
                                <Form.Item label="Баландлиги">
                                    {getFieldDecorator('height', {
                                        initialValue: isEditing ? editRow.height : '1',
                                        rules: [{required: true, message: 'Мажбурий!'}],
                                    })(<InputNumber min={1}/>)}
                                </Form.Item>
                            </Col>
                            <Col span={3}>

                                <Form.Item label="Эни">
                                    {getFieldDecorator('width', {
                                        initialValue: isEditing ? editRow.width : '1',
                                        rules: [{required: true, message: 'Мажбурий!'}]
                                    })(<InputNumber min={1}/>)}
                                </Form.Item>

                            </Col>
                            <Col span={3}>
                                <Form.Item label="Оғирлиги(gr)">
                                    {getFieldDecorator('weight', {
                                        initialValue: isEditing ? editRow.weight : 100,
                                        rules: [{required: true, message: 'Мажбурий!'}]
                                    })(<InputNumber  step={100} min={100}/>)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={[8, 8]}>
                            <Col span={12}>
                                <Row gutter={[8, 8]}>

                                    <Col span={6}>

                                        <Form.Item label="Чегирма">
                                            {getFieldDecorator('discount', {
                                                initialValue: isEditing ? editRow.discount : false,
                                            })(<Checkbox onChange={this.onChange}/>)}
                                        </Form.Item>
                                    </Col>


                                    <Col span={9}>

                                        <Form.Item label="Чегирма нархи">
                                            {getFieldDecorator('discountPrice', {
                                                initialValue: isEditing ? editRow.discountPrice : 0
                                            })(<Input disabled={!this.state.checked}/>)}
                                        </Form.Item>
                                    </Col>

                                    <Col span={9}>
                                        <Form.Item label="Нархи">
                                            {getFieldDecorator('price', {
                                                initialValue: isEditing ? editRow.price : '1',
                                                rules: [{required: true, message: 'Мажбурий!'}],
                                            })(<Input/>)}
                                        </Form.Item>
                                    </Col>

                                </Row>
                                <Row gutter={[8, 8]}>

                                    <Col span={8}>
                                        <Form.Item label="Махсус ID">
                                            {getFieldDecorator('productKey', {
                                                initialValue: isEditing ? editRow.productKey : '',
                                                // rules: [{required: true, message: 'Ўзбекча ном киритинг!'}]
                                            })(<Input/>)}
                                        </Form.Item>
                                    </Col>


                                    <Col span={8}>

                                        <Form.Item label="Статус">
                                            {getFieldDecorator('active', {
                                                initialValue: isEditing ? (editRow.active ? "true" : "false") : "true",
                                                rules: [{required: true}],
                                            })(
                                                <Select

                                                >
                                                    <Option value="true">Актив</Option>
                                                    <Option value="false">Ўчирилган</Option>
                                                </Select>,
                                            )}
                                        </Form.Item>
                                    </Col>

                                    <Col span={8}>

                                        <Form.Item label="Бош саҳифа">
                                            {getFieldDecorator('home', {
                                                initialValue: isEditing ? (editRow.recentlyAdded ? 1 : (editRow.recommended ? 2 : 0)) : 0,
                                                rules: [{required: true}],
                                            })(
                                                <Select

                                                >
                                                    <Option value={0}>Йўқ</Option>
                                                    <Option value={1}>Янги қўшилган</Option>
                                                    <Option value={2}>Тавсия этилган</Option>
                                                </Select>,
                                            )}
                                        </Form.Item>
                                    </Col>

                                </Row>
                            </Col>


                            <Col span={6} offset={3}>
                                <Form.Item label={"Товар расми"} className="form-item-upload">
                                    {getFieldDecorator('photo', {
                                        valuePropName: 'photo',
                                        getValueFromEvent: this.normFile,
                                        rules: [{required: !isEditing, message: 'Илтимос, расм юкланг!'}]
                                    })(
                                        <Upload
                                            name="avatar"
                                            listType="picture-card"
                                            className="avatar-uploader"
                                            beforeUpload={beforeUpload}
                                            showUploadList={false}
                                            onChange={this.handlePicChange}
                                        >
                                            {imageUrl || isEditing ? <img src={imageUrl ? imageUrl : editRow.img} alt=""
                                                                          style={{width: '100%'}}/> : uploadButton}

                                        </Upload>
                                    )}

                                </Form.Item>

                            </Col>

                        </Row>



                    </Form>
                </Modal>
            );
        }
    }
);

export default GoodsModalForm;