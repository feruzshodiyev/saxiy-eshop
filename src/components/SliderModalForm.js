import React, {Component} from 'react';
import {Col, Form, Modal, Row, Upload, Icon, message, Cascader, Input} from "antd";
import axios from "axios";
import {API_BASE_URL} from "../constants";


import './ComponentsStyles.scss'


function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        message.error('Вы можете загрузить только JPG / PNG файл!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Изображение должно быть меньше 2 МБ!');
    }
    return isJpgOrPng && isLt2M;
}


const SliderModalForm = Form.create({name: 'form_in_modal'})(
    class extends Component {
        constructor(props) {
            super(props);
            this.state = {
                imageUrl: null,
                fileList: [],
                categoryName: ""
            }
        }




        clearState=()=>{
            this.setState({
                imageUrl: null,
                fileList: [],
                categoryName: ""
            })
        };

        handlePicChange = info => {
            const file = info.file;

            const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
            const isLt2M = file.size / 1024 / 1024 < 2;
            if (isJpgOrPng && isLt2M) {
                getBase64(file.originFileObj, imageUrl => {
                        this.setState({
                            imageUrl,
                        });
                    }
                );
            }

        };

        onChange = (value, selectedOptions) => {
            console.log('val: ', value[value.length - 1]);
            console.log('selectedOptions: ', selectedOptions[selectedOptions.length - 1]);
            this.setState({
                categoryName: selectedOptions[selectedOptions.length - 1].nameUz
            })
        };

// Just show the latest item.
        displayRender = (label) => {
            return label[label.length - 1];
        };

        normFile = e => {
            console.log('Upload event:', e);
            if (Array.isArray(e)) {
                return e;
            }
            return e && e.fileList;
        };

        render() {
            const {visible, onCancel, onCreate, form, confirmLoading, categories} = this.props;
            const {getFieldDecorator} = form;
            const {imageUrl} = this.state;

            const uploadButton = (
                <div>
                    <Icon type={'picture'}/>
                    <div className="ant-upload-text">Расм юкланг <br/> 600X300 pixels <br/>ўлчам тавсия этилади</div>
                </div>
            );

            return (
                <Modal
                    visible={visible}
                    title={"Янги слайд қўшиш"}
                    okText="Сақлаш"
                    cancelText="Бекор қилиш"
                    confirmLoading={confirmLoading}
                    onCancel={()=>{
                        this.clearState();
                        onCancel()}}
                    onOk={()=>{
                        this.clearState();
                        onCreate()}}
                >
                    <Form className="slider-form" layout="vertical">

                        <Form.Item style={{display: 'none'}}>
                            {getFieldDecorator('categoryName', {
                                initialValue: this.state.categoryName,
                                rules: [{required: true, message: ''}],
                            })(<Input/>)}
                        </Form.Item>

                        <Form.Item label="Категория">
                            {getFieldDecorator('categoryId', {
                                rules: [{required: true, message: 'Илтимос, категорияни танланг!'}]
                            })(
                                <Cascader
                                    notFoundContent={
                                        <Icon type="loading"/>
                                    }
                                    allowClear={false}
                                    fieldNames={{label: 'nameUz', value: 'id', children: 'subCategory'}}
                                    options={categories}
                                    placeholder="Категория танланг"
                                    expandTrigger="hover"
                                    displayRender={this.displayRender}
                                    onChange={this.onChange}
                                />
                            )}
                        </Form.Item>

                        <Form.Item className="form-item-upload">
                            {getFieldDecorator('photo', {
                                valuePropName: 'photo',
                                getValueFromEvent: this.normFile,
                                rules: [{required: true, message: 'Илтимос, расм юкланг!'}]
                            })(
                                <Upload
                                    name="avatar"
                                    listType="picture-card"
                                    className="slide-uploader"
                                    beforeUpload={beforeUpload}
                                    showUploadList={false}
                                    onChange={this.handlePicChange}
                                >
                                    {imageUrl ? <img src={imageUrl} alt=""
                                                                  style={{width: '100%'}}/> : uploadButton}

                                </Upload>
                            )}
                        </Form.Item>

                    </Form>
                </Modal>
            );
        }
    }
);

export default SliderModalForm;