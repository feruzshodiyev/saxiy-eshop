import React, {Component} from 'react';
import {Col, Form, Input, Modal, Row} from "antd";


const DeliveryPointModalForm = Form.create({ name: 'form_in_modal' })(
    class  extends Component {
        render() {
            const {visible, onCancel, onCreate, form, confirmLoading, isEditing, editRow} = this.props;
            const {getFieldDecorator} = form;
            return (
                <Modal
                    visible={visible}
                    title={isEditing ? "Курьер маълумотини таҳрирлаш" : "Янги курьер қўшиш"}
                    okText="Сақлаш"
                    cancelText="Бекор қилиш"
                    confirmLoading={confirmLoading}
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">

                            {isEditing ?   <Form.Item style={{display: "none"}}>
                                {getFieldDecorator('id', {
                                    initialValue: editRow.id
                                })(<Input/>)}
                            </Form.Item> : ""}

                        <Row >
                            <Col span={24}>
                                <Form.Item label="Исми">
                                    {getFieldDecorator('name', {
                                        initialValue: isEditing ? editRow.name : '',
                                        rules: [{required: true, message: 'Курьер исмини киритинг!'}],
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>

                        </Row>
                        <Row>
                            <Col span={24}>
                                <Form.Item label="Телефон рақами">
                                    {getFieldDecorator('phoneNumber', {
                                        initialValue: isEditing ? editRow.phoneNumber : '',
                                        rules: [{required: true, message: 'Телефон рақамини киритинг!'}],
                                    })(<Input/>)}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
            );
        }
    }
);

export default DeliveryPointModalForm;