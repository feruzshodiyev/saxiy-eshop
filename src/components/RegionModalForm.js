import React from "react";

import { Button, Modal, Form, Input, Radio } from 'antd';

import ShopModalForm from "./ShopModalForm";

const RegionModalForm = Form.create({ name: 'form_in_modal' })(
    // eslint-disable-next-line
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form } = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    visible={visible}
                    title="Вилоят қўшиш"
                    okText="Сақлаш"
                    cancelText="Бекор қилиш"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <Form.Item label="Номи (русча)">
                            {getFieldDecorator('nameRu', {
                                rules: [{ required: true, message: 'Русча ном киритинг!' }],
                            })(<Input />)}
                        </Form.Item>
                        <Form.Item label="Номи (ўзбекча)">
                            {getFieldDecorator('nameUz',{
                                rules: [{ required: true, message: 'Ўзбекча ном киритинг!' }],
                            })(<Input />)}
                        </Form.Item>
                    </Form>
                </Modal>
            );
        }
    },
);

export default RegionModalForm;
