import React, {Component} from 'react';
import {Button, notification, Form, Icon, Input, Layout, Result, Col, Row, Select} from "antd";
import MaskedInput from "antd-mask-input";
import axios from 'axios';
import "./SignUp.scss";
import {API_BASE_URL, LANGUAGE} from "../../constants";
import {Link} from "react-router-dom";

const {Content} = Layout;
const {Option} = Select;
const lang = localStorage.getItem(LANGUAGE);


class SignUp extends Component {

    state = {
        phoneNumber: '',
        password: '',
        confirmLoading: false,
        confirmLoading2: false,
        secretKey: null,
        showRegForm: true,
        showCodeForm: false,
        showSuccess: false,


    };


    handleRegister = values => {
        this.setState({
            confirmLoading: true
        });

        let data = {
            "fullName": values.fullName,
            "password": values.password,
            "phoneNumber": values.phoneNumber.replace("(", "").replace(")", "").replace(/\s/g, ""),
            "district" : values.district,
            "region": values.region
        };

        axios.post(API_BASE_URL + "user", data, {
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log(res);
            this.setState({
                confirmLoading: false,
                secretKey: res.data.message,
                showRegForm: false,
                showCodeForm: true,
                phoneNumber: values.phoneNumber,
                password: values.password
            });
        }).catch(err => {
            console.log(err);
            this.setState({
                confirmLoading: false,

            });
            const lang = localStorage.getItem(LANGUAGE);

            notification.error({
                message: lang === "ru" ? "Произошла ошибка!" : "Xatolik yuz berdi!"
            })
        })
    };

    handleSubmitCode = (values) => {

        this.setState({
            confirmLoading2: true
        });

        let data = {
            "code": values.code,
            "secretKey": this.state.secretKey
        };


        axios.post(API_BASE_URL + 'user/activate', {}, {
            headers: {
                "Content-Type": "application/json"
            },
            params: {
                "code": values.code,
                "secretKey": this.state.secretKey
            }

        }).then(res => {

            console.log(res);
            if (res.data.success) {
                this.setState({
                    showCodeForm: false,
                    showSuccess: true
                })
            } else {
                notification.error({
                    message: res.data.message
                });
                this.setState({
                    confirmLoading2: false,
                });
            }

        }).catch(err => {
            this.setState({
                confirmLoading2: false,
            });
        })
    };

    handleCancelConfirm=()=>{
      this.setState({
          showRegForm: true,
          showCodeForm: false
      })
    };




    render() {


        return (
            <Layout className={"sign-up-layout"}>
                <Content className="sign-up-content">
                    <h1 className="sign-up-title">{lang === "ru" ? "Регистрация" : "Ro'yxatdan o'tish"}</h1>
                    {this.state.showRegForm ?
                        <WrappedSignUpForm
                            onRegister={(valuse) => this.handleRegister(valuse)}
                            confirmLoading={this.state.confirmLoading}
                        /> : ''}


                    {this.state.showCodeForm ?
                        <WrappedSignCodeForm
                            cancelConfirm={this.handleCancelConfirm}
                            onSubmitCode={(values) => this.handleSubmitCode(values)}
                            phoneNumber={this.state.phoneNumber}
                            confirmLoading={this.state.confirmLoading2}
                        />
                        : ""}

                    {this.state.showSuccess ?
                        <Success/> : ""
                    }


                </Content>
            </Layout>
        );
    }
}

export default SignUp;


class SignUpForm extends React.Component {

    state = {
        confirmDirty: false,

        districts: [],
        regions: [],
        selectedRegionId: null,
        selectedDistrictId: "def",
    };

componentDidMount() {
    this.fetchRegions();
}

    fetchRegions = () => {

        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "region", {
            params: {
                "lang": lang === "ru" ? "ru" : "uz"
            }
        }).then(res => {

            this.setState({
                regions: res.data,
                loading: false
            });

            console.log(res)
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };


    fetchDistricts = () => {
        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "district/ap/dist-id/" + this.state.selectedRegionId, {}).then(res => {
            this.setState({
                districts: res.data,
                loading: false
            });
            console.log(res)
        }).catch(err => {
            this.setState({
                districts: [],
                loading: false
            });
            console.log(err)
        })
    };

    handleChangeRegion = (value) => {
        this.setState({
            selectedRegionId: value,
            // selectedDistrictId: "def"
        }, () => this.fetchDistricts());

    };


    handleChangeDistrict = (value) => {
        this.setState({
            selectedDistrictId: value
        }, () => {

        })
    };


    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log(values);
            if (!err) {
                this.props.onRegister(values)
            }
        });
    };

    handleConfirmBlur = e => {
        const {value} = e.target;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    };

    compareToFirstPassword = (rule, value, callback) => {
        const lang = localStorage.getItem(LANGUAGE);

        const {form} = this.props;
        if (value && value !== form.getFieldValue('password')) {
            callback(lang === "ru" ? "Два пароля, которые вы вводите, несовместимы!" : "Siz kiritgan ikkita parol mos kelmaydi!");
        } else {
            callback();
        }
    };

    validateToNextPassword = (rule, value, callback) => {
        const {form} = this.props;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], {force: true});
        }
        callback();
    };

    render() {
        const lang = localStorage.getItem(LANGUAGE);
        const {getFieldDecorator} = this.props.form;



        return (
            <Form onSubmit={this.handleSubmit} className="sign-up-form">
                <Form.Item label={lang === "ru" ? "Номер телефона" : "Telefon raqami"}>
                    {getFieldDecorator('phoneNumber', {
                        rules: [{
                            required: true,
                            message: lang === "ru" ? "Введите номер телефона!" : "Telefon raqamini kiriting!"
                        }]
                    })(<MaskedInput
                        prefix={<Icon type="mobile" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        mask="(11) 111 11 11"
                    />)}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('fullName', {
                        rules: [{
                            required: true,
                            message: lang === "ru" ? "Пожалуйста, введите ваше имя!" : 'Iltimos, ismingizni kiriting!'
                        }],
                    })(
                        <Input
                            prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            placeholder={lang === "ru" ? "Имя!" : "Ismingiz"}
                        />,
                    )}
                </Form.Item>
                <Form.Item hasFeedback>
                    {getFieldDecorator('password', {
                        rules: [
                            {
                                required: true,
                                message: lang === "ru" ? "Пожалуйста, введите пароль!" : 'Iltimos, parol kiriting!',
                            },
                            {
                                validator: this.validateToNextPassword,
                            },
                        ],
                    })(<Input.Password placeholder={lang === "ru" ? "Пароль" : "Parol"}
                                       prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}/>)}
                </Form.Item>
                <Form.Item hasFeedback>
                    {getFieldDecorator('confirm', {
                        rules: [
                            {
                                required: true,
                                message: lang === "ru" ? "Пожалуйста, подтвердите ваш пароль!" : 'Iltimos, parolingizni tasdiqlang!',
                            },
                            {
                                validator: this.compareToFirstPassword,
                            },
                        ],
                    })(<Input.Password prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                       placeholder={lang === "ru" ? "Подтвердите пароль" : "Parolni tasdiqlash"}
                                       onBlur={this.handleConfirmBlur}/>)}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('region', {
                        rules: [
                            {
                                required: true,
                                message: lang === "ru" ? "Пожалуйста, выберите свой регион!" : 'Iltimos, viloyatingizni tanlang!',
                            },
                        ],
                    })(<Select
                        className={"select-address"}
                        placeholder={lang === "ru" ? "Выберите регион" : "Viloyatingizni tanlang"}
                        onChange={this.handleChangeRegion}
                    >
                        { this.state.regions && this.state.regions.map((item, index) => (
                            <Option value={item.id} key={index}>{item.name}</Option>
                        ))}
                    </Select>)}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('district', {
                        rules: [
                            {
                                required: true,
                                message: lang === "ru" ? "Пожалуйста, выберите свой регион!" : 'Iltimos, viloyatingizni tanlang!',
                            },
                        ],
                    })(<Select
                        className={"select-address"}
                        placeholder={lang === "ru" ? "Выберите район" : "Tuman tanlang"}
                        onChange={this.handleChangeDistrict}
                        // value={this.state.selectedDistrictId}
                    >
                        {this.state.districts && this.state.districts.map(item => (
                            <Option value={item.id} key={item.id}>{lang === "ru" ? item.nameRu : item.nameUz}</Option>
                        ))}
                    </Select>)}
                </Form.Item>
                <Form.Item>
                    <Button loading={this.props.confirmLoading} type="primary" htmlType="submit"
                            className="login-form-button">
                        {lang === "ru" ? "Отправить" : "Yuborish"}
                    </Button>
                    <Row>
                        <Col span={18}>
                            <p className={"already-registered"}>{lang === "ru" ? "Уже зарегистрированы?" : "Ro'yxatdan o'tganmisiz?"}</p>

                        </Col>
                        <Col span={6}>
                            <Button type={"link"} className="login-form-forgot">
                                <Link to={"/sign-in"}>
                                    {lang === "ru" ? "Войти" : "Kirish"}
                                </Link>
                            </Button>
                        </Col>
                    </Row>

                </Form.Item>
            </Form>
        );
    }
}

const WrappedSignUpForm = Form.create({name: 'normal_login'})(SignUpForm);


class SignCodeForm extends Component {


    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log(values);
            if (!err) {
                this.props.onSubmitCode(values)
            }
        });
    };

    render() {
        const lang = localStorage.getItem(LANGUAGE);

        const {getFieldDecorator} = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit} className="sign-up-form">
                <h3>{lang === "ru" ? "Код был отправлен на номер " : ""}<span
                    style={{color: "#3352ff"}}>{this.props.phoneNumber}</span>{lang === "ru" ? "" : " Raqamiga tasdiqlash kodi yuborildi"}
                </h3>
                <Form.Item label={lang === "ru" ? "Введите код подтверждения" : 'Tasdiqlash kodini kiriting'}>
                    {getFieldDecorator('code', {
                        rules: [{
                            required: true,
                            message: lang === "ru" ? "Введите код!" : 'Tasdiqlash kodini kiriting!'
                        }]
                    })(<Input/>)}
                </Form.Item>
                <Form.Item>
                    <Button loading={this.props.confirmLoading} type="primary" htmlType="submit"
                            className="login-form-button">
                        {lang === "ru" ? "Отправить" : "Yuborish"}
                    </Button>

                    <Button type={"link"} onClick={()=>this.props.cancelConfirm()}>
                        {lang === "ru" ? "Я не получил код" : "Kod kelmadi"}
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

const WrappedSignCodeForm = Form.create({name: 'normal_login'})(SignCodeForm);


class Success extends Component {
    render() {
        const lang = localStorage.getItem(LANGUAGE);

        return (
            <Result
                icon={<Icon type="smile" theme="twoTone"/>}
                title={lang === "ru" ? "Регистрация завершена!" : "Ro'yxatdan o'tish amalga oshirildi!"}
                extra={<Button type="primary"><Link
                    to={"/sign-in"}>{lang === "ru" ? "Продолжить" : "Davom etish"}</Link></Button>}
            />
        );
    }
}

