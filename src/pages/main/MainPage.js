import React, {Component} from 'react';
import {Col, Layout} from "antd";
import Home from "./Home";

const {Sider, Content} = Layout;

class MainPage extends Component {
    render() {
        return (
            <Layout style={{backgroundColor:'#fff'}}>
                <Content>
                    <Home/>
                </Content>
            </Layout>
        );
    }
}

export default MainPage;