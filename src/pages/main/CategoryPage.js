import React, {Component} from 'react';

import "./CategoryPage.scss"

import {Button, Card, Icon, Layout, List, Modal, Typography} from "antd";
import axios from 'axios';
import {API_BASE_URL, LANGUAGE} from "../../constants";
import {addQuantity, addToCart, subtractQuantity} from "./redux/actions/cartActions";
import {addRelated, addSelected, loadingModal, showModalProduct} from './redux/actions/productActions'
import {connect} from "react-redux";
import Categories from "./components/Categories";
import NumberFormat from "react-number-format";


const {Sider, Content} = Layout;

const {Meta} = Card;
const { Paragraph } = Typography;
const ButtonGroup = Button.Group;

const lang = localStorage.getItem(LANGUAGE);

class CategoryPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            viewProduct: false,
            selectedProduct: {},
            selectedProductSrc: null
        }

    }



    handleAddToCart = (id, src) => {
        this.props.addToCart(id, src);
    };

    showProductModal = (item) => {
        this.props.loadingModal(true);
        this.props.showModalProduct(true);
        this.props.addSelected(item);

        this.fetchRelatedProducts(item.category)
    };

    fetchRelatedProducts = (id) => {
        const lang = localStorage.getItem(LANGUAGE);
        axios.get(API_BASE_URL + "product/category/" + id, {
            params:{
                "lang" : lang==="ru" ? "ru": "uz"
            }
        }).then(res => {

            console.log(res);
            this.props.addRelated(res.data);
            this.props.loadingModal(false);

        }).catch(err => {
            this.props.loadingModal(false);
            console.log(err);

        })
    };


    handleSubQuantity = (id) => {
        this.props.subtractQuantity(id);
    };

    handleAddQuantity = (id) => {
        this.props.addQuantity(id);
    };

    showAgreedModal=(sellerPhone)=>{
        Modal.info({
            title: <p style={{fontSize:15,textAlign:"center"}}>{lang === "ru" ? "Для заказа данного товара обратитесь по нижеуказанному номеру!":
                "Ushbu tovarga buyurtma berish uchun quyidagi raqamga murojaat qiling!"}</p>,
            content: (
                <div>
                    <a href={"tel:+998"+sellerPhone}><p style={{fontSize:18}}><Icon type="phone"/> (+998)
                        <NumberFormat value={sellerPhone} displayType={'text'} format=" ## ### ##-##" /></p></a>
                </div>
            ),
            onOk() {},
        });
    };


    render() {



        return (
            <Layout className="category-page-layout">
                <Sider className="category-page-sider">
                    <Categories/>
                </Sider>

                <Content className="category-page-content">
                    <div className="list-products">
                        <List
                            // bordered
                            loading={this.props.loading}
                            grid={{
                                gutter: 16,
                                xs: 1,
                                sm: 2,
                                md: 3,
                                lg: 4,
                                xl: 4,
                                xxl: 6,
                            }}
                            // header={<div>header</div>}
                            // footer={<div>footer</div>}
                            dataSource={this.props.categoryProducts}
                            renderItem={item => (
                                <List.Item>
                                    <Card
                                        className="category-product-card"
                                        hoverable={true}
                                        cover={
                                            <div className="product-image-container">
                                                <img
                                                    onClick={()=>this.showProductModal(item, 2)}
                                                    className="product-image-category"
                                                    alt="product"
                                                    src={item.img}
                                                />
                                            </div>

                                        }
                                        actions={[
                                            item.quantity ?
                                                <ButtonGroup>
                                                    <Button disabled={item.quantity === 1}
                                                            onClick={() => this.handleSubQuantity(item.id)} type="primary" shape="round"
                                                            icon={"minus"}/>
                                                    <Button disabled shape="round">{item.quantity}</Button>
                                                    <Button onClick={() => this.handleAddQuantity(item.id)} type="primary" shape="round"
                                                            icon={"plus"}/>
                                                </ButtonGroup>
                                                :
                                            <Button onClick={() => item.price==1 ? this.showAgreedModal(item.sellerPhone):  this.handleAddToCart(item.id, 3)} type="primary"
                                                    icon="shopping-cart">{lang === "ru" ? "Добавить в корзину":" Savatga qo'shish "}</Button>
                                        ]}
                                    >
                                        <Meta
                                            onClick={()=>this.showProductModal(item, 2)}
                                            title={item.name}
                                            description={<div>
                                                <Paragraph ellipsis>{item.categoryName}</Paragraph>
                                                {item.price==1 ?<span style={{marginLeft: 10, fontSize: "large", color: "#0b27ff"}}>{lang === "ru" ? "Цена договорная":"Kelishilgan narx"}</span>:
                                                    <NumberFormat style={{marginLeft: 10, fontSize: "large", color: "#0b27ff"}}
                                                                  value={item.price} displayType={'text'}
                                                                  thousandSeparator={' '} suffix={lang === "ru" ? " сум" : " so'm"}/> }
                                            </div>}
                                        />
                                    </Card>
                                </List.Item>
                            )}
                        />
                    </div>

                </Content>
            </Layout>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        categoryProducts: state.categoryProducts,
        loading: state.loading
    }
};

const mapDispatchToProps = (dispatch) => {

    return {
        addToCart: (id, src) => {
            dispatch(addToCart(id, src))
        },
        showModalProduct: (showModal) => {
            dispatch(showModalProduct(showModal))
        },
        addSelected: (product) => {
            dispatch(addSelected(product))
        },
        addRelated: (products) => {
            dispatch(addRelated(products))
        },
        loadingModal: (loading) => {
            dispatch(loadingModal(loading))
        },
        addQuantity: (id) => {
            dispatch(addQuantity(id))
        },
        subtractQuantity: (id) => {
            dispatch(subtractQuantity(id))
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryPage);