import React, {Component} from 'react';

import "./ComponentStyles.scss"
import {Button, Card, Col, Icon, Modal, Row, Spin} from "antd";
import {addQuantity, addToCart, subtractQuantity} from "../redux/actions/cartActions";
import {addRelated, addSelected, loadingModal, showModalProduct} from "../redux/actions/productActions";
import {connect} from "react-redux";
import NumberFormat from "react-number-format";
import Carousel from "react-multi-carousel";
import axios from "axios";
import {API_BASE_URL, LANGUAGE} from "../../../constants";

const {Meta} = Card;
const ButtonGroup = Button.Group;

const responsive = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: {max: 4000, min: 1200},
        items: 4
    },
    desktop: {
        breakpoint: {max: 1200, min: 950},
        items: 3,
    },
    tablet: {
        breakpoint: {max: 950, min: 700},
        items: 3,
    },
    tabletSmall: {
        breakpoint: {max: 700, min: 425},
        items: 2,
    },
    mobile: {
        breakpoint: {max: 425, min: 0},
        items: 1,
    },
};
const lang = localStorage.getItem(LANGUAGE);
class ProductViewModal extends Component {


    handleAddToCart = (id, src) => {
        this.props.addToCart(id, src);
    };

    handleSelectRelated = (item) => {
        this.props.loadingModal(true);
        this.props.showModalProduct(true);
        this.props.addSelected(item);

        this.fetchRelatedProducts(item.category)
    };

    fetchRelatedProducts = (id) => {
        const lang = localStorage.getItem(LANGUAGE);
        axios.get(API_BASE_URL + "product/category/" + id, {
            params:{
                "lang" : lang==="ru" ? "ru": "uz"
            }
        }).then(res => {

            console.log(res);
            this.props.addRelated(res.data);
            this.props.loadingModal(false);

        }).catch(err => {
            this.props.loadingModal(false);
            console.log(err)

        })
    };

    handleSubQuantity = (id) => {
        this.props.subtractQuantity(id);
    };

    handleAddQuantity = (id) => {
        this.props.addQuantity(id);
    };

    showAgreedModal=(sellerPhone)=>{
        Modal.info({
            title: <p style={{fontSize:15,textAlign:"center"}}>{lang === "ru" ? "Для заказа данного товара обратитесь по нижеуказанному номеру!":
                "Ushbu tovarga buyurtma berish uchun quyidagi raqamga murojaat qiling!"}</p>,
            content: (
                <div>
                    <a href={"tel:+998"+sellerPhone}><p style={{fontSize:18}}><Icon type="phone"/> (+998)
                        <NumberFormat value={sellerPhone} displayType={'text'} format=" ## ### ##-##" /></p></a>
                </div>
            ),
            onOk() {},
        });
    };


    render() {


        let relatedProducts = this.props.relatedProducts.map(item => {
            return (
                <div className={"related-carusel-product-item"} key={item.id}>
                    <Card
                        hoverable
                        className="related-carusel-product-card"
                        cover={
                            <div className="related-product-image-container">
                                <img
                                    onClick={() => this.handleSelectRelated(item)}
                                    className="related-product-image"
                                    alt="product"
                                    src={item.img}
                                />
                            </div>
                        }
                        actions={[
                            item.quantity ?
                                <ButtonGroup>
                                    <Button disabled={item.quantity === 1}
                                            onClick={() => this.handleSubQuantity(item.id)} type="primary"
                                            shape="round"
                                            icon={"minus"}/>
                                    <Button disabled shape="round">{item.quantity}</Button>
                                    <Button onClick={() => this.handleAddQuantity(item.id)} type="primary"
                                            shape="round"
                                            icon={"plus"}/>
                                </ButtonGroup>
                                :
                            <Button onClick={() => item.price==1 ? this.showAgreedModal(item.sellerPhone):  this.handleAddToCart(item.id, 6)} type="primary"
                                    icon="shopping-cart">{lang === "ru" ? "Добавить в корзину":"Savatga qo'shish"}</Button>
                        ]}
                    >
                        <Meta
                            onClick={() => this.handleSelectRelated(item)}
                            title={item.name}
                            description={<div>
                                <p>{item.categoryName}</p>
                                {item.price==1 ?<span style={{marginLeft: 10, fontSize: "large", color: "#0b27ff"}}>{lang === "ru" ? "Цена договорная":"Kelishilgan narx"}</span>:
                                    <NumberFormat style={{marginLeft: 10, fontSize: "large", color: "#0b27ff"}}
                                                  value={item.price} displayType={'text'}
                                                  thousandSeparator={' '} suffix={lang === "ru" ? " сум" : " so'm"}/> }
                            </div>}
                        />
                    </Card>
                </div>
            )
        });

        const product = this.props.selectedProduct[0];
        return (
            <Modal
                className="product-modal"
                title={<span>{product ? product.categoryName:""}</span>}
                visible={this.props.showModal}
                onCancel={() => this.props.showModalProduct(false)}
                footer={null}
            >
                <Spin spinning={this.props.isLoadingModal}>
                    {!this.props.isLoadingModal && product ?
                        <div>
                        <Row>
                            <Col xs={{span: 24}} sm={{span: 24}} md={{span: 13}} lg={{span: 14}}>
                                <div className={"modal-product-image-container"}>
                                    <img
                                        className="modal-product-image"
                                        alt="product"
                                        src={product.img}
                                    />
                                </div>
                            </Col>
                            <Col xs={{span: 24}} sm={{span: 24}} md={{span: 11}} lg={{span: 10}}>
                                <div className={"modal-product-details-container"}>
                                        <h3>{product.name}</h3>
                                    <p>{product.info}</p>
                                        <div>
                                            {product.price==1 ?<span className={"product-price"}
                                                                 >{lang === "ru" ? "Цена договорная":"Kelishilgan narx"}</span>:
                                                <NumberFormat className={"product-price"}
                                                              // style={{marginLeft: 10}}
                                                              value={product.price}
                                                              displayType={'text'}
                                                              thousandSeparator={' '}
                                                              suffix={lang === "ru" ? " сум" : " so'm"}/> }
                                        </div>
                                    {product.deliveryDay ? <p>{lang === "ru" ? `Товар будет доставлен в течении ${product.deliveryDay} дней.`:`Ushbu mahsulot ${product.deliveryDay} kun ichida yetkazib beriladi.`}</p>:""}
                                    {product.quantity ?
                                        <ButtonGroup className={"product-modal-button"}>
                                            <Button disabled={product.quantity === 1}
                                                    onClick={() => this.handleSubQuantity(product.id)} type="primary"
                                                    shape="round"
                                                    icon={"minus"}/>
                                            <Button disabled shape="round">{product.quantity}</Button>
                                            <Button onClick={() => this.handleAddQuantity(product.id)} type="primary"
                                                    shape="round"
                                                    icon={"plus"}/>
                                        </ButtonGroup>
                                        :
                                        <Button className={"product-modal-button"}
                                                onClick={() => product.price==1 ? this.showAgreedModal(product.sellerPhone):  this.handleAddToCart(product.id, 5)} type="primary"
                                                icon="shopping-cart">{lang === "ru" ? "Добавить в корзину" : "Savatga qo'shish"}</Button>
                                    }
                                </div>
                            </Col>
                        </Row>
                            <h2>{lang === "ru" ? "Похожие товары":"O'xshash mahsulotlar"}</h2>
                            <Carousel
                                className="modal-related-carousel-products"
                                showDots={false}
                                infinite={true}
                                autoPlay={true}
                                responsive={responsive}
                                removeArrowOnDeviceType={["tablet", "mobile", "tabletSmall", "desktop", "superLargeDesktop"]}>
                                {relatedProducts}
                            </Carousel>
                        </div> : ""

                    }

                </Spin>
            </Modal>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        selectedProduct: state.selectedProduct,
        relatedProducts: state.relatedProducts,
        isLoadingModal: state.isLoadingModal,
        showModal: state.showModal
    }
};

const mapDispatchToProps = (dispatch) => {

    return {
        addToCart: (id, src) => {
            dispatch(addToCart(id, src))
        },
        showModalProduct: (showModal) => {
            dispatch(showModalProduct(showModal))
        },
        addSelected: (product) => {
            dispatch(addSelected(product))
        },
        addRelated: (products) => {
            dispatch(addRelated(products))
        },
        loadingModal: (loading) => {
            dispatch(loadingModal(loading))
        },
        addQuantity: (id) => {
            dispatch(addQuantity(id))
        },
        subtractQuantity: (id) => {
            dispatch(subtractQuantity(id))
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductViewModal);