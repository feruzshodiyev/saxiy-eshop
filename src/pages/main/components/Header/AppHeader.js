import React, {Component} from 'react';
import {
    Layout,
    Menu,
    Icon,
    Avatar,
    Dropdown,
    Drawer,
    Badge,
    Card,
    List,
    Popover,
    Row,
    Col,
    Affix,
    Modal,
    Button,
    Input
} from 'antd';

import NumberFormat from 'react-number-format';

import {Trans, withTranslation} from 'react-i18next';


import axios from "axios";

import {Link, withRouter} from 'react-router-dom';

import {connect} from 'react-redux'


import './AppHeader.scss';
import {ACCESS_TOKEN_USER, API_BASE_URL, LANGUAGE} from "../../../../constants";

import {addQuantity, removeItem, subtractQuantity, changeQuantity} from "../../redux/actions/cartActions";
import Categories from "../Categories";
import ProductSearch from "../ProductSearch";
import CategoriesMobile from "../CategoriesMobile";


const Header = Layout.Header;
const MenuItem = Menu.Item;
const ButtonGroup = Button.Group;
const {Search} = Input;

const {confirm} = Modal;
const {SubMenu} = Menu;

class AppHeader extends Component {


    interval = null;

    constructor(props) {
        super(props);

        this.state = {
            prevScrollpos: window.pageYOffset,
            modalVisible: false,
            drawerVisible: false,
            drawerVisible2: false,
            drawerVisible3: false,
            categories: []
        }
    }

    componentDidMount() {

        // window.addEventListener('scroll', this.handleScroll);

        this.fetchCategories();
    }

    componentWillUnmount() {
        // window.removeEventListener("scroll", this.handleScroll);

    }

    handleScroll = () => {
        const {prevScrollpos} = this.state;

        const currentScrollPos = window.pageYOffset;

        const visible = prevScrollpos !== currentScrollPos;

        this.setState({
            visible: visible
        });

    };


    showDrawer = () => {
        this.setState({
            drawerVisible: true,
        });
    };


    showDrawer2 = () => {
        const isOnCartPage = this.props.history ? this.props.history.location.pathname.includes('/cart') : false;
        if (this.props.items.length && !isOnCartPage) {
            this.setState({
                drawerVisible2: true,
            });
        }

    };

    showDrawer3 = () => {
        const isOnCartPage = this.props.history ? this.props.history.location.pathname.includes('/cart') : false;
        if (this.props.items.length && !isOnCartPage) {
            this.setState({
                drawerVisible3: true,
            });
        }

    };

    onCloseDrawer = () => {
        this.setState({
            drawerVisible: false,
        });
    };

    onCloseDrawer2 = () => {
        this.setState({
            drawerVisible2: false,
        });
    };
    onCloseDrawer3 = () => {
        this.setState({
            drawerVisible3: false,
        });
    };


    //to remove the item completely
    handleRemove = (id) => {
        this.props.removeItem(id);
    };
    //to add the quantity
    handleAddQuantity = (id) => {
        this.props.addQuantity(id);
    };
    //to substruct from the quantity
    handleSubtractQuantity = (item) => {
        if (item.quantity > 1) {
            this.props.subtractQuantity(item.id);
        }
    };


    handleChangeLang = (lang) => {

        const {i18n} = this.props;

        i18n.changeLanguage(lang);

        localStorage.setItem(LANGUAGE, lang);

        window.location.reload();

    };


    fetchCategories = () => {
        axios.get(API_BASE_URL + "category/tree/", {}).then(res => {
            this.setState({
                categories: res.data
            })
        }).catch(err => {
            console.log(err)
        })
    };

    onChange = (e, id) => {
        const {value} = e.target;
        const reg = /^-?[0-9]*(\.[0-9]*)?$/;
        if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
            this.props.changeQuantity(id, value);
        }
    };


    handleLogout = () => {
        const lang = localStorage.getItem(LANGUAGE);
        confirm({
            title: lang==="ru"? "Вы действительно хотите выйти?": 'Rostdan ham chiqmoqchimisiz?',
            okText:  lang==="ru"? "Да": "Ha",
            cancelText: lang==="ru"? "Нет":"Yo'q",
            onOk() {
                localStorage.removeItem(ACCESS_TOKEN_USER);
                window.location.reload();
            },
            onCancel() {
                console.log('Cancel');
            },
        });

    };

    handleCheckout = () => {

        console.log("call!");
        this.onCloseDrawer2();
        this.onCloseDrawer3();

        if (!this.props.isAuthenticated) {
            this.showModalAuth()
        }
    };

    showModalAuth = () => {
        this.setState({
            modalVisible: true
        })
    };

    closeModal = () => {
        this.setState({
            modalVisible: false
        });
        this.onCloseDrawer3();
    };


    render() {

        const lang = localStorage.getItem(LANGUAGE);
        let addedItems = this.props.items;
        let total = this.props.total;

        const drawerMenu = (
            <div>
                <CategoriesMobile
                    handleSelect={this.onCloseDrawer}
                />
                <Menu
                    mode="inline"
                >
                    <Menu.Divider className="devider-under-category"/>
                    <SubMenu title= {  <span>
                <Icon className="menu-icon" type="phone" />
                <span className={"menu-text"}>{lang === "ru" ? "Связаться с нами":"Biz bilan aloqa"}</span>
              </span>}>
                    <Menu.Item
                    >
                        <a href="tel:+998998651004">

                            <span className={"menu-text"}>+998 (99) 865-1004</span>
                        </a>

                    </Menu.Item>
                        <Menu.Item
                    >
                        <a href="tel:+998993651004">

                            <span className={"menu-text"}>+998 (99) 365-1004</span>
                        </a>

                    </Menu.Item>
                    </SubMenu>
                    <Menu.Divider/>
                    <SubMenu title= {  <span>
                <Icon className="menu-icon" type="global" />
                <span className={"menu-text"}>{lang === "ru" ? "Язык":"Til"}</span>
              </span>}>
                        <Menu.Item
                            onClick={() => this.handleChangeLang("uz")}
                        >
                            <span className={"menu-text"}>O'zbekcha</span>
                        </Menu.Item>
                        <Menu.Item
                            onClick={() => this.handleChangeLang("ru")}
                        >

                            <span className={"menu-text"}>Русский</span>

                        </Menu.Item>
                    </SubMenu>
                    <Menu.Divider/>
                    <SubMenu title= {  <span>
                <Icon className="menu-icon" type="user" />
                <span className={"menu-text"}>{this.props.isAuthenticated ? this.props.userDetail.fullName:lang === "ru" ? "Кабинет":"Kabinet"}</span>
              </span>}>
                        {this.props.isAuthenticated ?
                            <Menu.Item onClick={this.onCloseDrawer}>
                                <Link to="/my-profile"> <span className={"menu-text"}>{lang === "ru" ? "Мой профиль":"Mening profilim"}</span></Link>
                            </Menu.Item>:""
                        }
                        {this.props.isAuthenticated ?
                            <Menu.Item onClick={this.onCloseDrawer}>
                                <Link to="/my-orders"> <span className={"menu-text"}>{lang === "ru" ? "Мои заказы":"Buyurtmalarim"}</span></Link>
                            </Menu.Item>:
                            <Menu.Item
                            onClick={this.onCloseDrawer}>
                                <Link to="/sign-in"> <span className={"menu-text"}>{lang === "ru" ? "Войти":"Kirish"}</span></Link>
                            </Menu.Item>

                        }
                        {this.props.isAuthenticated ?
                            <Menu.Item>
                            <Button type="danger" icon={"logout"}
                                    onClick={this.handleLogout}><span className={"menu-text"}>{lang === "ru" ? "Выход":"Chiqish"}</span></Button>
                            </Menu.Item>:
                            <Menu.Item onClick={this.onCloseDrawer}>
                                <Link to="/sign-up"><span className={"menu-text"}>{lang === "ru" ? "Зарегистрироваться":"Ro'yxatdan o'tish"}</span></Link>
                            </Menu.Item>

                        }
                    </SubMenu>

                </Menu>
            </div>
        );

        const cart = (
            <Card title={addedItems.length > 0 ? <div>
                <Link to="/cart"><Button onClick={() => this.handleCheckout()} type="primary" icon="shopping-cart">
                    {lang === "ru" ? "Сделать покупку":"Xaridni amalga oshirish"}
                    </Button></Link>
            </div> : null} style={{minWidth: 300}}>

                <List
                    dataSource={addedItems}
                    renderItem={item => (
                        <List.Item key={item.id}>
                            <List.Item.Meta
                                avatar={
                                    <Avatar size="large" shape="square" src={item.img}/>
                                }
                                title={<div>
                                    <p><span>{item.name}</span></p>

                                </div>}
                                description={<div>
                                    <Row>
                                        <Col xs={{span: 16, order: 2}} sm={{span: 12}} md={{span: 12}} lg={{span: 12}}>
                                            <Input
                                                style={{maxWidth: 140}}
                                                size={"small"}
                                                addonBefore={<Icon onClick={() => {
                                                    this.handleAddQuantity(item.id)
                                                }} className="quantity-icon" type="plus"/>}
                                                addonAfter={<Icon onClick={() => {
                                                    this.handleSubtractQuantity(item)
                                                }} className="quantity-icon" type="minus"/>}
                                                onChange={(event) => this.onChange(event, item.id)}
                                                value={item.quantity}
                                            />
                                        </Col>
                                        <Col xs={{span: 24, order: 1}} sm={{span: 12}} md={{span: 12}} lg={{span: 12}}>
                                            <NumberFormat style={{marginLeft: 10}} value={item.price * item.quantity}
                                                          displayType={'text'}
                                                          thousandSeparator={' '} suffix={lang === "ru" ? " сум":" so'm"}/>
                                        </Col>
                                    </Row>
                                </div>}
                            />
                            <div>
                                <Button onClick={() => {
                                    this.handleRemove(item.id)
                                }} size="small" type="danger" icon="delete"/>

                            </div>

                        </List.Item>
                    )}
                >
                </List>

            </Card>
        );

        const menu = (
            <Menu>
                <Menu.Item
                    onClick={() => this.handleChangeLang("uz")}
                >
                    O'zbekcha
                </Menu.Item>
                <Menu.Item
                    onClick={() => this.handleChangeLang("ru")}
                >
                    Русский
                </Menu.Item>
            </Menu>
        );


        const userMenu = (
            <Menu>
                <Menu.Item>
                    <Button style={{width: '100%'}} type="primary"><Link to="/sign-in">{lang === "ru" ? "Войти":"Kirish"}</Link></Button>
                </Menu.Item>
                <Menu.Item>
                    <Button style={{width: '100%'}} type="danger"><Link to="/sign-up">{lang === "ru" ? "Зарегистрироваться":"Ro'yxatdan o'tish"}</Link></Button>
                </Menu.Item>
            </Menu>
        );

        const userMenu2 = (
            <Menu>
                <Menu.Item>
                    <Button style={{width: '100%'}}><Link to="/my-profile">{lang === "ru" ? "Мой профиль":"Mening profilim"}</Link></Button>
                </Menu.Item>
                <Menu.Item>
                    <Button style={{width: '100%'}}><Link to="/my-orders">{lang === "ru" ? "Мои заказы":"Buyurtmalarim"}</Link></Button>
                </Menu.Item>
                <Menu.Item>
                    <Button style={{width: '100%'}} type="danger" icon={"logout"}
                            onClick={this.handleLogout}>{lang === "ru" ? "Выход":"Chiqish"}</Button>
                </Menu.Item>
            </Menu>
        );

        return (

            <div>

                <Modal
                    title={lang === "ru" ? "Войдите, чтобы сделать покупку!":"Xaridni amalga oshirish uchun tizimga kiring!"}
                    visible={this.state.modalVisible}
                    footer={null}
                    closable={false}
                    onCancel={this.closeModal}
                >

                    <Button onClick={this.closeModal} type="link" block>
                        <Link to={"/sign-in"}>
                            {lang === "ru" ? "Войти":"Kirish"}

                        </Link>
                    </Button>
                    <Button onClick={this.closeModal} type="link" block>
                        <Link to={"/sign-up"}>
                            {lang === "ru" ? "Зарегистрироватьса":"Ro'yxatdan o'tish"}

                        </Link>

                    </Button>

                </Modal>
                <div className="header-top-desktop">
                    <Row>
                        <Col span={4}>

                        </Col>
                        <Col span={4}>

                        </Col>
                        <Col span={4}>

                        </Col>
                        <Col span={4}>
                            <a href="tel:+998998651004"><h3><Icon type="phone"/>+998 (99) 865-1004</h3></a>
                        </Col>
                        <Col span={4}>
                            <a href="tel:+998993651004"><h3><Icon type="phone"/>+998 (99) 365-1004</h3></a>
                        </Col>
                        <Col span={4}>
                            <Dropdown overlay={menu} trigger={['click']}>
                                <Button type="link" icon={"global"} style={{float: 'right', right: 40}} onClick={e => e.preventDefault()}>
                                    {lang === "ru" ? "Русский":"O'zbekcha"}
                                    <Icon type="down"/>
                                </Button>
                            </Dropdown>
                        </Col>
                    </Row>
                </div>
                <Affix className={"affix"} offsetTop={0}>

                    <Header style={{background: '#4000ff'}}>
                        <div className="header-top-desktop">
                            <Row>
                                <Col span={3}>
                                    <Link to="/">
                                        <div className="logo-header"/>
                                    </Link>
                                </Col>

                                <Col span={2}>

                                </Col>

                                <Col span={10}>
                                    <ProductSearch/>
                                </Col>
                                <Col style={{padding: 10}} span={9}>
                                    <Row>
                                        <Col span={14}>
                                            <Dropdown overlay={this.props.isAuthenticated ? userMenu2 : userMenu}>
                                                <a style={{float: 'right', width: 150, color: "white"}}>
                                                    <Icon style={{fontSize: 30}} type="user"/>
                                                    <div style={{
                                                        display: 'inline-block',
                                                        top: 1,
                                                        position: 'relative',
                                                        padding: "0px 0px 0px 5px",
                                                        width:"74%"
                                                    }}>
                                                        <h3 className={"user-text"} style={{top: -17, position: 'absolute', color: "white"}}>
                                                            {this.props.isAuthenticated ? this.props.userDetail.fullName:lang === "ru" ? "Кабинет":"Kabinet"}
                                                        </h3>
                                                        <p style={{margin: 0, padding: 0}}>
                                                            {this.props.isAuthenticated ? lang === "ru" ? "Заказы":"Buyurtmalar" : lang === "ru" ? "Войти":"Kirish"}
                                                        </p>
                                                    </div>
                                                </a>
                                            </Dropdown>
                                        </Col>
                                        <Col span={10}>

                                            <a style={{color: "white"}} onClick={this.showDrawer2}>
                                                <Badge style={{display: "inline-block"}} count={addedItems.length}
                                                       showZero>
                                                    <Icon style={{fontSize: 30}} type="shopping-cart"/>
                                                </Badge>
                                                <div style={{
                                                    display: 'inline-block',
                                                    top: 1,
                                                    position: 'relative',
                                                    padding: "0px 0px 5px 15px"
                                                }}>
                                                    <h3 style={{top: -17, position: 'absolute', color: "white"}}>
                                                        {lang === "ru" ? "Корзина":"Savatcha"}
                                                    </h3>
                                                    <p style={{margin: 0, padding: 0}}>
                                                        {addedItems.length ?
                                                            <NumberFormat value={total} displayType={'text'}
                                                                          thousandSeparator={' '}
                                                                          suffix={lang === "ru" ? " сум":" so'm"}/> : lang === "ru" ? "нет товара":"mahsulot yo'q"}
                                                    </p>
                                                </div>
                                            </a>
                                            <Drawer
                                                width={420}
                                                className={this.state.drawerVisible2 ? "header-drawer" : "header-drawer-closed"}
                                                title={lang === "ru" ? "Корзина":"Savatcha"}
                                                closable={true}
                                                onClose={this.onCloseDrawer2}
                                                visible={this.state.drawerVisible2}
                                            >
                                                {cart}
                                            </Drawer>
                                        </Col>
                                    </Row>


                                </Col>
                            </Row>
                        </div>
                    </Header>
                </Affix>

                <Header className="header-mobile">
                    <div className={"header-mobile-div"}>
                        <Row>
                            <Col span={2}>
                                <a style={{color: "white"}} onClick={this.showDrawer}>
                                    <Icon style={{fontSize: "x-large"}} type="menu"/>
                                </a>
                                <Drawer
                                    className={this.state.drawerVisible ? "header-drawer" : ""}
                                    title={<Link onClick={this.onCloseDrawer} to="/">
                                        <div className="logo-drawer"/>
                                    </Link>}
                                    placement="left"
                                    onClose={this.onCloseDrawer}
                                    visible={this.state.drawerVisible}
                                >
                                    {drawerMenu}
                                </Drawer>
                            </Col>

                            <Col span={19} offset={1}>
                                <ProductSearch/>
                            </Col>
                            <Col span={1} offset={1}>
                                <a style={{color: "white"}} onClick={this.showDrawer3}>
                                    <Badge count={addedItems.length}
                                           showZero>
                                        <Icon style={{fontSize: 30}} type="shopping-cart"/>
                                    </Badge>
                                </a>
                                <Drawer
                                    className={this.state.drawerVisible3 ? "header-drawer" : "header-drawer-closed"}
                                    title={lang === "ru" ? "Корзина":"Savatcha"}
                                    closable={true}
                                    onClose={this.onCloseDrawer3}
                                    visible={this.state.drawerVisible3}
                                >
                                    {cart}
                                </Drawer>
                            </Col>
                        </Row>
                    </div>
                </Header>


            </div>


        );
    }
}


const mapStateToProps = (state) => {
    return {
        items: state.addedItems,
        total: state.total
    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        removeItem: (id) => {
            dispatch(removeItem(id))
        },
        addQuantity: (id) => {
            dispatch(addQuantity(id))
        },
        subtractQuantity: (id) => {
            dispatch(subtractQuantity(id))
        },
        changeQuantity: (id, number) => {
            dispatch(changeQuantity(id, number))
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withTranslation('common')(AppHeader)));
