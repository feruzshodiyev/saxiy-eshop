import React, {Component} from 'react';
import {Avatar, Button, Card, Col, Dropdown, Icon, Input, List, Modal, Row} from "antd";
import NumberFormat from "react-number-format";
import axios from 'axios';
import {addToCart} from "../redux/actions/cartActions";
import {addRelated, addSearch, addSelected, loadingModal, showModalProduct} from "../redux/actions/productActions"
import {connect} from "react-redux";
import {API_BASE_URL, LANGUAGE} from "../../../constants";
import './ComponentStyles.scss';
import {Link} from "react-router-dom";

const {Search} = Input;
const lang = localStorage.getItem(LANGUAGE);
class ProductSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dropdownVisible: false,
            products: [],
            loading: false
        }
    }

    handleSearch = (value) => {
        const lang = localStorage.getItem(LANGUAGE);
        if (value.length > 3) {
            this.setState({
                loading: true,
                dropdownVisible: true
            });
            axios.get(API_BASE_URL + "product/search", {

                params: {
                    "q": value,
                    "lang": lang === "ru" ? "ru" : "uz"
                }
            }).then(res => {
                this.props.addSearch(res.data);

                this.setState({
                    loading: false,
                    products: res.data
                });
            }).catch(err => {
                this.props.addSearch([]);
                this.setState({
                    loading: false
                });
            })


        } else {
            this.setState({
                loading: false,
                dropdownVisible: false
            });
            this.props.addSearch([])
        }
    };


    showProductModal = (item) => {
        this.props.loadingModal(true);
        this.props.showModalProduct(true);
        this.props.addSelected(item);
        this.props.addRelated(this.state.products);
        this.props.loadingModal(false);
    };


    handleAddToCart = (id) => {

        this.props.addToCart(id, 4);

    };


    handleChange = (event) => {
        if (event.target.value.length > 3) {
            this.handleSearch(event.target.value)
        } else {
            this.setState({
                loading: false,
                dropdownVisible: false
            });
        }
    };

    showAgreedModal=(sellerPhone)=>{
        Modal.info({
            title: <p style={{fontSize:15,textAlign:"center"}}>{lang === "ru" ? "Для заказа данного товара обратитесь по нижеуказанному номеру!":
                "Ushbu tovarga buyurtma berish uchun quyidagi raqamga murojaat qiling!"}</p>,
            content: (
                <div>
                    <a href={"tel:+998"+sellerPhone}><p style={{fontSize:18}}><Icon type="phone"/> (+998)
                        <NumberFormat value={sellerPhone} displayType={'text'} format=" ## ### ##-##" /></p></a>
                </div>
            ),
            onOk() {},
        });
    };

    render() {

        const searchResult = (
            <Card className={"search-card"}>

                <List
                    loading={this.state.loading}
                    dataSource={this.props.searchProducts}
                    renderItem={item => (
                        <List.Item key={item.id}>
                            <List.Item.Meta
                                avatar={
                                    <Avatar onClick={() => this.showProductModal(item)}
                                            className={"search-product-image"} size="large" shape="square"
                                            src={item.img}/>
                                }
                                title={<div onClick={() => this.showProductModal(item)}
                                            className={"search-product-title"}>
                                    <p><span>{item.name}</span></p>

                                </div>}
                                description={<div>
                                    <Row>

                                        <Col xs={{span: 24}} lg={{span: 12}}>
                                            <div onClick={() => this.showProductModal(item)}
                                                 className={"search-product-price"}>
                                                {item.price==1 ?<span style={{marginLeft: 10}}>{lang === "ru" ? "Цена договорная":"Kelishilgan narx"}</span>:
                                                    <NumberFormat style={{marginLeft: 10}}
                                                                  value={item.price}
                                                                  displayType={'text'}
                                                                  thousandSeparator={' '}
                                                                  suffix={lang === "ru" ? " сум" : " so'm"}/> }
                                            </div>
                                        </Col>

                                        <Col xs={{span: 24}} lg={{span: 12}}>
                                            <Button size={"small"}
                                                    onClick={() => item.price==1 ? this.showAgreedModal(item.sellerPhone):   this.handleAddToCart(item.id)}
                                                    shape="round"
                                                    icon="shopping-cart"
                                                    type={"primary"}>
                                                {lang === "ru" ? "Добавить в корзину" : "Savatga qo'shish"}</Button>
                                        </Col>
                                    </Row>
                                </div>}
                            />
                        </List.Item>
                    )}
                >
                </List>
            </Card>
        );
        return (
            <div>
                <Dropdown
                    overlayClassName="search-dropdown"
                    visible={this.state.dropdownVisible}
                    overlay={searchResult}>
                    <Search
                        allowClear={true}
                        placeholder={lang === "ru" ? "Поиск по товарам" : "Mahsulotlar aro qidiruv"}
                        size="large"
                        onSearch={value => this.handleSearch(value)}
                        onChange={event => this.handleChange(event)}
                    />
                </Dropdown>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        searchProducts: state.searchProducts
    }
};

const mapDispatchToProps = (dispatch) => {

    return {
        addSearch: (products) => {
            dispatch(addSearch(products))
        },
        addToCart: (id, src) => {
            dispatch(addToCart(id, src))
        },
        showModalProduct: (showModal) => {
            dispatch(showModalProduct(showModal))
        },
        addSelected: (product) => {
            dispatch(addSelected(product))
        },
        addRelated: (products) => {
            dispatch(addRelated(products))
        },
        loadingModal: (loading) => {
            dispatch(loadingModal(loading))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductSearch);
