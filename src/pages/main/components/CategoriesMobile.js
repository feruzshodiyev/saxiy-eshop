import React, {Component} from 'react';
import {API_BASE_URL, LANGUAGE} from "../../../constants";
import {Icon, Menu} from "antd";
import {Link, withRouter} from "react-router-dom";
import {connect} from 'react-redux'
import "./ComponentStyles.scss"
import {addByCategory, startLoading} from "../redux/actions/productActions";
import axios from "axios";

const {SubMenu} = Menu;

class CategoriesMobile extends Component {

    componentDidMount() {
        this.checkForCurrentPageOnLoad()
    }


    checkForCurrentPageOnLoad = () => {
        if (this.props.history.location.pathname.includes('/category/')) {
            const str = this.props.history.location.pathname;
            const id = str.slice(str.lastIndexOf("/") + 1, str.length);
            console.log(id);
            this.fetchProductByCategory(id)
        } else if (this.props.history.location.pathname.includes('/p-category/')){
            const str = this.props.history.location.pathname;
            const id = str.slice(str.lastIndexOf("/") + 1, str.length);
            this.fetchProductByParentCategory(id)
        } else {
            console.log("location not found")
        }
    };


    fetchProductByCategory = (id) => {
        this.props.handleSelect();
        this.props.startLoading(true);
        const lang = localStorage.getItem(LANGUAGE);
        axios.get(API_BASE_URL + "product/category/" + id, {
            params: {
                "lang": lang === "ru" ? "ru" : "uz"
            }
        }).then(res => {

            console.log(res);

            this.props.addByCategory(res.data);
            this.props.startLoading(false);

        }).catch(err => {

            this.props.startLoading(false);
        })
    };

    fetchProductByParentCategory = (id) => {
        this.props.handleSelect();
        this.props.startLoading(true);
        const lang = localStorage.getItem(LANGUAGE);
        axios.get(API_BASE_URL + "product/parent-category/" + id, {
            params: {
                "lang": lang === "ru" ? "ru" : "uz"
            }
        }).then(res => {

            console.log(res);

            this.props.addByCategory(res.data);
            this.props.startLoading(false);

        }).catch(err => {

            this.props.startLoading(false);
        })


    };


    render() {
        const lang = localStorage.getItem(LANGUAGE);

        return (
            <Menu
                className="category-menu"
                mode="inline">
                <Menu.ItemGroup key="g1" title={<h3>{lang === 'ru' ? "Категории":"Kategoriyalar"}</h3>}>
                    {/*<Menu.Divider/>*/}
                    {
                        this.props.categories.map(item =>
                            item.subCategory ?
                                <SubMenu
                                    className="category-menu-item"
                                    key={item.id}
                                    title={
                                        <span>
          <img height={25} width={25} src={item.icon} className={"category-icon-img"}/>
          <span className="category-list-text">{lang === 'ru' ? item.nameRu : item.nameUz}</span>
        </span>
                                    }
                                >
                                    <Menu.ItemGroup key="g2" >

                                        <Menu.Item
                                            onClick={() => this.fetchProductByParentCategory(item.id)}
                                            className="category-menu-item"
                                        >
                                            <Link to={`/p-category/${item.id}`}>
                                                {/*<img height={30} width={30} src={item.icon}/>*/}
                                                <div
                                                    className="category-list-text">{lang === 'ru' ? "Все":"Barchasi"}</div>
                                            </Link>
                                        </Menu.Item>

                                    {item.subCategory.map(subItem =>
                                            subItem.subCategory ?
                                                <SubMenu
                                                    className="category-menu-item"
                                                    key={subItem.id}
                                                    title={
                                                        <span>
          <Icon type={subItem.icon}/>
          <span className="category-list-text">{lang === 'ru' ? subItem.nameRu : subItem.nameUz}</span>
        </span>
                                                    }>

                                                    <Menu.ItemGroup key="g3">

                                                        <Menu.Item
                                                            onClick={() => this.fetchProductByParentCategory(subItem.id)}
                                                            className="category-menu-item"
                                                        >
                                                            <Link to={`/p-category/${subItem.id}`}>
                                                                {/*<img height={30} width={30} src={item.icon}/>*/}
                                                                <div
                                                                    className="category-list-text">{lang === 'ru' ? "Все":"Barchasi"}</div>
                                                            </Link>
                                                        </Menu.Item>


                                                    {subItem.subCategory.map(subItem2 =>
                                                        <Menu.Item
                                                            onClick={() => this.fetchProductByCategory(subItem2.id)}
                                                            className="category-menu-item"
                                                            key={subItem2.id}>
                                                            <Link to={`/category/${subItem2.id}`}>
                                                                {/*<img height={30} width={30} src={item.icon}/>*/}
                                                                <span
                                                                    className="category-list-text">{lang === 'ru' ? subItem2.nameRu : subItem2.nameUz}</span>
                                                            </Link>
                                                        </Menu.Item>
                                                    )

                                                    }
                                                    </Menu.ItemGroup>

                                                </SubMenu> :

                                                <Menu.Item
                                                    onClick={() => this.fetchProductByCategory(subItem.id)}
                                                    className="category-menu-item"
                                                    key={subItem.id}>
                                                    <Link to={`/category/${subItem.id}`}>
                                                        {/*<img height={30} width={30} src={item.icon}/>*/}
                                                        <span
                                                            className="category-list-text">{lang === 'ru' ? subItem.nameRu : subItem.nameUz}</span>
                                                    </Link>
                                                </Menu.Item>
                                    )

                                    }
                                    </Menu.ItemGroup>

                                </SubMenu> :

                                <Menu.Item
                                    onClick={() => this.fetchProductByCategory(item.id)}
                                    className="category-menu-item"
                                    key={item.id}>
                                    <Link to={`/category/${item.id}`}>
                                        <img height={25} width={25} src={item.icon}/>
                                        <span
                                            className="category-list-text">{localStorage.getItem(LANGUAGE) === 'ru' ? item.nameRu : item.nameUz}</span>
                                    </Link>
                                </Menu.Item>)
                    }

                </Menu.ItemGroup>
            </Menu>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        categories: state.categories
    }
};

const mapDispatchToProps = (dispatch) => {

    return {

        addByCategory: (products) => {
            dispatch(addByCategory(products))
        },
        startLoading: (loading) => {
            dispatch(startLoading(loading))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CategoriesMobile));