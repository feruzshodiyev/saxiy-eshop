import React, {Component} from 'react';

import "./Profile.scss"
import {ACCESS_TOKEN_USER, API_BASE_URL, LANGUAGE} from "../../constants";
import NumberFormat from "react-number-format";
import {Button, Form, Icon, Input, notification, Spin} from "antd";

import axios from "axios";

const lang = localStorage.getItem(LANGUAGE);

class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showForm: false,
            spinning: false
        }
    }

    showHideForm = () => {
        this.setState({
            showForm: !this.state.showForm
        })
    };

    handleSubmitPasswords = (values) => {
        console.log(values)
        this.setState({
            spinning: true
        });
        axios.put(API_BASE_URL + "user/edit-password", values, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_USER)
            }
        }).then(res => {
            console.log(res);
            notification.success({
                message: lang === "ru" ? "Пароль сохранен!" : "Parol saqlandi!"
            });
            this.setState({
                spinning: false,
                showForm: false,
            })
        }).catch(err => {
            console.log(err);
            notification.error({
                message: lang === "ru" ? "Ошибка!" : "Xatolik!"
            });
            this.setState({
                spinning: false
            })
        })
    };

    render() {
        return (
            <Spin spinning={this.state.spinning}>
                <div className="profile-wrapper">

                    <p>
                        <span>{lang === "ru" ? "Пользователь: " : "Foydalanuvchi: "}</span>{this.props.userDetail.fullName}
                    </p>
                    <p><span>{lang === "ru" ? "Номер телефона: " : "Telefon raqami: "}</span><NumberFormat
                        format={"(##) ### ## ##"} displayType={"text"}
                        value={this.props.userDetail.phoneNumber} renderText={value => value}/></p>

                    <Button block onClick={this.showHideForm} className={"show-form-btn"}
                            type={"link"}>{lang === "ru" ? "Изменить пароль" : "Parolni o'zgartirish"} <Icon
                        type="down"/></Button>

                    {this.state.showForm ? <WrappedPasswordForm
                        onSubmitPasswords={this.handleSubmitPasswords}
                    /> : ""}

                </div>
            </Spin>
        );
    }
}

export default Profile;


class PasswordForm extends React.Component {

    state = {
        confirmDirty: false,
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log(values);
            if (!err) {
                this.props.onSubmitPasswords(values)
            }
        });
    };


    render() {
        const lang = localStorage.getItem(LANGUAGE);
        const {getFieldDecorator} = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit} className="password-change-form">
                <Form.Item>
                    {getFieldDecorator('oldPassword', {
                        rules: [
                            {
                                required: true,
                                message: lang === "ru" ? "Пожалуйста, введите пароль!" : 'Iltimos, parol kiriting!',
                            },
                        ],
                    })(<Input.Password placeholder={lang === "ru" ? "Ваш старый пароль" : "Eski parolingiz"}
                                       prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}/>)}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('newPassword', {
                        rules: [
                            {
                                required: true,
                                message: lang === "ru" ? "Пожалуйста, введите новый пароль!" : 'Iltimos, yangi parol kiritng!',
                            }
                        ],
                    })(<Input.Password prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                       placeholder={lang === "ru" ? "Новый пароль" : "Yangi parol"}
                    />)}
                </Form.Item>
                <Form.Item>
                    <Button loading={this.props.confirmLoading} type="primary" htmlType="submit"
                            className="send-pasword-button">
                        {lang === "ru" ? "Отправить" : "Yuborish"}
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

const WrappedPasswordForm = Form.create({name: 'normal_login'})(PasswordForm);