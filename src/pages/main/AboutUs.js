import React, {Component} from 'react';

import "./AboutUs.scss"
import {LANGUAGE} from "../../constants";

const lang = localStorage.getItem(LANGUAGE);

class AboutUs extends Component {
    render() {
        return (
            lang === "ru" ?
                <div className={"wrap-about-us"}>
                    <p>Через Интернет магазин <strong>&laquo;</strong><strong>TANTI</strong><strong>&raquo;</strong> Вы сможете купить товары местных производителей Узбекистана и у прямых поставщиков иностранных компании, а также из оптовых рынков <strong>&laquo;</strong><strong>Урикзор</strong><strong>&raquo;</strong><em>(продукты питания, стройматериалы, детские игрушки)</em><strong>, &laquo;</strong><strong>Абу-сахий</strong><strong>&raquo;, &laquo;</strong><strong>Бек барака</strong><strong>&raquo;, &laquo;</strong><strong>Чинни бозор</strong><strong>&raquo;, &laquo;</strong><strong>Солнечный</strong><strong>&raquo;(<em>канцтовары</em>)</strong><strong>, </strong><strong>Стройматериалы</strong><em>(рынки </em><em>&laquo;Изза&raquo;</em><em>,</em><em> &laquo;Жомий&raquo;,&laquo;Куйлюк&raquo;)</em> <strong>и др</strong><strong>.</strong></p>
                    <p>Цены на все товары являются оптовыми.</p>
                    <p>Выбранные Вами товары собираются в Вашей персональной корзинке. За покупки можете заплатить автоматически через карту или наличными при получении товара. Вместе с этим, при отсутствии выбранного товара оператор&nbsp;позвонит, и предложит Вам альтернативный товар.</p>
                    <p>Ваш заказ будет доставлен в указанный срок( начиная с 1 дня)&nbsp; по Вашему адресу или в наш филиал вблизи Вашего проживания. О точном времени доставки товара, заранее Вас оповестят.</p>
                    <p>&nbsp;</p>
                </div> :
                <div className={"wrap-about-us"}>
                    <p><strong>&laquo;</strong><strong>TANTI</strong><strong>&raquo;</strong> Интернет дўконидаги маҳсулотлар, махаллий ишлаб чиқарувчилар, чет-элдан тўғридан-тўғри, <strong>&laquo;</strong><strong>Ўрикзор</strong><strong>&raquo;</strong><em>(озиқ-овқат, қурилиш моллари ва болалар ўйинчоқлари)</em><strong>,</strong> <strong>&laquo;</strong><strong>Абу-сахий</strong><strong>&raquo;,</strong> <strong>&laquo;</strong><strong>Бек барака</strong><strong>&raquo;,</strong> <strong>&laquo;</strong><strong>Чинни бозор</strong><strong>&raquo;,</strong> <strong>&laquo;</strong><strong>Қуёшли</strong><strong>&raquo;</strong><strong>(</strong><strong><em>Солнечный</em></strong> <strong><em>канцтовар</em></strong><strong>)</strong> <strong>ва</strong> <strong>Қурилиш моллари </strong>улгуржи бозорларидан етказилади.</p>
                    <p>Барча маҳсулотлар учун улгуржи харид нархлари белгиланган. Бунда озиқ-овқат маҳсулотлари фақат улгуржи хисобда сотилади. Ноозиқ овқат маҳсулотларини доналаб хам ҳарид қилишингиз мумкин.&nbsp;</p>
                    <p>Интернет дўкондан танлаган маҳсулотингиз, шахсий дўкон саватига йиғилади. Харидни автоматик тарзда пластик картангиз орқали ёки маҳсулотингизни олгандан кейин нақд кўринишида тўлашингиз мумкин. Агар танлаган маҳсулотингизни биронтаси тугаб қолган бўлса, Сиз билан оператор боғланиб ўз таклифини беради.</p>
                    <p>Буюртмангиз, белгиланган муддатда(1 иш кунидан бошланади) Сизни яшаш манзилингиз ёки унга яқин жойдаги, бизнинг филиалга етказилади. Етказишни аниқ вақти ҳақида Сизни олдиндан огохлантирамиз.</p>
                </div>
        );
    }
}

export default AboutUs;