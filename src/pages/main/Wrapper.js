import React, {Component} from 'react';

import {Link, Redirect, Route, Switch, withRouter} from 'react-router-dom';
import {Sugar} from 'react-preloaders';
import AppHeader from "./components/Header/AppHeader";
import {BackTop, Button, Col, Divider, Layout, notification, Row} from "antd";
import MainPage from "./MainPage";
import CartPage from "./CartPage";
import CategoryPage from "./CategoryPage";
import axios from 'axios';
import {ACCESS_TOKEN_USER, API_BASE_URL, LANGUAGE} from "../../constants";

import {addRecently, addRecommended, addCategories, addByCategory} from './redux/actions/productActions'

import {connect} from 'react-redux'
import SignUp from "../signup/SignUp";
import SignInForm from "../signin/SignIn";
import MyOrders from "./MyOrders";
import ProductPage from "./ProductPage";
import ProductViewModal from "./components/ProductViewModal";
import AboutUs from "./AboutUs";
import Agreement from "./Agreement";

import "./Wrapper.scss"
import Profile from "./Profile";
import ForgotPassword from "./ForgotPassword";
import {initAddedItems} from "./redux/actions/cartActions";

const {Footer} = Layout;

const lang = localStorage.getItem(LANGUAGE);

class Wrapper extends Component {
    state = {
        isAuthenticated: false,
        loading: false,
        userDetail: {}

    };

    saveAddedItemsToLocalStorage() {

        localStorage.setItem("added-items", JSON.stringify(this.props.addedItems))
    };

    getAddedItemsFromLocalStorage = () => {
        const addedItems = localStorage.getItem("added-items");

        if (addedItems){
            this.props.initAddedItems(JSON.parse(addedItems))
        }

    };

    componentDidMount() {

        this.getAddedItemsFromLocalStorage();

        this.checkAuth();
        this.fetchRecently();
        this.fetchRecommended();
        this.fetchCategories();

        window.addEventListener(
            "beforeunload",
            this.saveAddedItemsToLocalStorage.bind(this)
        )

    }

    componentWillUnmount() {
        window.removeEventListener(
            "beforeunload",
            this.saveAddedItemsToLocalStorage.bind(this)
        );

        this.saveAddedItemsToLocalStorage();
    }


    checkAuth = () => {
        const token = localStorage.getItem(ACCESS_TOKEN_USER);

        if (token) {

            axios.get(API_BASE_URL + "user/details", {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_USER)
                }
            }).then(res => {
                console.log(res);

                this.setState({
                    isAuthenticated: true,
                    userDetail: res.data
                })


            }).catch(err => {
                if (err.response && err.response.status === 401) {
                    localStorage.removeItem(ACCESS_TOKEN_USER)
                }
                this.setState({
                    loading: false
                })
            });


        } else {
            this.setState({
                loading: false
            });
        }
    };


    handleLogin = (values) => {
        console.log("call!", values);
        this.setState({
            loading: true
        });
        axios.post("http://saxiysavdo.uz:6526/oauth/token", {}, {
            headers: {
                'Authorization': "Basic d2ViOll5NWpydUJtbVdUakREajY=",
            },
            params: {
                "username": values.username,
                "password": values.password,
                "grant_type": values.grant_type
            }
        }).then(res => {
            console.log("res: ", res.data.access_token);
            localStorage.setItem(ACCESS_TOKEN_USER, res.data.access_token);


            this.checkAuth()
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log("err", err);
            const lang = localStorage.getItem(LANGUAGE);
            notification.error({
                message: lang === "ru" ? "Произошло ошибка!" : "Xatolik yuz berdi!",
                description: lang === "ru" ? "Пожалуйста, попробуйте еще раз." : "Iltimos qayta urinib ko'ring."
            })
        })

    };


    fetchRecently = () => {
        const lang = localStorage.getItem(LANGUAGE);
        axios.get(API_BASE_URL + "product/recently", {
            params: {
                "lang": lang === "ru" ? "ru" : "uz"
            }
        }).then(res => {
            this.props.addRecently(res.data)
        }).catch(err => {
            console.log(err)
        })
    };

    fetchRecommended = () => {
        const lang = localStorage.getItem(LANGUAGE);
        axios.get(API_BASE_URL + "product/recommended", {
            params: {
                "lang": lang === "ru" ? "ru" : "uz"
            }
        }).then(res => {
            this.props.addRecommended(res.data)
        }).catch(err => {
            console.log(err)
        })
    };

    fetchCategories = () => {
        axios.get(API_BASE_URL + "category/tree/", {}).then(res => {
            console.log("categories ", res);
            this.props.addCategories(res.data)
        }).catch(err => {
            console.log(err)
        })
    };

    render() {
        return (
            <Layout style={{minHeight: '100vh', backgroundColor: '#fff'}}>
                <ProductViewModal/>
                <AppHeader
                    userDetail={this.state.userDetail}
                    isAuthenticated={this.state.isAuthenticated}/>
                <Sugar color={'#4000ff'}/>
                <BackTop visibilityHeight={200}/>
                <Switch>
                    <Route exact path="/" render={(props) => <MainPage
                        {...props}
                    />}/>
                    <Route path="/cart"
                           render={(props) => this.props.addedItems.length > 0 && this.state.isAuthenticated ? <CartPage
                               {...props}
                           /> : <Redirect to="/"/>}/>

                    <Route path="/sign-in" render={(props) => !this.state.isAuthenticated ? <SignInForm
                            loading={this.state.loading}
                            handleLogin={values => this.handleLogin(values)}
                            {...props}
                        /> :
                        <Redirect to="/"/>}/>
                    <Route path="/sign-up" render={(props) => <SignUp
                        isAuthenticated={this.state.isAuthenticated}
                        onLogin={values => this.handleLogin(values)}
                        {...props}
                    />}/>
                    <Route path="/my-orders" render={(props) => this.state.isAuthenticated ? <MyOrders
                            userDetail={this.state.userDetail}
                            {...props}
                        /> :
                        <Redirect to="/"/>}/>

                    <Route path="/my-profile" render={(props) => this.state.isAuthenticated ? <Profile
                            userDetail={this.state.userDetail}
                            {...props}
                        /> :
                        <Redirect to="/"/>}/>
                    <Route path="/category/:id" render={(props) => <CategoryPage
                        {...props}
                    />}/>
                    <Route path="/p-category/:id" render={(props) => <CategoryPage
                        {...props}
                    />}/>
                    <Route path="/forgot-password" render={(props) => <ForgotPassword
                        {...props}
                    />}/>
                    <Route path="/about-us" render={(props) => <AboutUs
                        {...props}
                    />}/>
                    <Route path="/using-agreement" render={(props) => <Agreement
                        {...props}
                    />}/>
                </Switch>
                <Footer className={"footer"}>
                    <div className={"footer-wrap"}>
                    <Row gutter={16}>
                        <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                            <img className={"footer-logo"} src="http://tanti.uz:6526/api/v1/downloadFile/185057photo_2020-07-15_23-19-14.jpg" alt="tanti"/>
<p className={"info-text-footer"}>TANTI.uz: {lang === "ru" ? "Доставка производится по всему Узбекистану" : "Yetkazib Berish O'zbekiston bo'yicha amalga oshiriladi"}</p>
                        </Col>
                        <Col xs={24} sm={24} md={12} lg={8} xl={8}>

                            <Row>
                                <Col xs={24} sm={12} md={24} lg={24} xl={24}>
                            <Button type="link" size={"large"}>
                                <Link to={"/about-us"}>
                                {lang === "ru" ? "О нас":"Biz haqimizda"}
                                </Link>
                            </Button>
                                </Col>
                                <Col xs={24} sm={12} md={24} lg={24} xl={24}>
                            <Button type="link" size={"large"}>
                                <Link to={"/using-agreement"}>
                                {lang === "ru" ? "Публичная оферта":"Ommaviy oferta"}
                                </Link>
                            </Button>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={24} sm={24} md={12} lg={8} xl={8}>
                            <p>{lang === "ru" ? "Скачайте мобильное приложение TANTI и совершайте покупки удобно.":"TANTI mobil ilovasini yuklab oling va xaridlaringnizni qulay amalga oshiring."}</p>
                            <a target={"_blank"} href="https://play.google.com/store/apps/details?id=uz.ms.saxiysavdo">
                                <img className={"google-play-link"} src="http://tanti.uz:6526/api/v1/downloadFile/142738googleplay.png" alt="google play"/>
                            </a>
                        </Col>
                    </Row>
                    </div>
                    <br />
                    <br />

                    <Divider>TANTI ©2020</Divider>
                </Footer>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        addedItems: state.addedItems,
    }
};

const mapDispatchToProps = (dispatch) => {

    return {
        addRecommended: (products) => {
            dispatch(addRecommended(products))
        },
        addRecently: (products) => {
            dispatch(addRecently(products))
        },
        addCategories: (categories) => {
            dispatch(addCategories(categories))
        },
        addByCategory: (products) => {
            dispatch(addByCategory(products))
        },
        initAddedItems: (products) => {
            dispatch(initAddedItems(products))
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Wrapper));