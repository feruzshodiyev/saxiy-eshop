import React, {Component} from 'react';

import axios from 'axios';
import {ACCESS_TOKEN_USER, API_BASE_URL, LANGUAGE} from "../../constants";
import {Button, Card, Descriptions, Layout, List, Spin, Tag} from "antd";
import "./MyOrders.scss";
import NumberFormat from "react-number-format";
import OrderModal from "../../components/OrderModal";
import moment from "moment";


const {Content} = Layout;

class MyOrders extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            orders: [],
            modalVisible: false,
            viewProducts: [],
        }
    }

    componentDidMount() {
        this.fetchOrders();
    }


    fetchOrders = () => {

        const lang = localStorage.getItem(LANGUAGE);

        this.setState({
            loading: true
        });
        axios.get(API_BASE_URL + "order", {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_USER)
            },
            params: {
                "lang": lang === "ru" ? "ru" : "uz"
            }
        }).then(res => {
            console.log(res);
            this.setState({
                loading: false,
                orders: res.data
            });
        }).catch(err => {
            this.setState({
                loading: false
            });
        })

    };

    closeModal = () => {
        this.setState({
            modalVisible: false
        })
    };

    openModal = (products) => {
        this.setState({
            modalVisible: true,
            viewProducts: products
        })
    };

    render() {
        const lang = localStorage.getItem(LANGUAGE);
        return (
            <Layout className={"oreder-layput"}>
                <OrderModal
                    visible={this.state.modalVisible}
                    handleClose={this.closeModal}
                    products={this.state.viewProducts}
                />
                <Content className="my-order-content">
                    <Spin spinning={this.state.loading}>
                        <h2 className="title-orders">{lang === "ru" ? "Мои заказы" : "Buyurtmalarim"}</h2>
                        <List
                            header={<Descriptions
                                className={"list-header"}
                                column={{xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1}}
                            >
                                <Descriptions.Item
                                    label={lang === "ru" ? "Заказчик" : "Buyurtmachi"}>{this.props.userDetail.fullName}</Descriptions.Item>
                                <Descriptions.Item
                                    label={lang === "ru" ? "Номер телефона" : "Telefon raqami"}><NumberFormat
                                    format={"(##) ### ## ##"} displayType={"text"}
                                    value={this.props.userDetail.phoneNumber}/></Descriptions.Item>
                            </Descriptions>}
                            split={false}
                            itemLayout="horizontal"
                            dataSource={this.state.orders}
                            renderItem={item => (
                                <List.Item>
                                    <Card className={"order-card"}>

                                        <Descriptions
                                            title={<div className={"order-title-wrapper"}>
                                                <p><span>{lang === "ru" ? "Общая стоимость: " : "Jami narxi: "}</span> <NumberFormat value={item.totalCost.totalCost} displayType={'text'}
                                                                 thousandSeparator={' '}
                                                                 suffix={lang === "ru" ? " сум" : " so'm"}/></p>
                                                <p><span>{lang === "ru" ? "Дата: " : "Sana: "}</span>{moment(item.createdDate).format("L")}</p>
                                                <p><span>{lang === "ru" ? "Дата изменения: " : "O'zgratirilgan sanasi: "}</span>{moment(item.modifiedDate).format("L")}</p>

                                            </div>}
                                            bordered
                                            column={{xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1}}
                                        >
                                            <Descriptions.Item
                                                label={lang === "ru" ? "Магазин" : "Do'kon"}>{item.market}</Descriptions.Item>
                                            <Descriptions.Item
                                                label={lang === "ru" ? "Продавец" : "Sotuvchi"}>{item.seller.fullName}</Descriptions.Item>
                                            <Descriptions.Item
                                                label={lang === "ru" ? "Номер телефона" : "Telefon raqami"}><NumberFormat
                                                format={"(##) ### ## ##"} displayType={"text"}
                                                value={item.seller.phoneNumber}/></Descriptions.Item>
                                            <Descriptions.Item
                                                label={lang === "ru" ? "Способ оплаты" : "To'lov turi"}>{item.paymentType === 0 ? lang === "ru" ? "Наличными" : "Нақд" : lang === "ru" ? "Через «Click»" : "«Click» orqali"}</Descriptions.Item>

                                            {item.paymentType === 0 ? "" :
                                                <Descriptions.Item
                                                    label={lang === "ru" ? "Номер платежа" : "To'lov raqami"}>{item.paymentId}</Descriptions.Item>}

                                            <Descriptions.Item
                                                label={lang === "ru" ? "Адрес доставки" : "Yetkazish manzili"}>{item.address.name}</Descriptions.Item>
                                            <Descriptions.Item
                                                label={lang === "ru" ? "Время доставки" : "Yetkazish vaqti"}>{item.address.deliveryMessage}</Descriptions.Item>
                                            <Descriptions.Item
                                                label={lang === "ru" ? "Стоимость доставки" : "Yetkazish narxi"}>{item.address.price}</Descriptions.Item>
                                            <Descriptions.Item label={lang === "ru" ? "Статус" : "Holati"}><Tag
                                                color={item.status === "OPEN" ? "green" : "red"}>
                                                {item.status}
                                            </Tag></Descriptions.Item>
                                            <Descriptions.Item
                                                label={lang === "ru" ? "Список товаров" : "Tovarlar ro'yxati"}>
                                                <Button type={"primary"}
                                                        onClick={() => this.openModal(item.orderedProductsDto)}>{lang === "ru" ? "Просмотр" : "Ko'rish"}</Button>

                                            </Descriptions.Item>
                                        </Descriptions>
                                    </Card>
                                </List.Item>
                            )}
                        />
                    </Spin>
                </Content>
            </Layout>
        );
    }
}

export default MyOrders;