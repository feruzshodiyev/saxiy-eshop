import React, {Component} from 'react';

import {Card, Button, Carousel as CaruselAntd, Row, Col, Modal, Icon} from 'antd';
import './Home.scss'

import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

import {addQuantity, addToCart, subtractQuantity, initAddedItems} from './redux/actions/cartActions'

import {connect} from 'react-redux'
import Categories from "./components/Categories";
import axios from "axios";
import {API_BASE_URL, LANGUAGE} from "../../constants";
import NumberFormat from "react-number-format";

import {showModalProduct, addSelected, addRelated, loadingModal} from "./redux/actions/productActions";

const {Meta} = Card;
const ButtonGroup = Button.Group;
const responsive = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: {max: 4000, min: 1200},
        items: 5
    },
    desktop: {
        breakpoint: {max: 1200, min: 950},
        items: 4,
    },
    tablet: {
        breakpoint: {max: 950, min: 686},
        items: 3,
    },
    tabletSmall: {
        breakpoint: {max: 686, min: 425},
        items: 2,
    },
    mobile: {
        breakpoint: {max: 425, min: 0},
        items: 1,
    },
};

const lang = localStorage.getItem(LANGUAGE);

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            advertise: [],
            slides: [],
            categories: [],
            logos: [],
            viewProduct: false,
            selectedProduct: {},
            selectedProductSrc: null,
            modalLoading: false

        }
    }




    componentDidMount() {
        this.fetchSlides();
        this.fetchAdvertise();
        this.fetchLogos();

    }




    fetchSlides = () => {
        axios.get(API_BASE_URL + "slider").then(res => {
            console.log("sliders: ", res);
            this.setState({
                slides: res.data
            })
        }).catch(err => {
            console.log(err)
        })
    };

    fetchAdvertise = () => {
        axios.get(API_BASE_URL + "advertise").then(res => {
            this.setState({
                advertise: res.data
            })
        }).catch(err => {
            console.log(err)
        })
    };
    fetchLogos = () => {
        axios.get(API_BASE_URL + "market-logo").then(res => {
            this.setState({
                logos: res.data
            })
        }).catch(err => {
            console.log(err)
        })
    };


    handleAddToCart = (id, src) => {

        this.props.addToCart(id, src);

    };

    showProductModal = (item) => {
        this.props.loadingModal(true);
        this.props.showModalProduct(true);
        this.props.addSelected(item);

        this.fetchRelatedProducts(item.category)
    };

    fetchRelatedProducts = (id) => {
        const lang = localStorage.getItem(LANGUAGE);
        axios.get(API_BASE_URL + "product/category/" + id, {
            params: {
                "lang": lang === "ru" ? "ru" : "uz"
            }
        }).then(res => {

            console.log(res);
            this.props.addRelated(res.data);
            this.props.loadingModal(false);

        }).catch(err => {
            this.props.loadingModal(false);

        })
    };

    handleSubQuantity = (id) => {
        this.props.subtractQuantity(id);
    };

    handleAddQuantity = (id) => {
        this.props.addQuantity(id);
    };

    showAgreedModal=(sellerPhone)=>{
        Modal.info({
            title: <p style={{fontSize:15,textAlign:"center"}}>{lang === "ru" ? "Для заказа данного товара обратитесь по нижеуказанному номеру!":
                "Ushbu tovarga buyurtma berish uchun quyidagi raqamga murojaat qiling!"}</p>,
            content: (
                <div>
                    <a href={"tel:+998"+sellerPhone}><p style={{fontSize:18}}><Icon type="phone"/> (+998)
                    <NumberFormat value={sellerPhone} displayType={'text'} format=" ## ### ##-##" /></p></a>
                </div>
            ),
            onOk() {},
        });
    };

    render() {


        const caruselItems = this.state.slides.map(item => {
            return (
                <div className={"slider-image-container"} key={item.id}>
                    <img className="slider-image"
                         src={item.imgUrl}/>
                </div>
            )
        });

        let recommendedProductsItemList = this.props.recommendedProducts.map(item => {
            return (
                <div className={"carusel-product-item"} key={item.id}>
                    <Card

                        hoverable
                        className="carusel-product-card"
                        cover={
                            <div className="product-image-container">
                                <img
                                    onClick={() => this.showProductModal(item)}
                                    className="product-image"
                                    alt="product"
                                    src={item.img}
                                />
                            </div>
                        }
                        actions={[
                            item.quantity ?
                                <ButtonGroup>
                                    <Button disabled={item.quantity === 1}
                                            onClick={() => this.handleSubQuantity(item.id)} type="primary" shape="round"
                                            icon={"minus"}/>
                                    <Button disabled shape="round">{item.quantity}</Button>
                                    <Button onClick={() => this.handleAddQuantity(item.id)} type="primary" shape="round"
                                            icon={"plus"}/>
                                </ButtonGroup>
                                :
                                <Button onClick={() => item.price==1 ? this.showAgreedModal(item.sellerPhone):  this.handleAddToCart(item.id, 2)} type="primary"
                                        icon="shopping-cart">{lang === "ru" ? "Добавить в корзину" : " Savatga qo'shish "}</Button>
                        ]}
                    >
                        <Meta
                            onClick={() => this.showProductModal(item)}
                            title={item.name}
                            description={<div>
                                <p>{item.categoryName}</p>
                                {item.price==1 ?<span style={{marginLeft: 10, fontSize: "large", color: "#0b27ff"}}>{lang === "ru" ? "Цена договорная":"Kelishilgan narx"}</span>:
                                    <NumberFormat style={{marginLeft: 10, fontSize: "large", color: "#0b27ff"}}
                                                                 value={item.price} displayType={'text'}
                                                                 thousandSeparator={' '} suffix={lang === "ru" ? " сум" : " so'm"}/> }

                            </div>}
                        />
                    </Card>
                </div>
            )
        });

        let recentlyProductsItemList = this.props.recentlyProducts.map(item => {
            return (
                <div className={"carusel-product-item"} key={item.id}>
                    <Card
                        hoverable
                        className="carusel-product-card"
                        cover={
                            <div className="product-image-container">
                                <img
                                    onClick={() => this.showProductModal(item)}
                                    className="product-image"
                                    alt="product"
                                    src={item.img}
                                />
                            </div>
                        }
                        actions={[
                            item.quantity ?
                                <ButtonGroup>
                                    <Button disabled={item.quantity === 1}
                                            onClick={() => this.handleSubQuantity(item.id)} type="primary" shape="round"
                                            icon={"minus"}/>
                                    <Button disabled shape="round">{item.quantity}</Button>
                                    <Button onClick={() => this.handleAddQuantity(item.id)} type="primary" shape="round"
                                            icon={"plus"}/>
                                </ButtonGroup>
                                :
                                <Button onClick={() => item.price==1 ? this.showAgreedModal(item.sellerPhone):  this.handleAddToCart(item.id, 1)} type="primary"
                                        icon="shopping-cart">{lang === "ru" ? "Добавить в корзину" : " Savatga qo'shish "}</Button>
                        ]}
                    >
                        <Meta
                            onClick={() => this.showProductModal(item)}
                            title={item.name}
                            description={<div>
                                <p>{item.categoryName}</p>

                                {item.price==1 ?<span style={{marginLeft: 10, fontSize: "large", color: "#0b27ff"}}>{lang === "ru" ? "Цена договорная":"Kelishilgan narx"}</span>:
                                    <NumberFormat style={{marginLeft: 10, fontSize: "large", color: "#0b27ff"}}
                                                  value={item.price} displayType={'text'}
                                                  thousandSeparator={' '} suffix={lang === "ru" ? " сум" : " so'm"}/> }

                            </div>}
                        />
                    </Card>
                </div>
            )
        });

        return (

            <div style={{maxWidth: 1400, margin: 'auto'}}>
                <div className="carousel-desctop">
                    <Row style={{backgroundColor: '#fff'}} gutter={[24, 24]} type="flex" justify="space-around">
                        <Col span={5}>
                            <Categories/>
                        </Col>
                        <Col span={11}>
                            <div className="market-logo-wrapper">
                                <Row>
                                    <Col span={4}>
                                        {this.state.logos.length > 0 ?
                                            <img className={"market-logo-img"}
                                                 src={this.state.logos[0].photo}/> : ""}

                                    </Col>
                                    <Col span={4}>
                                        {this.state.logos.length > 1 ?
                                            <img className={"market-logo-img"}
                                                 src={this.state.logos[1].photo}/> : ""}
                                    </Col>
                                    <Col span={4}>
                                        {this.state.logos.length > 2 ?
                                            <img className={"market-logo-img"}
                                                 src={this.state.logos[2].photo}/> : ""}
                                    </Col>
                                    <Col span={4}>
                                        {this.state.logos.length > 3 ?
                                            <img className={"market-logo-img"}
                                                 src={this.state.logos[3].photo}/> : ""}
                                    </Col>
                                    <Col span={4}>
                                        {this.state.logos.length > 4 ?
                                            <img className={"market-logo-img"}
                                                 src={this.state.logos[4].photo}/> : ""}
                                    </Col>
                                    <Col span={4}>
                                        {this.state.logos.length > 5 ?
                                            <img className={"market-logo-img"}
                                                 src={this.state.logos[5].photo}/> : ""}
                                    </Col>
                                </Row>
                            </div>
                            <CaruselAntd autoplay>
                                {caruselItems}
                            </CaruselAntd>
                        </Col>
                        <Col span={8}>
                            <div className="market-logo-wrapper">

                            </div>
                            {this.state.advertise.length > 0 ?
                                <img
                                    className={"advertise-img"}

                                    src={this.state.advertise[0].imgUrl}/> : ""
                            }
                        </Col>
                    </Row>
                </div>
                <div className={"carousel-mobile"}>
                    <CaruselAntd arrows={true} autoplay>
                        {caruselItems}
                    </CaruselAntd>
                </div>

                <div>

                    <div className="product-carousel-title">
                        <h2>{lang === "ru" ? "Рекомендованные товары" : "Tavsiya qilingan mahsulotlar"}</h2></div>

                    <Carousel
                        className="carousel-products"
                        showDots={false}
                        swipeable={true}
                        infinite={true}
                        autoPlay={true}
                        responsive={responsive}
                        removeArrowOnDeviceType={["tablet", "tabletSmall", "mobile", "desktop","superLargeDesktop"]}>

                        {recommendedProductsItemList}
                    </Carousel>
                </div>
                <div>
                    <div className="product-carousel-title">
                        <h2>{lang === "ru" ? "Новинки" : "Yangi qo'shilgan mahsulotlar"}</h2></div>

                    <Carousel
                        className="carousel-products"
                        showDots={false}
                        infinite={true}
                        autoPlay={true}
                        responsive={responsive}
                        removeArrowOnDeviceType={["tablet", "tabletSmall", "mobile", "desktop","superLargeDesktop"]}>
                        {recentlyProductsItemList}
                    </Carousel>
                </div>

            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        recentlyProducts: state.recentlyProducts,
        recommendedProducts: state.recommendedProducts,
    }
};

const mapDispatchToProps = (dispatch) => {

    return {
        addToCart: (id, src) => {
            dispatch(addToCart(id, src))
        },
        showModalProduct: (showModal) => {
            dispatch(showModalProduct(showModal))
        },
        addSelected: (product) => {
            dispatch(addSelected(product))
        },
        addRelated: (products) => {
            dispatch(addRelated(products))
        },
        loadingModal: (loading) => {
            dispatch(loadingModal(loading))
        },
        addQuantity: (id) => {
            dispatch(addQuantity(id))
        },
        subtractQuantity: (id) => {
            dispatch(subtractQuantity(id))
        }

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);