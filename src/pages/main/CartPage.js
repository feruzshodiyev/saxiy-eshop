import React, {Component} from 'react';

import "./CartPage.scss"
import {
    Avatar,
    Button,
    Card,
    Col,
    Collapse, Form,
    Icon,
    Modal,
    Layout,
    List,
    notification,
    Row,
    Select,
    Spin,
    Statistic, Alert
} from "antd";
import axios from "axios";
import {ACCESS_TOKEN, ACCESS_TOKEN_USER, API_BASE_URL, LANGUAGE} from "../../constants";

import {connect} from "react-redux";
import {clearCart} from "./redux/actions/cartActions"
import NumberFormat from "react-number-format";
import MaskedInput from "antd-mask-input";

const {Content} = Layout;
const {Option} = Select;

const lang = localStorage.getItem(LANGUAGE);

class CartPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            points: [],
            districts: [],
            regions: [],
            selectedRegionId: null,
            selectedDistrictId: "def",
            selectedPointId: "def",

            pointPrice: 0,
            deliveryMessage: "",

            key: "tab1",

            addedProducts: [],
            total: 0,

            amount: {},
            products: [],

            calculate: {},

            paymentLoading: false,
            verifyLoading: false,

            cardToken: '',
            showCardForm: true,
            showVerifyForm: false,
            showEndingContent: false

        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.initProducts();

        this.fetchRegions();
    }


    initProducts = () => {

        const addedProducts = this.props.addedItems;
        const total = this.props.total;

        let amount = {};
        let products = [];

        addedProducts.map(item => {
            amount[item.id] = item.quantity;

            products.push(item.id)
        });

        this.setState({
            addedProducts: addedProducts,
            total: total,
            amount: amount,
            products: products
        });

        console.log("addedProducts", addedProducts);

        console.log("amount", amount);

        console.log("products", products);

    };


    calculate = () => {
        if (this.state.selectedPointId !== "def") {

            this.setState({
                loading: true
            });

            axios.post(API_BASE_URL + "order/calculate-delivery-cost", {
                "amount": this.state.amount,
                "deliveryPointId": this.state.selectedPointId,
                "product": this.state.products

            }, {}).then(res => {
                console.log(res);
                this.setState({
                    calculate: res.data,
                    loading: false
                })
            }).catch(err => {
                notification.error({
                    message: lang==="ru"? "Ошибка в расчете стоимости.":"Xarajatlarni hisoblashda xatolik.",
                    description: lang==="ru"? "Проверьте подключение к интернету!":"Internet aloqasini tekshiring!"

                });
                this.setState({
                    loading: false
                })

            })
        }

    };

    fetchRegions = () => {

        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "region", {
            params: {
                "lang":lang === "ru" ? "ru": "uz"
            }
        }).then(res => {

            this.setState({
                regions: res.data,
                loading: false
            });

            console.log(res)
        }).catch(err => {
            this.setState({
                loading: false
            });
            notification.error({
                message: lang==="ru"? "Вилаяты не пришли с сервера.":"Viloyatlar serverdan kelmadi.",
                description: lang==="ru"? "Проверьте подключение к интернету!":"Internet aloqasini tekshiring!"

            });
            console.log(err)
        })
    };


    fetchDistricts = () => {
        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "district/ap/dist-id/" + this.state.selectedRegionId, {
            // headers: {
            //     'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            // }
        }).then(res => {
            this.setState({
                districts: res.data,
                loading: false
            });
            console.log(res)
        }).catch(err => {
            this.setState({
                districts: [],
                loading: false
            });
            notification.error({
                message: lang==="ru"? "Районы не пришли с сервера.":"Tumanlar serverdan kelmadi.",
                description: lang==="ru"? "Проверьте подключение к интернету!":"Internet aloqasini tekshiring!"

            });

            console.log(err)
        })
    };

    fetchPoints = () => {
        if (this.state.selectedDistrictId !== 'def') {

            this.setState({
                loading: true
            });

            axios.get(API_BASE_URL + "delivery-point/district/ap/" + this.state.selectedDistrictId, {}).then(res => {
                this.setState({
                    points: res.data,
                    loading: false
                });
                console.log(res)
            }).catch(err => {
                this.setState({
                    loading: false
                });
                console.log(err)
            })
        }

    };


    handleChangeRegion = (value) => {
        this.setState({
            selectedRegionId: value,
            selectedDistrictId: "def"
        }, () => this.fetchDistricts());

    };


    handleChangeDistrict = (value) => {
        this.setState({
            selectedDistrictId: value,
            selectedPointId: 'def'
        }, () => {
            this.fetchPoints();
        })
    };

    handleChangePoint = (value) => {

        let pointPrice = 0;
        let deliveryMessage = "";

        this.state.points.map(item => {

            if (item.id.toString() === value) {
                pointPrice = item.price;
                deliveryMessage = lang==="ru"? item.deliveryMessageRu : item.deliveryMessageUz
            }

        });

        this.setState({
            selectedPointId: value,
            pointPrice: pointPrice,
            deliveryMessage: deliveryMessage
        }, () => {
            this.calculate();
        })

    };


    onTabChange = (key, type) => {
        console.log(key, type);
        this.setState({[type]: key});
    };


    submitPayForm = (values) => {
        this.setState({
            paymentLoading: true
        });

        const cardNumber = values.cardNumber.replace(/\s/g, "");
        const expDate = values.expDate.replace("/", "");

        console.log(cardNumber);
        console.log(expDate);

        let formData = new FormData();

        formData.append("cardNumber", cardNumber);
        formData.append("expDate", expDate);

        if (cardNumber.length === 16 && expDate.length === 4) {
            axios.post(API_BASE_URL + "payment/card/create", formData, {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_USER),
                }
            }).then(res => {
                console.log(res);
                this.setState({
                    paymentLoading: false,
                    cardToken: res.data.card_token,
                    showCardForm: false,
                    showVerifyForm: true
                });
            }).catch(err => {

                this.setState({
                    paymentLoading: false
                });

                notification.error({
                    message: lang==="ru"? "Произошла ошибка!":"Xatolik yuz berdi!",
                    description: lang==="ru"? "Пожалуйста, попробуйте еще раз.": "Iltimos qayta urinib ko‘ring."
                })
            })
        } else {

            notification.error({
                message: lang==="ru"? "Номер карты или срок действия введены неверно!":"Karta raqmi yoki muddati xato kiritildi!"
            });

            this.setState({
                paymentLoading: false
            });
        }


    };

    submitVerifyForm = (values) => {

        this.setState({
            verifyLoading: true
        });

        let formData = new FormData();

        formData.append("cardToken", this.state.cardToken);
        formData.append("code", values.code);

        axios.post(API_BASE_URL + "payment/card/verify", formData, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_USER),
            }
        }).then(res => {
            console.log(res);
            this.setState({
                verifyLoading: false,
                showEndingContent: true,
                showVerifyForm: false

            });
        }).catch(err => {

            this.setState({
                verifyLoading: false
            });

            notification.error({
                message: lang==="ru"? "Произошла ошибка!":"Xatolik yuz berdi!",
                description: lang==="ru"? "Пожалуйста, попробуйте еще раз.": "Iltimos qayta urinib ko‘ring."
            })
        })
    };

    checkoutPaper = () => {
        if (this.state.selectedPointId !== 'def' && this.state.calculate) {
            this.setState({
                loading: true
            });

            const mti = new Date().toLocaleString().replace(/\s/g, "");

            axios.post(API_BASE_URL + "order", {
                "amount": this.state.amount,
                "deliveryPointId": this.state.selectedPointId,
                "paymentType": 0,
                "merchantTransId": mti,
                "product": this.state.products

            },{
                headers:{
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_USER),
                }
            }).then(res=>{
                console.log(res);
                localStorage.removeItem("added-items");
                this.props.clearCart();
                Modal.success({
                    title: lang==="ru"? "Спасибо за покупку!" : "Xaridingiz uchun tashakkur!",
                    content: lang==="ru"? "Ваши товары будут доставлены в указанный срок. При возникновении вопросов касательно покупок, оператор свяжется с Вами ." : 'Tanlagan mahsulot(lar)ingiz belgilangan muddatda yetkaziladi. Qo\'shimcha savol tugilsa operator siz bilan bog\'lanadi.',
                    onOk() {
                        window.location.reload();
                    }
                });
            }).catch(err=>{
                console.log(err);
                notification.error({
                    message:lang==="ru"? "Произошла ошибка при обработке вашего заказа!" : "Buyurtmani saqlashda xatolik ro‘y berdi!",
                    description:lang==="ru"? "Пожалуйста, проверьте ваше интернет-соединение и попробуйте снова." : "Iltimos, internet aloqasini tekshirib qayta urinib ko‘ring."
                });
                this.setState({
                    loading: false
                });
            })

        } else {
            notification.error({
                message: this.state.selectedPointId === 'def' ? lang==="ru"? "Место доставки не выбрано!" :  "Yetkazish joyi tanlanmagan!" : lang==="ru"? "Общая сумма к оплате еще не рассчитана!" :  "Jami to‘lov hisoblanmagan!"
            })
        }
    };
    checkoutElectron=()=>{
        if (this.state.selectedPointId !== 'def' && this.state.calculate) {
            this.setState({
                loading: true
            });


            const mti = new Date().toLocaleString().replace(/\s/g, "");


            axios.post(API_BASE_URL + "order", {
                "amount": this.state.amount,
                "deliveryPointId": this.state.selectedPointId,
                "paymentType": 1,
                "merchantTransId": mti,
                "product": this.state.products,
                "cardToken": this.state.cardToken

            },{
                headers:{
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_USER),
                }
            }).then(res=>{
                console.log(res);
                localStorage.removeItem("added-items");
                this.props.clearCart();
                Modal.success({
                    title: lang==="ru"? "Спасибо за покупку!" : "Xaridingiz uchun tashakkur!",
                    content: lang==="ru"? "Ваши товары будут доставлены в указанный срок. При возникновении вопросов касательно покупок, оператор свяжется с Вами ." : 'Tanlagan mahsulot(lar)ingiz belgilangan muddatda yetkaziladi. Qo\'shimcha savol tugilsa operator siz bilan bog\'lanadi.',
                    onOk() {
                        window.location.reload();
                    }
                });
            }).catch(err=>{
                console.log(err);
                notification.error({
                    message: lang==="ru"? "Произошла ошибка при обработке вашего заказа!" : "Buyurtmani saqlashda xatolik ro‘y berdi!",
                    description: lang==="ru"? "Пожалуйста, проверьте ваше интернет-соединение и попробуйте снова." : "Iltimos, internet aloqasini tekshirib qayta urinib ko‘ring."
                });
                this.setState({
                    loading: false
                });
            })



        } else {
            notification.error({
                message: this.state.selectedPointId === 'def' ? lang==="ru"? "Место доставки не выбрано!" :  "Yetkazish joyi tanlanmagan!" : lang==="ru"? "Общая сумма к оплате еще не рассчитана!" :  "Jami to‘lov hisoblanmagan!"
            })
        }
    };

    cancelVerify=()=>{
        this.setState({
            cardToken:"",
            showVerifyForm: false,
            showCardForm: true,
        })
    };

    cancelPayment=()=>{
        this.setState({
            cardToken:"",
            showVerifyForm: false,
            showEndingContent: false,
            showCardForm: true,
        })
    };

    render() {

        const tabList = [
            {
                key: 'tab1',
                tab: <img width={80} src={"http://saxiysavdo.uz:6526/api/v1/downloadFile/153045bycash.png"} alt={"cash"}/>,
            },
            {
                key: 'tab2',
                tab: <img width={80} src={"http://saxiysavdo.uz:6526/api/v1/downloadFile/258081logo.png"} alt={"click"}/>,
            },
        ];

        const deliveryInfo = (
            this.state.calculate.totalDeliveryCost ? <div className={"delivery-info"}>
                    <Alert
                        message={lang === "ru" ? "ЗАМЕТКА!":"ESLATMA!"}
                        description={this.state.deliveryMessage}
                        type="info"
                        showIcon
                    />
                </div>:
                <div className={"delivery-info"}>
                    <Alert message={lang === "ru" ? "Вы не выбрали пункт доставки!":"Siz yetkazish nuqtasini tanlamadingiz!"} type="error" />
                </div>

        );

        const deliveryAlertInfo = (
            this.state.calculate.totalDeliveryCost ? <div>

                </div>:
                <Alert message={lang === "ru" ? "Вы не выбрали пункт доставки!":"Siz yetkazish nuqtasini tanlamadingiz!"} type="error" />
        );

        const contentList = {
            tab1: <div className={"payment-card-content"}>
                <h3>{lang === "ru" ? "Оплата наличными." :"Naqd shaklda to'lash."}</h3>

                <Statistic title={lang === "ru" ? "Всего к оплате" :"Jami to‘lanishi lozim"} groupSeparator={" "}
                           value={this.state.calculate.totalDeliveryCost ? this.state.calculate.totalDeliveryCost+this.state.total : 0}
                           suffix={lang === "ru" ? " сум" :" so'm"}/>

                {deliveryAlertInfo}

                <Button onClick={this.checkoutPaper} className={"checkout-btn"} type="primary" shape="round"
                        icon="shopping-cart" size={"large"}>
                    {lang === "ru" ? "Оформить заказ" :"Buyurtmani yuborish"}
                </Button>

            </div>,
            tab2: <div className={"payment-card-content"}>
                <h3>{lang === "ru" ? "Оплата через платежную систему CLICK." :"CLICK to'lov tizimi orqali to'lash."}</h3>
                {this.state.showCardForm ?
                    <PaymentFormWrap
                        loading={this.state.paymentLoading}
                        handleSubmit={values => this.submitPayForm(values)}
                    />:""}
                {this.state.showVerifyForm ?
                    <VerifyCodeFormWrap
                        onCancel={()=>this.cancelVerify()}
                        loading={this.state.verifyLoading}
                        handleSubmit={values => this.submitVerifyForm(values)}
                    /> : ""}

                {this.state.showEndingContent ? <div>
                    <Statistic title={lang === "ru" ? "Всего к оплате" :"Jami to‘lanishi lozim"} groupSeparator={" "}
                               value={this.state.calculate.totalDeliveryCost ? this.state.calculate.totalDeliveryCost+this.state.total : 0}
                               suffix={lang === "ru" ? " сум" :" so'm"}/>

                    {deliveryAlertInfo}

                    <Button onClick={()=>this.checkoutElectron()} className={"checkout-btn"} type="primary" shape="round"
                            icon="shopping-cart" size={"large"}>
                        {lang === "ru" ? "Оформить заказ" :"Buyurtmani yuborish"}
                    </Button>
                </div> : ""}

            </div>,
        };


        return (
            <Layout>
                <Content className="cart-content">
                    <Spin spinning={this.state.loading}>
                        <Row>
                            <Col xs={{span: 24}} lg={{span: 17}}>
                                <Card
                                    className="point-card"
                                    type="inner"
                                    title={<p>{lang === "ru" ? "Выбрать адрес доставки":"Yetkazish manzilini kiritish"} <span style={{color:'red'}}>*</span></p>}

                                >
                                    <Row>
                                        <Col xs={{span: 24}} lg={{span: 8}}>
                                            <Select
                                                className={"select-address"}
                                                placeholder={lang === "ru" ? "Выберите регион" :"Viloyatni tanlang"}
                                                onChange={this.handleChangeRegion}
                                                style={{width: "90%"}}
                                            >
                                                {this.state.regions.map(item => (
                                                    <Option key={item.id}>{item.name}</Option>
                                                ))}
                                            </Select>
                                        </Col>
                                        <Col xs={{span: 24}} lg={{span: 8}}>
                                            <Select
                                                className={"select-address"}
                                                placeholder={lang === "ru" ? "Выберите район" :"Tuman tanlang"}
                                                onChange={this.handleChangeDistrict}
                                                value={this.state.selectedDistrictId}
                                                style={{width: "90%"}}
                                            >
                                                <Option key={"def"}>{lang === "ru" ? "Выберите район" :"Tuman tanlang"}</Option>
                                                {this.state.districts.map(item => (
                                                    <Option key={item.id}>{lang === "ru" ?item.nameRu:item.nameUz}</Option>
                                                ))}
                                            </Select>
                                        </Col>
                                        <Col xs={{span: 24}} lg={{span: 8}}>
                                            <Select
                                                className={"select-address"}
                                                // placeholder={"Етказиш жойини танланг"}
                                                onChange={this.handleChangePoint}
                                                style={{width: "90%"}}
                                                value={this.state.selectedPointId}
                                            >
                                                <Option key={"def"}>{lang === "ru" ? "Выберите место доставки" :"Yetkazish joyini tanlang"}</Option>
                                                {this.state.points.map(item => (
                                                    <Option key={item.id}>{lang === "ru" ?item.nameRu:item.nameUz}</Option>
                                                ))}
                                            </Select>
                                        </Col>
                                    </Row>
                                    {deliveryInfo}
                                </Card>

                                <Card
                                    className="point-card"
                                    type="inner"
                                    title={lang === "ru" ? "Расходы" :"Xarajatlar tarkibi"}
                                >
                                    <Row>
                                        <Col xs={{span: 24}} lg={{span: 8}}>
                                            <Statistic title={lang === "ru" ? "Jami mahsulotlar narxi" :"Общая цена продукта"} groupSeparator={" "}
                                                       value={this.state.total} suffix={lang === "ru" ? " сум" :" so'm"}/>

                                        </Col>
                                        <Col xs={{span: 24}} lg={{span: 8}}>
                                            <Statistic title={lang === "ru" ? "Стоимость доставки":"Yetakzish xarajatlari"} groupSeparator={" "}
                                                       value={this.state.calculate.totalDeliveryCost} suffix={lang === "ru" ? " сум" :" so'm"}/>

                                        </Col>
                                        <Col xs={{span: 24}} lg={{span: 8}}>
                                            <Statistic title={lang === "ru" ? "Общая сумма":"Jami summa"} groupSeparator={" "}
                                                       value={this.state.calculate.totalDeliveryCost ? this.state.calculate.totalDeliveryCost+this.state.total : 0}
                                                       suffix={lang === "ru" ? " сум" :" so'm"}/>
                                        </Col>
                                    </Row>
                                </Card>

                                <Card
                                    className="point-card"
                                    type="inner"
                                    title={lang === "ru" ? "СПОСОБЫ ОПЛАТЫ":"TO'LOV USULLARI"}
                                    tabList={tabList}
                                    activeTabKey={this.state.key}
                                    onTabChange={key => {
                                        this.onTabChange(key, 'key');
                                    }}
                                >
                                    {contentList[this.state.key]}
                                </Card>
                            </Col>
                            <Col xs={{span: 24}} lg={{span: 7}}>
                                <Card
                                    className="offer-list-card"
                                    type="inner"
                                    title={lang === "ru" ?"Список товаров":"Buyurtma tarkibi"}>
                                    <List
                                        dataSource={this.state.addedProducts}
                                        renderItem={item => (
                                            <List.Item key={item.id}>
                                                <List.Item.Meta
                                                    avatar={
                                                        <Avatar size="large" shape="square" src={item.img}/>
                                                    }
                                                    title={<div>
                                                        <p><span>{item.name}</span></p>

                                                    </div>}
                                                    description={<div>

                                                        <NumberFormat style={{marginLeft: 10}}
                                                                      value={item.price * item.quantity}
                                                                      displayType={'text'}
                                                                      thousandSeparator={' '} suffix={lang === "ru" ? " сум" :" so'm"}/>
                                                    </div>}
                                                />

                                            </List.Item>
                                        )}
                                    >
                                    </List>

                                    <div>

                                    </div>

                                </Card>
                            </Col>
                        </Row>
                    </Spin>
                </Content>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        addedItems: state.addedItems,
        total: state.total
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        clearCart: ()=>{
            dispatch(clearCart())
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CartPage);


class PaymentForm extends Component {

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.handleSubmit(values);
            }
        });
    };

    render() {
        const {loading} = this.props;
        const {getFieldDecorator} = this.props.form;
        return (
            <div className="pay-form">
                <Form onSubmit={this.handleSubmit}>
                    <Form.Item label={lang === "ru" ?"Номер карты":"Karta raqami"}>
                        {getFieldDecorator('cardNumber', {
                            rules: [{required: true, message: lang === "ru" ?"Введите номер карты!":'Karta raqminini kiriting!'}],
                        })(
                            <MaskedInput

                                mask="1111 1111 1111 1111"
                                prefix={<Icon type="credit-card" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            />,
                        )}
                    </Form.Item>
                    <Form.Item label={lang === "ru" ?"Срок действия":"Amal qilish muddati"}>
                        {getFieldDecorator('expDate', {
                            rules: [{required: true, message: lang === "ru" ?'Введите срок действия!':'Amal qilish muddatini kiriting!'}],
                        })(
                            <MaskedInput
                                mask={"11/11"}
                                prefix={<Icon type="calendar" style={{color: 'rgba(0,0,0,.25)'}}/>}

                            />,
                        )}
                    </Form.Item>
                    <Form.Item>

                        <Button loading={loading} type="primary" htmlType="submit" className="login-form-button">
                            {lang === "ru" ? 'Отправить':'Yuborish'}
                        </Button>

                    </Form.Item>


                </Form>
            </div>
        );
    }
}


const PaymentFormWrap = Form.create({name: 'paymnt0-form'})(PaymentForm);


class VerifyCodeForm extends Component {

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.handleSubmit(values);
            }
        });
    };

    render() {
        const {loading} = this.props;
        const {getFieldDecorator} = this.props.form;
        return (
            <div className="pay-form">
                <Form onSubmit={this.handleSubmit}>
                    <Form.Item label={lang === "ru" ?"Введите код подтверждения":"Tasdiqlash kodini kiriting"}>
                        {getFieldDecorator('code', {
                            rules: [{required: true, message: lang === "ru" ?"Введите код подтверждения":"Tasdiqlash kodini kiriting"}],
                        })(
                            <MaskedInput
                                style={{textAlign: "center"}}
                                mask="11111"

                            />,
                        )}
                    </Form.Item>
                    <Form.Item>

                        <Button loading={loading} type="primary" htmlType="submit" className="login-form-button">
                            {lang === "ru" ? 'Отправить':'Yuborish'}
                        </Button>
                        <a href="" onClick={()=>this.props.onCancel()}>{lang === "ru" ?"Я не получил код подтверждения!":"Tasdiqlash kodi kelmadi!"}</a>
                    </Form.Item>


                </Form>
            </div>
        );
    }
}


const VerifyCodeFormWrap = Form.create({name: 'pverify-form'})(VerifyCodeForm);
