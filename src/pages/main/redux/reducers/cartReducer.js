import {
    ADD_QUANTITY,
    ADD_TO_CART,
    REMOVE_ITEM,
    SUB_QUANTITY,
    CHANGE_QUANTITY, INIT_ADDED_ITEMS, CLEAR_CART
} from "../actions/action-types/cart-actions";
import {
    BY_CATEGORY,
    FETCH_CATEGORIES,
    RECENTLY,
    RECOMMENDED,
    START_LOADING,
    SEARCH,
    RELATED, LOADING_MODAL, SHOW_MODAL, SELECTED
} from "../actions/action-types/product-actions"
import {Avatar, notification} from "antd";
import React from "react";

const initState = {
    recentlyProducts: [],
    recommendedProducts: [],
    categoryProducts: [],
    searchProducts: [],
    selectedProduct: [],
    relatedProducts: [],

    categories: [],

    addedItems: [],
    total: 0,

    loading: false,
    isLoadingModal: false,
    showModal: false
};
const cartReducer = (state = initState, action) => {

    if (action.type === CLEAR_CART){
        return {
            ...state,
            addedItems: [],
            total: 0,
        }
    }
    if (action.type === INIT_ADDED_ITEMS){

        let total = 0;

        action.products.map(item=>{
            total += item.price * item.quantity
        });

        return {
            ...state,
            addedItems: action.products,
            total
        }
    }

    if (action.type === ADD_TO_CART) {
        let addedItem = {};
        let addedItemIndex = null;


        if (action.src === 1) {
            addedItem = state.recentlyProducts.find(item => item.id === action.id);
            addedItemIndex = state.recentlyProducts.findIndex(item => item.id === action.id);
            let products = [...state.recentlyProducts];

            //check if the action id exists in the addedItems

            let existed_item = state.addedItems.find(item => action.id === item.id);
            if (existed_item) {
                addedItem.quantity = existed_item.quantity + 1;
                products[addedItemIndex].quantity = existed_item.quantity;
                return {
                    ...state,
                    total: state.total + addedItem.price,
                    recentlyProducts: products
                }
            } else {
                addedItem.quantity = 1;
                products[addedItemIndex].quantity = 1;
                //calculating the total
                let newTotal = state.total + addedItem.price;

                notification.open({
                    message:addedItem.name,
                    icon: <Avatar shape="square" src={addedItem.img} />,
                    duration: 1.5
                });

                return {
                    ...state,
                    addedItems: [...state.addedItems, addedItem],
                    total: newTotal,
                    recentlyProducts: products
                }

            }
        } else if (action.src === 2) {
            addedItem = state.recommendedProducts.find(item => item.id === action.id);
            addedItemIndex = state.recommendedProducts.findIndex(item => item.id === action.id);
            let products = [...state.recommendedProducts];

            //check if the action id exists in the addedItems

            let existed_item = state.addedItems.find(item => action.id === item.id);
            if (existed_item) {
                addedItem.quantity = existed_item.quantity + 1;
                products[addedItemIndex].quantity = existed_item.quantity;
                return {
                    ...state,
                    total: state.total + addedItem.price,
                    recommendedProducts: products
                }
            } else {
                addedItem.quantity = 1;
                products[addedItemIndex].quantity = 1;
                //calculating the total
                let newTotal = state.total + addedItem.price;

                notification.open({
                    message:addedItem.name,
                    icon: <Avatar shape="square" src={addedItem.img} />,
                    duration: 1.5
                });

                return {
                    ...state,
                    addedItems: [...state.addedItems, addedItem],
                    total: newTotal,
                    recommendedProducts: products
                }

            }
        } else if (action.src === 3) {
            addedItem = state.categoryProducts.find(item => item.id === action.id);

            addedItemIndex = state.categoryProducts.findIndex(item => item.id === action.id);
            let products = [...state.categoryProducts];

            //check if the action id exists in the addedItems

            let existed_item = state.addedItems.find(item => action.id === item.id);
            if (existed_item) {
                addedItem.quantity = existed_item.quantity + 1;
                products[addedItemIndex].quantity = existed_item.quantity;
                return {
                    ...state,
                    total: state.total + addedItem.price,
                    categoryProducts: products
                }
            } else {
                addedItem.quantity = 1;
                products[addedItemIndex].quantity = 1;
                //calculating the total
                let newTotal = state.total + addedItem.price;

                notification.open({
                    message:addedItem.name,
                    icon: <Avatar shape="square" src={addedItem.img} />,
                    duration: 1.5
                });

                return {
                    ...state,
                    addedItems: [...state.addedItems, addedItem],
                    total: newTotal,
                    categoryProducts: products
                }

            }
        } else if (action.src === 4) {
            addedItem = state.searchProducts.find(item => item.id === action.id);

            addedItemIndex = state.searchProducts.findIndex(item => item.id === action.id);
            let products = [...state.searchProducts];

            //check if the action id exists in the addedItems

            let existed_item = state.addedItems.find(item => action.id === item.id);
            if (existed_item) {
                addedItem.quantity = existed_item.quantity + 1;
                products[addedItemIndex].quantity = existed_item.quantity;
                return {
                    ...state,
                    total: state.total + addedItem.price,
                    searchProducts: products
                }
            } else {
                addedItem.quantity = 1;
                products[addedItemIndex].quantity = 1;
                //calculating the total
                let newTotal = state.total + addedItem.price;

                notification.open({
                    message:addedItem.name,
                    icon: <Avatar shape="square" src={addedItem.img} />,
                    duration: 1.5
                });

                return {
                    ...state,
                    addedItems: [...state.addedItems, addedItem],
                    total: newTotal,
                    searchProducts: products
                }

            }
        } else if (action.src === 5) {
            addedItem = state.selectedProduct.find(item => item.id === action.id);

            addedItemIndex = state.selectedProduct.findIndex(item => item.id === action.id);
            let products = [...state.selectedProduct];

            //check if the action id exists in the addedItems

            let existed_item = state.addedItems.find(item => action.id === item.id);
            if (existed_item) {
                addedItem.quantity = existed_item.quantity + 1;
                products[addedItemIndex].quantity = existed_item.quantity;
                return {
                    ...state,
                    total: state.total + addedItem.price,
                    selectedProduct: products
                }
            } else {
                addedItem.quantity = 1;
                products[addedItemIndex].quantity = 1;
                //calculating the total
                let newTotal = state.total + addedItem.price;

                notification.open({
                    message:addedItem.name,
                    icon: <Avatar shape="square" src={addedItem.img} />,
                    duration: 1.5
                });

                return {
                    ...state,
                    addedItems: [...state.addedItems, addedItem],
                    total: newTotal,
                    selectedProduct: products
                }

            }
        } else if (action.src === 6) {
            addedItem = state.relatedProducts.find(item => item.id === action.id);

            addedItemIndex = state.relatedProducts.findIndex(item => item.id === action.id);
            let products = [...state.relatedProducts];

            //check if the action id exists in the addedItems

            let existed_item = state.addedItems.find(item => action.id === item.id);
            if (existed_item) {
                addedItem.quantity = existed_item.quantity + 1;
                products[addedItemIndex].quantity = existed_item.quantity;
                return {
                    ...state,
                    total: state.total + addedItem.price,
                    relatedProducts: products
                }
            } else {
                addedItem.quantity = 1;
                products[addedItemIndex].quantity = 1;
                //calculating the total
                let newTotal = state.total + addedItem.price;

                notification.open({
                    message:addedItem.name,
                    icon: <Avatar shape="square" src={addedItem.img} />,
                    duration: 1.5
                });

                return {
                    ...state,
                    addedItems: [...state.addedItems, addedItem],
                    total: newTotal,
                    relatedProducts: products
                }

            }
        }


    }


    if (action.type === REMOVE_ITEM) {
        let itemToRemove = state.addedItems.find(item => action.id === item.id);
        let new_items = state.addedItems.filter(item => action.id !== item.id);

        //calculating the total
        let newTotal = state.total - (itemToRemove.price * itemToRemove.quantity);


        let recentlyProducts = [...state.recentlyProducts];
        let recentlyIndex = recentlyProducts.findIndex(item => action.id === item.id);

        let recommendedProducts = [...state.recommendedProducts];
        let recommendedIndex = recommendedProducts.findIndex(item => action.id === item.id);

        let categoryProducts = [...state.categoryProducts];
        let categoryIndex = categoryProducts.findIndex(item => action.id === item.id);

        let searchProducts = [...state.searchProducts];
        let searchIndex = searchProducts.findIndex(item => action.id === item.id);

        let selectedProduct = [...state.selectedProduct];
        let selectedIndex = selectedProduct.findIndex(item => action.id === item.id);

        let relatedProducts = [...state.relatedProducts];
        let relatedIndex = relatedProducts.findIndex(item => action.id === item.id);

        if (recentlyProducts.some(item => action.id === item.id)) {
            console.log("recently!!")
            recentlyProducts[recentlyIndex].quantity = null
        }

        if (recommendedProducts.some(item => action.id === item.id)) {
            recommendedProducts[recommendedIndex].quantity = null
        }

        if (categoryProducts.some(item => action.id === item.id)) {
            categoryProducts[categoryIndex].quantity = null
        }

        if (searchProducts.some(item => action.id === item.id)) {
            searchProducts[searchIndex].quantity = null
        }

        if (selectedProduct.some(item => action.id === item.id)) {
            selectedProduct[selectedIndex].quantity = null
        }

        if (relatedProducts.some(item => action.id === item.id)) {
            relatedProducts[relatedIndex].quantity = null
        }


        return {
            ...state,
            addedItems: new_items,
            total: newTotal,
            recentlyProducts: recentlyProducts,
            recommendedProducts: recommendedProducts,
            categoryProducts: categoryProducts,
            searchProducts: searchProducts,
            selectedProduct: selectedProduct,
            relatedProducts: relatedProducts,
        }
    }


    //INSIDE CART COMPONENT
    if (action.type === ADD_QUANTITY) {
        let addedItem = state.addedItems.find(item => item.id === action.id);
        addedItem.quantity += 1;


        let recentlyProducts = [...state.recentlyProducts];
        let recentlyIndex = recentlyProducts.findIndex(item => action.id === item.id);

        let recommendedProducts = [...state.recommendedProducts];
        let recommendedIndex = recommendedProducts.findIndex(item => action.id === item.id);

        let categoryProducts = [...state.categoryProducts];
        let categoryIndex = categoryProducts.findIndex(item => action.id === item.id);

        let searchProducts = [...state.searchProducts];
        let searchIndex = searchProducts.findIndex(item => action.id === item.id);

        let selectedProduct = [...state.selectedProduct];
        let selectedIndex = selectedProduct.findIndex(item => action.id === item.id);

        let relatedProducts = [...state.relatedProducts];
        let relatedIndex = relatedProducts.findIndex(item => action.id === item.id);

        if (recentlyProducts.some(item => action.id === item.id)) {
            recentlyProducts[recentlyIndex].quantity = addedItem.quantity
        }

        if (recommendedProducts.some(item => action.id === item.id)) {
            recommendedProducts[recommendedIndex].quantity = addedItem.quantity
        }

        if (categoryProducts.some(item => action.id === item.id)) {
            categoryProducts[categoryIndex].quantity = addedItem.quantity
        }

        if (searchProducts.some(item => action.id === item.id)) {
            searchProducts[searchIndex].quantity = addedItem.quantity
        }

        if (selectedProduct.some(item => action.id === item.id)) {
            selectedProduct[selectedIndex].quantity = addedItem.quantity
        }

        if (relatedProducts.some(item => action.id === item.id)) {
            relatedProducts[relatedIndex].quantity = addedItem.quantity
        }


        let newTotal = state.total + addedItem.price;
        return {
            ...state,
            total: newTotal,
            recentlyProducts: recentlyProducts,
            recommendedProducts: recommendedProducts,
            categoryProducts: categoryProducts,
            searchProducts: searchProducts,
            selectedProduct: selectedProduct,
            relatedProducts: relatedProducts,
        }
    }


    if (action.type === SUB_QUANTITY) {
        let addedItem = state.addedItems.find(item => item.id === action.id);
        //if the qt == 0 then it should be removed
        if (addedItem.quantity === 1) {
            let new_items = state.addedItems.filter(item => item.id !== action.id);
            let newTotal = state.total - addedItem.price;
            return {
                ...state,
                addedItems: new_items,
                total: newTotal
            }
        } else {
            addedItem.quantity -= 1;


            let recentlyProducts = [...state.recentlyProducts];
            let recentlyIndex = recentlyProducts.findIndex(item => action.id === item.id);

            let recommendedProducts = [...state.recommendedProducts];
            let recommendedIndex = recommendedProducts.findIndex(item => action.id === item.id);

            let categoryProducts = [...state.categoryProducts];
            let categoryIndex = categoryProducts.findIndex(item => action.id === item.id);

            let searchProducts = [...state.searchProducts];
            let searchIndex = searchProducts.findIndex(item => action.id === item.id);

            let selectedProduct = [...state.selectedProduct];
            let selectedIndex = selectedProduct.findIndex(item => action.id === item.id);

            let relatedProducts = [...state.relatedProducts];
            let relatedIndex = relatedProducts.findIndex(item => action.id === item.id);

            if (recentlyProducts.some(item => action.id === item.id)) {
                recentlyProducts[recentlyIndex].quantity = addedItem.quantity
            }

            if (recommendedProducts.some(item => action.id === item.id)) {
                recommendedProducts[recommendedIndex].quantity = addedItem.quantity
            }

            if (categoryProducts.some(item => action.id === item.id)) {
                categoryProducts[categoryIndex].quantity = addedItem.quantity
            }

            if (searchProducts.some(item => action.id === item.id)) {
                searchProducts[searchIndex].quantity = addedItem.quantity
            }

            if (selectedProduct.some(item => action.id === item.id)) {
                selectedProduct[selectedIndex].quantity = addedItem.quantity
            }

            if (relatedProducts.some(item => action.id === item.id)) {
                relatedProducts[relatedIndex].quantity = addedItem.quantity
            }


            let newTotal = state.total - addedItem.price;
            return {
                ...state,
                total: newTotal,
                recentlyProducts: recentlyProducts,
                recommendedProducts: recommendedProducts,
                categoryProducts: categoryProducts,
                searchProducts: searchProducts,
                selectedProduct: selectedProduct,
                relatedProducts: relatedProducts,
            }
        }

    }

    if (action.type === CHANGE_QUANTITY) {
        let addedItem = state.addedItems.find(item => item.id === action.id);

        let oldTotal = state.total - addedItem.quantity * addedItem.price;

        let recentlyProducts = [...state.recentlyProducts];
        let recentlyIndex = recentlyProducts.findIndex(item => action.id === item.id);

        let recommendedProducts = [...state.recommendedProducts];
        let recommendedIndex = recommendedProducts.findIndex(item => action.id === item.id);

        let categoryProducts = [...state.categoryProducts];
        let categoryIndex = categoryProducts.findIndex(item => action.id === item.id);

        let searchProducts = [...state.searchProducts];
        let searchIndex = searchProducts.findIndex(item => action.id === item.id);

        let selectedProduct = [...state.selectedProduct];
        let selectedIndex = selectedProduct.findIndex(item => action.id === item.id);

        let relatedProducts = [...state.relatedProducts];
        let relatedIndex = relatedProducts.findIndex(item => action.id === item.id);

        if (action.number > 0) {
            addedItem.quantity = action.number;

            if (recentlyProducts.some(item => action.id === item.id)) {
                recentlyProducts[recentlyIndex].quantity = addedItem.quantity
            }

            if (recommendedProducts.some(item => action.id === item.id)) {
                recommendedProducts[recommendedIndex].quantity = addedItem.quantity
            }

            if (categoryProducts.some(item => action.id === item.id)) {
                categoryProducts[categoryIndex].quantity = addedItem.quantity
            }

            if (searchProducts.some(item => action.id === item.id)) {
                searchProducts[searchIndex].quantity = addedItem.quantity
            }

            if (selectedProduct.some(item => action.id === item.id)) {
                selectedProduct[selectedIndex].quantity = addedItem.quantity
            }

            if (relatedProducts.some(item => action.id === item.id)) {
                relatedProducts[relatedIndex].quantity = addedItem.quantity
            }

            let newTotal = oldTotal + addedItem.price * action.number;

            return {
                ...state,
                total: newTotal,
                recentlyProducts: recentlyProducts,
                recommendedProducts: recommendedProducts,
                categoryProducts: categoryProducts,
                searchProducts: searchProducts,
                selectedProduct: selectedProduct,
                relatedProducts: relatedProducts
            }

        } else {
            addedItem.quantity = 1;

            if (recentlyProducts.some(item => action.id === item.id)) {
                recentlyProducts[recentlyIndex].quantity = addedItem.quantity
            }

            if (recommendedProducts.some(item => action.id === item.id)) {
                recommendedProducts[recommendedIndex].quantity = addedItem.quantity
            }

            if (categoryProducts.some(item => action.id === item.id)) {
                categoryProducts[categoryIndex].quantity = addedItem.quantity
            }

            if (searchProducts.some(item => action.id === item.id)) {
                searchProducts[searchIndex].quantity = addedItem.quantity
            }

            if (selectedProduct.some(item => action.id === item.id)) {
                selectedProduct[selectedIndex].quantity = addedItem.quantity
            }

            if (relatedProducts.some(item => action.id === item.id)) {
                relatedProducts[relatedIndex].quantity = addedItem.quantity
            }

            let newTotal = oldTotal + addedItem.price;

            return {
                ...state,
                total: newTotal,
                recentlyProducts: recentlyProducts,
                recommendedProducts: recommendedProducts,
                categoryProducts: categoryProducts,
                searchProducts: searchProducts,
                selectedProduct: selectedProduct,
                relatedProducts: relatedProducts
            }
        }

    }

    if (action.type === RECENTLY) {
        return {
            ...state,
            recentlyProducts: action.products
        }
    }

    if (action.type === RECOMMENDED) {
        return {
            ...state,
            recommendedProducts: action.products
        }
    }
    if (action.type === BY_CATEGORY) {

        return {
            ...state,
            categoryProducts: action.products
        }
    }
    if (action.type === SEARCH) {

        return {
            ...state,
            searchProducts: action.products
        }
    }
    if (action.type === RELATED) {
        let new_items = action.products.filter(item => state.selectedProduct[0].id !== item.id);
        return {
            ...state,
            relatedProducts: new_items
        }
    }

    if (action.type === SELECTED) {
        let products = [];
        products.push(action.product);
        return {
            ...state,
            selectedProduct: products
        }
    }

    if (action.type === FETCH_CATEGORIES) {

        return {
            ...state,
            categories: action.categories
        }
    }
    if (action.type === START_LOADING) {

        return {
            ...state,
            loading: action.loading
        }
    }
    if (action.type === LOADING_MODAL) {
        return {
            ...state,
            isLoadingModal: action.loading
        }
    }
    if (action.type === SHOW_MODAL) {
        return {
            ...state,
            showModal: action.showModal
        }
    } else {
        return state
    }

};


export default cartReducer;