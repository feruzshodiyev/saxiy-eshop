import {
    ADD_TO_CART,
    REMOVE_ITEM,
    SUB_QUANTITY,
    ADD_QUANTITY,
    CHANGE_QUANTITY,
    INIT_ADDED_ITEMS, CLEAR_CART
} from "./action-types/cart-actions";

export const initAddedItems= (products)=>{
    return{
        type: INIT_ADDED_ITEMS,
        products
    }
};


export const addToCart= (id, src)=>{
    return{
        type: ADD_TO_CART,
        id,
        src
    }
};

export const removeItem= (id)=>{
    return{
        type: REMOVE_ITEM,
        id }
};

export const subtractQuantity= (id)=>{
    return{
        type: SUB_QUANTITY,
        id }
};

export const addQuantity= (id)=>{
    return{
        type: ADD_QUANTITY,
        id }
};

export const changeQuantity =(id, number)=>{
    return{
        type: CHANGE_QUANTITY,
        number,
        id
    }
};

export const clearCart =()=>{
    return{
        type: CLEAR_CART
    }
};