 export const RECOMMENDED = "RECOMMENDED";
 export const RECENTLY = "RECENTLY";
 export const BY_CATEGORY = "CATEGORY";
 export const SEARCH = "SEARCH";
 export const RELATED = "RELATED";
 export const SELECTED = "SELECTED";

 export const LOADING_MODAL = "LOADING_MODAL";
 export const SHOW_MODAL = "SHOW_MODAL";

 export const FETCH_CATEGORIES = "FETCH_CATEGORIES";

 export const START_LOADING = "START_LOADING";
