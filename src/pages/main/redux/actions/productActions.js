import {
    BY_CATEGORY,
    RECENTLY,
    RECOMMENDED,
    FETCH_CATEGORIES,
    START_LOADING,
    SEARCH,
    RELATED, SELECTED, LOADING_MODAL, SHOW_MODAL
} from "./action-types/product-actions"



export const addByCategory=(products)=>{
    return{
        type: BY_CATEGORY,
        products

    }
};

export const addRecently=(products)=>{
    return{
        type: RECENTLY,
        products
    }
};

export const addRecommended=(products)=>{
    return{
        type: RECOMMENDED,
        products
    }
};

export const addSearch=(products)=>{
    return{
        type: SEARCH,
        products
    }
};


export const addRelated=(products)=>{
    return{
        type: RELATED,
        products
    }
};

export const addSelected=(product)=>{
    return{
        type: SELECTED,
        product
    }
};



export const addCategories=(categories)=>{
    return{
        type: FETCH_CATEGORIES,
        categories
    }
};

export const startLoading=(loading)=>{
    return{
        type: START_LOADING,
        loading
    }
};
export const loadingModal=(loading)=>{
    return{
        type: LOADING_MODAL,
        loading
    }
};
export const showModalProduct=(showModal)=>{
    return{
        type: SHOW_MODAL,
        showModal
    }
};





