import React, {Component} from 'react';

import "./ForgotPassword.scss"
import {ACCESS_TOKEN_USER, API_BASE_URL, LANGUAGE} from "../../constants";
import {Button, Form, Icon, Input, notification, Result, Spin} from "antd";
import MaskedInput from "antd-mask-input";
import {Link} from "react-router-dom";

import axios from 'axios';


const lang = localStorage.getItem(LANGUAGE);

class ForgotPassword extends Component {
    state = {
        showPhoneForm: true,
        showPasswordForm: false,
        showSuccess: false,
        loading: false,
        phoneNumber: 0,
        secretKey: null
    };

    handleSubmitPhoneNum=(values)=>{
        this.setState({
            loading: true,
            phoneNumber: values.phone
        });

        axios.post(API_BASE_URL+"user/forget-password",{}, {
            params:{
                "phone":values.phone.replace("(", "").replace(")", "").replace(/\s/g, "")
            }
        }).then(res=>{
            console.log(res);
            this.setState({
                loading: false,
                secretKey: res.data,
                showPhoneForm: false,
                showPasswordForm: true,

            })
        }).catch(err=>{
            this.setState({
                loading: false,
            });

            notification.error({
                message: lang === "ru" ? "Ошибка!":"Xatolik!"
            })
        })

    };

    handleNoCodeAction=()=>{
      this.setState({
          showPhoneForm: true,
          showPasswordForm: false,
      })
    };

    handleSubmitResetPassword=(values)=>{
        this.setState({
            loading: true
        });

        axios.put(API_BASE_URL+"user/forget-password-confirm",{}, {
            params:{
                "code": values.code,
                "newPassword": values.password,
                "secretKey": this.state.secretKey
            }
        }).then(res=>{
            console.log(res);
            this.setState({
                loading: false,
                showPhoneForm: false,
                showPasswordForm: false,
                showSuccess:true

            })
        }).catch(err=>{
            this.setState({
                loading: false,
            });

            notification.error({
                message: lang === "ru" ? "Ошибка!":"Xatolik!"
            })
        })
    };


    render() {
        return (
            <Spin spinning={this.state.loading}>
            <div className={"forgot-wrap"}>
                <h2>{lang === "ru" ? "Сброс пароля":"Parolni tiklash"}</h2>
                {this.state.showPhoneForm ? <WrappedForgotFormPhone
                    onSubmitPhoneNum={this.handleSubmitPhoneNum}
                />:""}
                {this.state.showPasswordForm ? <WrappedResetForm
                    noCode={this.handleNoCodeAction}
                    onSubmitResetPassword={this.handleSubmitResetPassword}
                    phoneNumber={this.state.phoneNumber}
                />:""}
                {this.state.showSuccess ? <Success/>:""}
            </div>
            </Spin>
        );
    }
}

export default ForgotPassword;


class ForgotFormPhone extends React.Component {


    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log(values);
            if (!err) {
                this.props.onSubmitPhoneNum(values)
            }
        });
    };

    render() {

        const {getFieldDecorator} = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit} className="forgot-password-form">
<p>{lang === "ru" ? "Пожалуйста, введите ваш номер телефона. Мы вышлем вам код в виде смс для создания нового пароля." : "Iltimos, telefon raqamingnizni kiring. Sizga yangi parol kiritishingiz uchun kodni sms tarzida yuboramiz."}</p>
                <Form.Item label={lang === "ru" ? "Номер телефона" : "Telefon raqami"}>
                    {getFieldDecorator('phone', {
                        rules: [{
                            required: true,
                            message: lang === "ru" ? "Введите номер телефона!" : "Telefon raqamini kiriting!"
                        }]
                    })(<MaskedInput
                        prefix={<Icon type="mobile" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        mask="(11) 111 11 11"
                    />)}
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit"
                            className="login-form-button">
                        {lang === "ru" ? "Отправить" : "Yuborish"}
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

const WrappedForgotFormPhone = Form.create({name: 'normal_form'})(ForgotFormPhone);

class ResetForm extends Component {


    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log(values);
            if (!err) {
                this.props.onSubmitResetPassword(values)
            }
        });
    };

    render() {
        const lang = localStorage.getItem(LANGUAGE);

        const {getFieldDecorator} = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit} className="sign-up-form">
                <h3>{lang==="ru"? "Код был отправлен на номер ":""}<span style={{color: "#3352ff"}}>{this.props.phoneNumber}</span>{lang==="ru"? "":" Raqamiga tasdiqlash kodi yuborildi"}</h3>
                <Form.Item label={lang==="ru"? "Введите код подтверждения":'Tasdiqlash kodini kiriting'}>
                    {getFieldDecorator('code', {
                        rules: [{required: true, message: lang==="ru"? "Введите код!":'Tasdiqlash kodini kiriting!'}]
                    })(<Input/>)}
                </Form.Item>
                <Form.Item label={lang==="ru"? "Введите новый пароль":'Yangi parol kiriting'}>
                    {getFieldDecorator('password', {
                        rules: [
                            {
                                required: true,
                                message: lang==="ru"? "Пожалуйста, введите пароль!":'Iltimos, parol kiriting!',
                            }
                        ],
                    })(<Input.Password placeholder={lang==="ru"? "Пароль":"Parol"}
                                       prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}/>)}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit"
                            className="login-form-button">
                        {lang==="ru"? "Отправить":"Yuborish"}
                    </Button>

                    <Button onClick={()=>this.props.noCode()} type={"link"}>{lang==="ru"? "Код не пришел!":"Kod kelmadi!"}</Button>
                </Form.Item>
            </Form>
        );
    }
}

const WrappedResetForm = Form.create({name: 'ResetForm'})(ResetForm);


class Success extends Component {
    render() {
        const lang = localStorage.getItem(LANGUAGE);

        return (
            <Result
                icon={<Icon type="smile" theme="twoTone" />}
                title={lang==="ru"? "Восстановление пароля завершено! ":"Parolni tiklash amalga oshirildi!"}
                extra={<Button type="primary"><Link to={"/sign-in"}>{lang==="ru"? "Продолжить":"Davom etish"}</Link></Button>}
            />
        );
    }
}