import React, {Component} from 'react';
import axios from "axios";
import {ACCESS_TOKEN, API_BASE_URL} from "../../constants";
import {Button, Col, Form, Icon, Input, InputNumber, notification, Popconfirm, Row, Select, Table} from "antd";
import DistrictModalForm from "../../components/DistrictModalForm";

const ButtonGroup = Button.Group;
const {Option} = Select;

const EditableContext = React.createContext();

class EditableCell extends React.Component {
    getInput = () => {
        return <Input/>;
    };

    renderCell = ({getFieldDecorator}) => {
        const {
            editing,
            dataIndex,
            title,
            inputType,
            record,
            index,
            children,
            ...restProps
        } = this.props;
        return (
            <td {...restProps}>
                {editing ? (
                    <Form.Item style={{margin: 0}}>
                        {getFieldDecorator(dataIndex, {
                            rules: [
                                {
                                    required: true,
                                    message: `${title} киритикнг!`,
                                },
                            ],
                            initialValue: record[dataIndex],
                        })(<Input/>)}
                    </Form.Item>
                ) : (
                    children
                )}
            </td>
        );
    };

    render() {
        return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>;
    }
}

class Districts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            data: [],
            editingKey: '',
            loading: false,
            regions: [],
            defaultRegion: {},
            selectedRegionId: 0
        };

        this.columns = [
            {
                title: 'Номи (русча)',
                dataIndex: 'nameRu',
                width: '25%',
                editable: true,
            },
            {
                title: 'Номи (ўзбекча)',
                dataIndex: 'nameUz',
                width: '40%',
                editable: true,
            },
            {
                title: 'Операция',
                dataIndex: 'id',
                render: (id, record) => {
                    const {editingKey} = this.state;
                    const editable = this.isEditing(record);
                    return editable ? (
                        <span>
              <EditableContext.Consumer>
                {form => (
                    <a
                        onClick={() => this.save(form, record.id)}
                        style={{marginRight: 8}}
                    >
                        Сақлаш
                    </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm
                  okText="Ҳа"
                  cancelText="Йоқ"
                  title="Таҳрирлашни бекор қиласизми?" onConfirm={() => this.cancel(record.id)}>
                <a>Бекор қилиш</a>
              </Popconfirm>
            </span>
                    ) : (
                        <div>
                            <ButtonGroup>
                                <Button
                                    type="primary"
                                    icon="edit"
                                    disabled={editingKey !== ''}
                                    onClick={() => this.edit(record.id)}/>
                                <Popconfirm placement="top"
                                            title="Вилоятни ўчирасизми?"
                                            onConfirm={() => this.deleteOneById(id)} okText="Ҳа" cancelText="Йўқ">
                                    <Button type="danger" icon="delete"/>
                                </Popconfirm>
                            </ButtonGroup>

                        </div>

                    );
                },
            },
        ];
    }


    componentDidMount() {
        this.fetchRegions()
    }


    fetchRegions = () => {

        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "region", {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {

            const firstReg = res.data[0];
            this.setState({
                regions: res.data,
                defaultRegion: firstReg,
                selectedRegionId: firstReg.id,
                loading: false
            }, () => this.fetchDistricts());

            console.log(res)
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };


    fetchDistricts = () => {
        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "district/ap/dist-id/" + this.state.selectedRegionId, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            this.setState({
                data: res.data,
                loading: false
            });
            console.log(res)
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };

    showModal = () => {
        this.setState({visible: true});
    };

    handleCancel = () => {
        this.setState({visible: false});
    };


    deleteOneById = (id) => {
        console.log("id", id);
        axios.delete(API_BASE_URL + "district/" + id, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            notification.success({message: "Туман ўчирилди!"});
            this.fetchDistricts();
        }).catch(err => {
            notification.error({message: "{Хатолик юз берди!"})
        })
    };


    handleCreate = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }


            axios.post(API_BASE_URL + "district", {
                "nameRu": values.nameRu,
                "nameUz": values.nameUz,
                "regionId": this.state.selectedRegionId
            }, {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
                }
            }).then(res => {
                notification.success({
                    message: "Янги туман қўшилди!",
                    placement: 'topLeft'
                });
                this.fetchDistricts();
            }).catch(err => {
                notification.error({
                    message: "Туман қўшишда хатолик юз берди!",
                    placement: 'topLeft'
                });
            });

            form.resetFields();
            this.setState({visible: false});
        });
    };

    saveFormRef = formRef => {
        this.formRef = formRef;
    };


    isEditing = record => {
        console.log(record);
        return record.id === this.state.editingKey;
    };

    cancel = () => {
        this.setState({editingKey: ''});
    };

    save(form, id) {
        form.validateFields((error, row) => {
            if (error) {
                return;
            }
            const newData = [...this.state.data];
            const index = newData.findIndex(item => id === item.id);
            if (index > -1) {
                const item = newData[index];
                console.log("item", index);
                newData.splice(index, 1, {
                    ...item,
                    ...row,
                });

                console.log("row", row);
                axios.put(API_BASE_URL + "district/" + item.id, {
                        "nameRu": row.nameRu,
                        "nameUz": row.nameUz,
                        "regionId": item.regionId
                    }, {
                        headers: {
                            'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
                        }
                    }
                ).then(res => {
                    notification.success({
                        message: "Туман таҳрирланди!"
                    })
                }).catch(err => {
                    notification.error({
                        message: "Таҳрирлашда хатолик юз берди!"
                    })
                });

                this.setState({data: newData, editingKey: ''});
            } else {
                newData.push(row);
                this.setState({data: newData, editingKey: ''});
            }


        });
    }

    edit(id) {
        this.setState({editingKey: id});
    }

    handleChangeRegion = (value) => {
        this.setState({
            selectedRegionId: value
        }, () => this.fetchDistricts());

    };


    render() {

        const components = {
            body: {
                cell: EditableCell,
            },
        };

        const columns = this.columns.map(col => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    inputType: 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });


        return (
            <div>
                <Row>
                    <Col xs={{span: 5}} lg={{span: 6}}>
                        <Button onClick={this.showModal} type="primary" style={{marginBottom: 16}}>
                            <Icon type="plus-circle"/>
                            Қўшиш
                        </Button>
                    </Col>
                    <Col xs={{span: 11, offset: 1}} lg={{span: 6, offset: 2}}>

                        <Select
                            // labelInValue
                            onChange={this.handleChangeRegion}
                            defaultValue={"def"}
                            style={{width: "100%"}}
                            // onChange={handleChange}
                        >
                            <Option style={{display: this.state.regions.length > 0 ? "none" : "contents"}}
                                    key={"def"}>{this.state.regions.length > 0 ? this.state.defaultRegion.name : "Вилоят топилмади"}</Option>
                            {this.state.regions.map(item => (
                                <Option key={item.id}>{item.name}</Option>
                            ))}
                        </Select>


                    </Col>
                    <Col xs={{span: 5, offset: 1}} lg={{span: 6, offset: 2}}>
                    </Col>
                </Row>

                <EditableContext.Provider value={this.props.form}>
                    <Table
                        loading={this.state.loading}
                        rowKey="id"
                        scroll={{y: 400}}
                        components={components}
                        bordered
                        dataSource={this.state.data}
                        columns={columns}
                        rowClassName="editable-row"
                        pagination={false}
                    />
                </EditableContext.Provider>

                <DistrictModalForm
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                />

            </div>
        );
    }
}

const DistrictsFormTable = Form.create()(Districts);

export default DistrictsFormTable;