import React, {Component} from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';

import axios from 'axios';

import {notification} from 'antd'

import AntLoginForm from "./AuthAdmin";

import AdminPanel from "./AdminPanel";
import {ACCESS_TOKEN, API_BASE_URL} from "../../constants";
import {Sugar} from "react-preloaders";


class WrapAdmin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthenticated: false,
            isAdmin: false,
            token: '',
            loading: true
        }
    }

    componentDidMount() {

        this.checkAuth();
    }

    checkAuth = () => {
        const token = localStorage.getItem(ACCESS_TOKEN)

        if (token) {

            axios.get(API_BASE_URL + "user/details", {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
                }
            }).then(res => {
                console.log(res);
                if (res.data.role ==='ADMINISTRATOR'){
                    this.setState({
                        token: token,
                        isAuthenticated: true,
                        isAdmin: true,
                        loading: false
                    })
                } else {
                    notification.error({message: "Фойдаланувчи админ емас!"})
                    this.setState({
                        loading: false
                    })
                }

            }).catch(err => {
                console.log("error : !!!", err.response.status)
                if (err.response &&err.response.status === 401) {
                    localStorage.removeItem(ACCESS_TOKEN)
                }
                this.setState({
                    loading: false
                })
            });


        }else {
            this.setState({
                loading:false
            });
        }
    };


    handleLogin = (values) => {

        this.setState({
            loading: true
        });
        axios.post("http://saxiysavdo.uz:6526/oauth/token", {}, {
            headers: {
                'Authorization': "Basic d2ViOll5NWpydUJtbVdUakREajY=",
            },
            params: {
                "username": values.username,
                "password": values.password,
                "grant_type": values.grant_type
            }
        }).then(res => {
            localStorage.setItem(ACCESS_TOKEN, res.data.access_token);


            this.checkAuth()
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log("err", err);
            notification.error({
                message: "Хатолик юз берди!",
                description: "Илтимос қайта уриниб кўринг."
            })
        })

    };


    render() {
        return (
            <div>
                <Sugar customLoading={this.state.loading} color={'#4000ff'}/>
                <Switch>

                    <Route path="/admin/auth"
                           render={(props) => !this.state.isAuthenticated || !this.state.isAdmin ? <AntLoginForm
                                   loading={this.state.loading}
                                   handleLogin={(values) => this.handleLogin(values)}
                                   {...props}
                               /> :
                               <Redirect to="/admin"/>}/>


                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}


                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/regions" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}

                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/districts" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}

                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/points" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}

                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/carriers" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}

                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/categories" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}

                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/profile" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}

                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/goods" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}

                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/slider" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}

                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/sider" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}

                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/logos" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}

                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/recommended" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}

                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/recently" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}
                    {this.state.isAuthenticated && this.state.isAdmin ?
                        <Route exact path="/admin/orders" render={(props) => <AdminPanel
                            token={this.state.token}
                            {...props}
                        />}/> :
                        <Redirect to="/admin/auth"/>}


                </Switch>
            </div>
        );
    }
}

export default WrapAdmin;