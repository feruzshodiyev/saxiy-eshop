import React, {Component} from 'react';
import axios from "axios";
import {ACCESS_TOKEN, API_BASE_URL} from "../../constants";
import {Avatar, Button, Col, Icon, notification, Popconfirm, Row, Select, Table, Tag} from "antd";
import GoodsModalForm from "../../components/GoodsModalForm";

const {Option} = Select;
const ButtonGroup = Button.Group;

class Recently extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            parentCategories: [],
            defaultParent: {},
            selectedParentId: null,
            subCategories1: [],
            defaultSub1: {},
            selectedSub1Id: null,
            subCategories2: [],
            defaultSub2: {},
            currentName: '',
            currentId: null,
            currentIcon: '',

            products: [],

            editRow: {},
            isEditing: false,
            visible: false,
            confirmLoading: false
        }
    }

    componentDidMount() {
        this.fetchProducts();

    }


    fetchProducts = () => {
        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "product/recently", {
            // headers: {
            //     'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            // }
        }).then(res => {
            this.setState({
                products: res.data,
                loading: false
            });
            console.log('products', res)
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };





    handleCancel = () => {
        this.setState({
            editRow: {},
            isEditing: false,
            visible: false
        });
        const {form} = this.formRef.props;
        form.resetFields();
    };



    showModalForEdit = (row) => {
        this.setState({
            visible: true,
            editRow: row,
            isEditing: true
        });
        const {form} = this.formRef.props;
        form.resetFields();
    };


    onEditModal = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            this.setState({
                confirmLoading: true
            });

            if (err) {
                this.setState({
                    confirmLoading: false
                });
                return;
            }

            this.handleEdit(values);

            form.resetFields();
        });


    };

    handleEdit = (values) => {

        console.log('values', values);
        let formData = new FormData();

        formData.append("id", values.id);
        formData.append("active", values.active);
        formData.append("nameUz", values.nameUz);
        formData.append("nameRu", values.nameRu);
        formData.append("infoUz", values.infoUz);
        formData.append("infoRu", values.infoRu);
        formData.append("amount", values.amount);
        formData.append("height", values.height);
        if (values.photo){
            formData.append("photo", values.photo[values.photo.length-1].originFileObj);
        }
        formData.append("length", values.length);
        formData.append("price", values.price);
        formData.append("weight", values.weight);
        formData.append("width", values.width);
        formData.append("discount", values.discount);
        formData.append("discountPrice", values.discountPrice);
        formData.append("category", this.state.currentId);
        formData.append("productKey", values.productKey);
        formData.append("deliveryPrice", values.deliveryPrice);
        formData.append("deliveryDay", values.deliveryDay)

        if (values.home === 1){
            formData.append("recentlyAdded", "true");
            formData.append("recommended", "false");
        }else
        if (values.home === 2){
            formData.append("recentlyAdded", "false");
            formData.append("recommended", "true");
        } else
        if (values.home === 0){
            formData.append("recentlyAdded", "false");
            formData.append("recommended", "false");
        }



        axios.put(API_BASE_URL + "product/" + values.id, formData, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN),
                'Content-Type':'multipart/form-data'
            }
        }).then(res => {

            this.setState({
                confirmLoading: false,
                visible: false
            });
            console.log("res: ", res);

            notification.success({
                message: "Товарни таҳрирлаш амалга оширилди!"
            });

            this.fetchProducts(this.state.currentId)

        }).catch(err => {
            this.setState({
                confirmLoading: false
            });
            notification.error({
                message: "Товарни таҳрирлашда хатолик юз берди!"
            });
            console.log("err: ", err)
        });

    };


    saveFormRef = formRef => {
        this.formRef = formRef;
    };


    deleteOneById = (id) => {
        axios.delete(API_BASE_URL + "product/" + id, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            this.fetchProducts(this.state.currentId);
            notification.success({
                message: "Товарни ўчириш амалга оширилди!"
            });
        }).catch(err => {
            notification.error({
                message: "Товарни ўчиришда хатолик юз берди!"
            });
        })
    };


    render() {

        const columns = [
            {
                title: 'Товар',
                dataIndex: 'img',
                key: 'productImage',
                render:(id, record) => (
                    <div>
                        <Avatar src={record.img} shape={"square"}/>
                    </div>
                )
            },
            {
                title: 'Номи (русча)',
                dataIndex: 'nameRu',
                key: 'nameRu',
                // width:200,
                ellipsis: true
            },
            {
                title: 'Номи (ўзбекча)',
                dataIndex: 'name',
                key: 'name',
                // width:200,
                ellipsis: true
            },
            {
                title: 'Миқдори (дона)',
                dataIndex: 'amount',
                key: 'amount',
            },
            {
                title: 'Нархи',
                dataIndex: 'price',
                key: 'price',
            },
            {
                title: 'Скидка',
                dataIndex: 'discountPrice',
                key: 'discountPrice',
            },
            {
                title: 'Узунлиги',
                dataIndex: 'length',
                key: 'length',
            },
            {
                title: 'Баландлиги',
                dataIndex: 'height',
                key: 'height',
            },
            {
                title: 'Эни',
                dataIndex: 'width',
                key: 'width',
            },
            {
                title: 'Оғирлиги',
                dataIndex: 'weight',
                key: 'weight',
            },
            {
                title: 'Махсус ID',
                dataIndex: 'productKey',
                key: 'productKey',
            },
            {
                title: 'Янги қўшилган',
                dataIndex: 'recentlyAdded',
                key: 'recentlyAdded',
                render: (recentlyAdded, record) => (
                    <Tag color={recentlyAdded ? "green" : "red"}>
                        {recentlyAdded ? "Актив" : "Ўчирилган"}
                    </Tag>
                )
            },
            {
                title: 'Тавсия этилган',
                dataIndex: 'recommended',
                key: 'recommended',
                render: (recommended, record) => (
                    <Tag color={recommended ? "green" : "red"}>
                        {recommended ? "Актив" : "Ўчирилган"}
                    </Tag>

                )
            },
            {
                title: 'Статус',
                dataIndex: 'active',
                key: 'active',
                render: (active, record) => (
                    <span>
                {/*<Popconfirm placement="top"*/}
                        {/*            title={active ? "\"Ўчирилган\" ҳолатга ўтказасизми?" : "\"Актив\" ҳолатга ўтказасизми?"}*/}
                        {/*            onConfirm={() => this.changeStatus(record)} okText="Ҳа" cancelText="Йўқ">*/}
                        <Tag color={active ? "green" : "red"}>
                        {active ? "Актив" : "Ўчирилган"}
                    </Tag>
                        {/*</Popconfirm>*/}
      </span>
                ),
            },
            {
                title: 'Операция',
                key: 'id',
                dataIndex: 'id',
                fixed: 'right',
                render: (id, record) => (
                    <div>
                        <ButtonGroup>
                            <Button onClick={() => this.showModalForEdit(record)} type="primary" icon="edit"/>
                            <Popconfirm placement="top"
                                        title="Товарни ўчирасизми?"
                                        onConfirm={() => this.deleteOneById(id)} okText="Ҳа" cancelText="Йўқ">
                                <Button
                                    type="danger" icon="delete"/>
                            </Popconfirm>
                        </ButtonGroup>
                    </div>

                ),
            }
        ];
        return (
            <div>
                <Row gutter={{xs: 8, sm: 16, md: 24, lg: 32}}>

                    <Col className="gutter-row" span={6}>

                    </Col>
                    <Col className="gutter-row" span={6}>

                    </Col>
                    <Col className="gutter-row" span={6}>

                    </Col>
                    <Col className="gutter-row" span={6}>

                    </Col>
                </Row>
                <Table
                    style={{minHeight: 400}}
                    loading={this.state.loading}
                    dataSource={this.state.products}
                    columns={columns}
                    pagination={false}
                    rowKey="id"
                    scroll={{x: 'calc(900px + 60%)',y: 400}}/>

                <GoodsModalForm
                    isEditing={this.state.isEditing}
                    editRow={this.state.editRow}
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.onEditModal}
                    confirmLoading={this.state.confirmLoading}
                />
            </div>
        );
    }
}

export default Recently;