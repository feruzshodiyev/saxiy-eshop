import React, {Component} from 'react';
import {Avatar, Button, Icon, notification, Popconfirm, Table} from "antd";
import LogoModalForm from "../../components/LogoModalForm";
import axios from "axios";
import {ACCESS_TOKEN, API_BASE_URL} from "../../constants";

class Logos extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: [],
            visible: false,
            confirmLoading: false

        };
    }


    componentDidMount() {
        this.fetchLogos()
    }

    fetchLogos = () => {
        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "market-logo", {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            console.log(res);
            this.setState({
                data: res.data,
                loading: false
            });
            console.log(res)
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };

    deleteOneById = (id) => {
        this.setState({
            loading: true
        });
        axios.delete(API_BASE_URL + "market-logo/" + id, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            this.fetchLogos();
            notification.success({
                message: "Логотип ўчириш амалга оширилди!"
            });
        }).catch(err => {
            notification.error({
                message: "Логотип ўчиришда хатолик юз берди!"
            });
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };






    showModal = () => {
        this.setState({visible: true});
    };

    handleCancel = () => {

        this.setState({
            editRow: {},
            isEditing: false,
            visible: false
        });

    };

    handleCreate = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);


            this.setState({
                confirmLoading: true
            });

            let formData = new FormData();

            formData.append("photo", values.photo[values.photo.length-1].originFileObj);


            axios.post(API_BASE_URL + "market-logo", formData, {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN),
                    'Content-Type':'multipart/form-data'
                }
            }).then(res => {

                this.setState({
                    confirmLoading: false,
                    visible: false
                });
                console.log("res: ", res);

                form.resetFields();
                notification.success({
                    message: "Логотип қўшиш амалга оширилди!"
                });

                this.fetchLogos()

            }).catch(err => {
                this.setState({
                    confirmLoading: false
                });
                notification.error({
                    message: "Логотип қўшишда хатолик юз берди!"
                });
                console.log("err: ", err)
            });


        });
    };

    saveFormRef = formRef => {
        this.formRef = formRef;
    };
    render() {

        const columns = [
            {
                title: 'Расм',
                dataIndex: 'photo',
                key: 'photo',
                render:(id, record) => (
                    <div>
                        <Avatar size="large" src={record.photo} shape={"square"}/>
                    </div>
                )
            },
            {
                title: 'Операция',
                key: 'id',
                dataIndex: 'id',
                render: (id, record) => (
                    <div>
                        <Popconfirm placement="top"
                                    title="Логотипни ўчирасизми?"
                                    onConfirm={() => this.deleteOneById(id)} okText="Ҳа" cancelText="Йўқ">
                            <Button
                                type="danger" icon="delete"/>
                        </Popconfirm>
                    </div>

                ),
            }
        ];

        return (
            <div>
                <Button onClick={this.showModal} type="primary">
                    <Icon type="plus-circle"/>
                    Қўшиш
                </Button>
                <Table
                    style={{minHeight: 400, marginTop: 10}}
                    loading={this.state.loading}
                    dataSource={this.state.data}
                    columns={columns}
                    pagination={false}
                    rowKey="id"
                    scroll={{y: 400}}/>

                <LogoModalForm
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                    confirmLoading={this.state.confirmLoading}
                />
            </div>
        );
    }
}

export default Logos;