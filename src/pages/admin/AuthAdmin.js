import React, {Component} from 'react';
import {Form, Icon, Input, Button, Checkbox, Row, Col} from 'antd';

import "./AuthAdminSrtyles.scss"

class AuthAdmin extends Component {

    constructor(props){
        super(props);
        this.state={

        }
    }


    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.handleLogin(values)
            }
        });
    };


    render() {
        const {loading} = this.props;
        const {getFieldDecorator} = this.props.form;
        return (
            <div>
                <div className="logo-auth"/>
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                        {getFieldDecorator('username', {
                            rules: [{required: true, message: 'Телефон рақамини киритинг!'}],
                        })(
                            <Input
                                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                placeholder="Логин"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{required: true, message: 'Парол киритинг!'}],
                        })(
                            <Input
                                prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                type="password"
                                placeholder="Парол"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item style={{display: "none"}}>
                        {getFieldDecorator('grant_type',{
                            initialValue: "password",
                            rules: [{required: true}],
                        })(
                            <Input/>,
                        )}
                    </Form.Item>
                    <Form.Item>

                        {/*{getFieldDecorator('remember', {*/}
                        {/*    valuePropName: 'checked',*/}
                        {/*    initialValue: true,*/}
                        {/*})(<Checkbox>Remember me</Checkbox>)}*/}
                        {
                            loading ?
                                <Icon style={{fontSize: 30, color: "#ffb200"}}  type="loading"/> :
                                <Button type="primary" htmlType="submit" className="login-form-button">
                                    Log in
                                </Button>
                        }

                    </Form.Item>
                </Form>
            </div>
        );
    }
}

const AntLoginForm = Form.create()(AuthAdmin);


export default AntLoginForm;