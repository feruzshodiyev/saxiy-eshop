import React, {Component} from 'react';

import {
    Table,
    Tag,
    Divider,
    Avatar,
    Popover,
    Button,
    Menu,
    Dropdown,
    Icon,
    Popconfirm,
    message,
    Row,
    Col,
    Form, Modal, notification
} from "antd";
import axios from 'axios';
import ShopModalForm from "../../components/ShopModalForm";
import {ACCESS_TOKEN, API_BASE_URL} from "../../constants";

const ButtonGroup = Button.Group;


class Shops extends Component {
    state = {
        visible: false,
        confirmLoading: false,
        data: [],
        tableLoading: false,
        editRow: {},
        isEditing: false,
        values: {}
    };


    componentDidMount() {

        this.getAllMarkets();
    }

    getAllMarkets = () => {
        this.setState({
            tableLoading: true
        });
        axios.get(API_BASE_URL + "market/ap", {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            console.log("res: ", res)
            this.setState({
                data: res.data,
                tableLoading: false
            })
        }).catch(err => {
            console.log("err: ", err)
            this.setState({
                tableLoading: false
            })
        })
    };



    changeStatus = (record) => {
        console.log(record);
        axios.put(API_BASE_URL+"market/activate/"+record.id,{},{
            params: {
                "active": !record.marketActive
            },
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res=>{
            console.log(res);
            this.getAllMarkets();
        }).catch(err=>{
            console.log(err);
            notification.error({
                message: "Маълумотларни ўзгартиришда хатолик юз берди!"
            });
        })

    };

    onEditModal = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            this.setState({
                confirmLoading: true
            });

            if (err) {
                this.setState({
                    confirmLoading: false
                });
                return;
            }
            this.setState({
                values: {
                    ...values,
                    'ownPhoneNumber': values.ownPhoneNumber.replace("(", "").replace(")","").replace(/\s/g, "")
                }
            },()=>{
                this.handleEdit(this.state.values);
            });
            form.resetFields();
        });


    };



    handleEdit = (values) => {



        console.log('Received values of form: ', values);
        axios.put(API_BASE_URL + "market/"+values.id, values, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {

            this.setState({
                confirmLoading: false,
                visible: false
            });
            console.log("res: ", res);
            this.setState({
                editRow: {},
                isEditing: false,
            });
            notification.success({
                message: "Дўкон таҳрирлаш амалга оширилди!"
            });

            this.getAllMarkets()

        }).catch(err => {
            this.setState({
                confirmLoading: false
            });
            notification.error({
                message: "Дўкон таҳрирлашда хатолик юз берди!"
            });
            console.log("err: ", err)
        });

    };

    showModalForEdit = (row) => {
        this.setState({
            visible: true,
            editRow: row,
            isEditing: true
        });
        const {form} = this.formRef.props;
        form.resetFields();
    };

    showModal = () => {
        this.setState({visible: true});
    };

    handleCancel = () => {

        this.setState({
            editRow: {},
            isEditing: false,
            visible: false});

    };

    handleCreate = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            this.setState({
                confirmLoading: true,
                values: {
                    ...values,
                    'ownPhoneNumber': values.ownPhoneNumber.replace("(", "").replace(")","").replace(/\s/g, "")
                }
            },()=>{
                axios.post(API_BASE_URL + "market", this.state.values, {
                    headers: {
                        'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
                    }
                }).then(res => {

                    this.setState({
                        confirmLoading: false,
                        visible: false
                    });
                    console.log("res: ", res);

                    form.resetFields();
                    notification.success({
                        message: "Дўкон қўшиш амалга оширилди!"
                    });

                    this.getAllMarkets()

                }).catch(err => {
                    this.setState({
                        confirmLoading: false
                    });
                    notification.error({
                        message: "Дўкон қўшишда хатолик юз берди!"
                    });
                    console.log("err: ", err)
                });
            });



        });
    };

    deleteOneById = (id) => {
        axios.delete(API_BASE_URL + "market/" + id, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            this.getAllMarkets();
            notification.success({
                message: "Дўконни ўчириш амалга оширилди!"
            });
        }).catch(err => {
            notification.error({
                message: "Дўконни ўчиришда хатолик юз берди!"
            });
            console.log(err)
        })
    };

    saveFormRef = formRef => {
        this.formRef = formRef;
    };

    render() {

        const columns = [
            {
                title: 'Статус',
                key: 'marketActive',
                dataIndex: 'marketActive',
                render: (marketActive, record) => (
                    <span>
                <Popconfirm placement="top"
                            title={marketActive ? "\"Ўчирилган\" ҳолатга ўтказасизми?" : "\"Актив\" ҳолатга ўтказасизми?"}
                            onConfirm={() => this.changeStatus(record)} okText="Ҳа" cancelText="Йўқ">
                    <Tag color={marketActive ? "green" : "red"}>
                        {marketActive ? "Актив" : "Ўчирилган"}
                    </Tag>
                </Popconfirm>
      </span>
                ),
            },
            {
                title: 'ID',
                dataIndex: 'marketId',
                key: 'marketId',
            },
            {
                title: 'Исми',
                dataIndex: 'ownFullName',
                key: 'ownFullName',
            },
            {
                title: 'Телефон',
                dataIndex: 'ownPhoneNumber',
                key: 'ownPhoneNumber',
            },
            {
                title: 'Магазин',
                dataIndex: 'name',
                key: 'name',
                width: 200,
            },
            {
                title: 'merchantId',
                dataIndex: 'merchantId',
                key: 'merchantId',
                width: 120,
            },
            {
                title: 'merchantUserId',
                dataIndex: 'merchantUserId',
                key: 'merchantUserId',
                width: 170,
            },
            {
                title: 'secretKey',
                dataIndex: 'secretKey',
                key: 'secretKey',
            },
            {
                title: 'serviceId',
                dataIndex: 'serviceId',
                key: 'serviceId',
            },

            {
                title: 'Операция',
                key: 'Операция',
                dataIndex: 'id',
                width: 120,
                fixed: 'right',
                render: (id, record) => (
                    <div>
                        <ButtonGroup>
                            <Button onClick={() => this.showModalForEdit(record)}  type="primary" icon="edit"/>
                            <Popconfirm placement="top"
                                        title="Дўконни ўчирасизми?"
                                        onConfirm={() => this.deleteOneById(id)} okText="Ҳа" cancelText="Йўқ">
                                <Button type="danger" icon="delete"/>
                            </Popconfirm>
                        </ButtonGroup>

                    </div>

                    //     <Dropdown overlay={() => <OptionMenu id={id} record={record}/>} trigger={['click']}>
                    //         <Button><Icon type="bars"/></Button>
                    //     </Dropdown>

                ),
            }
        ];


        return (
            <div>


                        <Button onClick={this.showModal}
                                type={"primary"}><Icon type="user-add"/><span>Янги дўкон қўшиш</span></Button>


                    <ShopModalForm
                        isEditing={this.state.isEditing}
                        editRow={this.state.editRow}
                        wrappedComponentRef={this.saveFormRef}
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        onCreate={this.state.isEditing ? this.onEditModal : this.handleCreate}
                        confirmLoading={this.state.confirmLoading}
                    />

                <Table
                    style={{minHeight: 400, marginTop: 10}}
                    loading={this.state.tableLoading}
                    dataSource={this.state.data}
                    columns={columns}
                    pagination={false}
                    rowKey="id"
                    scroll={{x: 'calc(700px + 60%)',y: 400}}/>


            </div>
        );
    }
}

export default Shops;