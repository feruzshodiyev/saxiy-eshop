import React, {Component} from 'react';

import {Table, Input, InputNumber, Popconfirm, Form, Button, Icon, notification} from 'antd';
import axios from 'axios';
import {ACCESS_TOKEN, API_BASE_URL} from "../../constants";
import RegionsModalForm from '../../components/RegionModalForm';

const ButtonGroup = Button.Group;

const EditableContext = React.createContext();

class EditableCell extends React.Component {
    getInput = () => {

        return <Input/>;
    };

    renderCell = ({getFieldDecorator}) => {
        const {
            editing,
            dataIndex,
            title,
            inputType,
            record,
            index,
            children,
            ...restProps
        } = this.props;
        return (
            <td {...restProps}>
                {editing ? (
                    <Form.Item style={{margin: 0}}>
                        {getFieldDecorator(dataIndex, {
                            rules: [
                                {
                                    required: true,
                                    message: `${title} киритикнг!`,
                                },
                            ],
                            initialValue: record[dataIndex],
                        })(<Input/>)}
                    </Form.Item>
                ) : (
                    children
                )}
            </td>
        );
    };

    render() {
        return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>;
    }
}


class Regions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            data: [],
            editingKey: '',
            loading: false
        };

        this.columns = [
            {
                title: 'Номи (русча)',
                dataIndex: 'nameRu',
                width: '25%',
                editable: true,
            },
            {
                title: 'Номи (ўзбекча)',
                dataIndex: 'nameUz',
                width: '40%',
                editable: true,
            },
            {
                title: 'Операция',
                dataIndex: 'id',
                render: (id, record) => {
                    const {editingKey} = this.state;
                    const editable = this.isEditing(record);
                    return editable ? (
                        <span>
              <EditableContext.Consumer>
                {form => (
                    <a
                        onClick={() => this.save(form, record.id)}
                        style={{marginRight: 8}}
                    >
                        Сақлаш
                    </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm
                  okText="Ҳа"
                  cancelText="Йоқ"
                  title="Таҳрирлашни бекор қиласизми?" onConfirm={() => this.cancel(record.id)}>
                <a>Бекор қилиш</a>
              </Popconfirm>
            </span>
                    ) : (
                        <div>
                            <ButtonGroup>
                                <Button type="primary" icon="edit" disabled={editingKey !== ''} onClick={() => this.edit(record.id)}/>

                                <Popconfirm placement="top"
                                            title="Вилоятни ўчирасизми?"
                                            onConfirm={() => this.deleteOneById(id)} okText="Ҳа" cancelText="Йўқ">
                                    <Button disabled={editingKey !== ''}
                                            type="danger" icon="delete"/>
                                </Popconfirm>
                            </ButtonGroup>

                        </div>

                    );
                },
            },
        ];
    }




    componentDidMount() {
        this.fetchRegions()
    }


    showModal = () => {
        this.setState({ visible: true });
    };

    handleCancel = () => {
        this.setState({ visible: false });
    };


    deleteOneById=(id)=>{
      console.log("id",id)
        axios.delete(API_BASE_URL+"region/"+id,{
            headers:{
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res=>{
            notification.success({message: "Вилоят ўчирилди!"})
            this.fetchRegions();
        }).catch(err=>{
            notification.error({message: "{Хатолик юз берди!"})
        })
    };


    handleCreate = () => {
        const { form } = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            axios.post(API_BASE_URL+"region", values,{
                headers:{
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
                }
            }).then(res=>{
                notification.success({
                    message: "Янги вилоят қўшилди!",
                    placement: 'topLeft'
                });
                this.fetchRegions();
            }).catch(err=>{
                notification.error({
                    message: "Вилоят қўшишда хатолик юз берди!",
                    placement: 'topLeft'
                });
            });

            form.resetFields();
            this.setState({ visible: false });
        });
    };



    saveFormRef = formRef => {
        this.formRef = formRef;
    };


    fetchRegions = () => {

        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "region/ap",{
            headers:{
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            this.setState({
                data: res.data,
                loading: false
            });
            console.log(res)
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };

    isEditing = record => {
        console.log(record);
       return  record.id === this.state.editingKey;
    };

    cancel = () => {
        this.setState({editingKey: ''});
    };

    save(form, id) {
        form.validateFields((error, row) => {
            if (error) {
                return;
            }
            const newData = [...this.state.data];
            const index = newData.findIndex(item => id === item.id);
            if (index > -1) {
                const item = newData[index];
                console.log("item", index);
                newData.splice(index, 1, {
                    ...item,
                    ...row,
                });

                axios.put(API_BASE_URL+"region/"+item.id, row, {
                    headers:{
                        'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
                    }
                    }
                ).then(res=>{
                    notification.success({
                        message: "Вилоят таҳрирланди!"
                    })
                }).catch(err=>{
                    notification.error({
                        message: "Таҳрирлашда хатолик юз берди!"
                    })
                });

                this.setState({data: newData, editingKey: ''});
            } else {
                newData.push(row);
                this.setState({data: newData, editingKey: ''});
            }


        });
    }

    edit(id) {
        console.log("key", id);
        this.setState({editingKey: id});
    }

    render() {
        const components = {
            body: {
                cell: EditableCell,
            },
        };

        const columns = this.columns.map(col => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    inputType: 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });

        return (
            <div>
                <Button onClick={this.showModal} type="primary" style={{marginBottom: 16}}>
                    <Icon type="plus-circle" />
                    Қўшиш
                </Button>
                <EditableContext.Provider value={this.props.form}>
                    <Table
                        loading={this.state.loading}
                        rowKey="id"
                        scroll={{y: 400}}
                        components={components}
                        bordered
                        dataSource={this.state.data}
                        columns={columns}
                        rowClassName="editable-row"
                        pagination={false}
                    />
                </EditableContext.Provider>
                <RegionsModalForm
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                />
            </div>

        );
    }
}

const RegionsFormTable = Form.create()(Regions);

export default RegionsFormTable;