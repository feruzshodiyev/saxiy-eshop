import React, {Component} from 'react';
import axios from "axios";
import {ACCESS_TOKEN, API_BASE_URL} from "../../constants";
import {Avatar, Button, Icon, notification, Popconfirm, Table} from "antd";
import RightSideAdModalForm from "../../components/RightSideAdModalForm";


class RightSideAd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: [],
            visible: false,
            confirmLoading: false,
            categories: [],

        };
    }


    componentDidMount() {
        this.fetchAds();
        this.fetchCategories()
    }

    fetchCategories = () => {
        this.setState({
            loading: true
        });
        axios.get(API_BASE_URL + "category/tree/", {}).then(res => {
            this.setState({
                categories: res.data,
                loading: false
            })
        }).catch(err => {
            console.log(err);
            this.setState({
                loading: false
            });
        })
    };

    fetchAds = () => {
        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "advertise", {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            console.log(res);
            this.setState({
                data: res.data,
                loading: false
            });
            console.log(res)
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };

    deleteOneById = (id) => {
        this.setState({
            loading: true
        });
        axios.delete(API_BASE_URL + "advertise/" + id, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            this.fetchAds();
            notification.success({
                message: "Слайдни ўчириш амалга оширилди!"
            });
        }).catch(err => {
            notification.error({
                message: "Слайдни ўчиришда хатолик юз берди!"
            });
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };






    showModal = () => {
        if (this.state.data.length === 0){
            this.setState({visible: true});
        } else {
            notification.error({message:"Олдин эски рекламани ўчиришингиз керак!"})
        }

    };

    handleCancel = () => {

        this.setState({
            editRow: {},
            isEditing: false,
            visible: false
        });

    };

    handleCreate = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            console.log('token: ', localStorage.getItem(ACCESS_TOKEN));

            this.setState({
                confirmLoading: true
            });

            let formData = new FormData();

            formData.append("categoryName", values.categoryName);
            const categoryId = values.categoryId[values.categoryId.length-1];
            formData.append("categoryId", categoryId);
            formData.append("photo", values.photo[values.photo.length-1].originFileObj);


            axios.post(API_BASE_URL + "advertise", formData, {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN),
                    'Content-Type':'multipart/form-data'
                }
            }).then(res => {

                this.setState({
                    confirmLoading: false,
                    visible: false
                });
                console.log("res: ", res);

                form.resetFields();
                notification.success({
                    message: "Реклама қўшиш амалга оширилди!"
                });

                this.fetchAds()

            }).catch(err => {
                this.setState({
                    confirmLoading: false
                });
                notification.error({
                    message: "Реклама қўшишда хатолик юз берди!"
                });
                console.log("err: ", err)
            });


        });
    };

    saveFormRef = formRef => {
        this.formRef = formRef;
    };


    render() {

        const columns = [
            {
                title: 'Расм',
                dataIndex: 'imgUrl',
                key: 'imgUrl',
                render:(id, record) => (
                    <div>
                        <Avatar size="large" src={record.imgUrl} shape={"square"}/>
                    </div>
                )
            },
            {
                title: 'Категория',
                dataIndex: 'categoryName',
                key: 'categoryName',
            },
            {
                title: 'Операция',
                key: 'id',
                dataIndex: 'id',
                render: (id, record) => (
                    <div>
                        <Popconfirm placement="top"
                                    title="Слайдни ўчирасизми?"
                                    onConfirm={() => this.deleteOneById(id)} okText="Ҳа" cancelText="Йўқ">
                            <Button
                                type="danger" icon="delete"/>
                        </Popconfirm>
                    </div>

                ),
            }
        ];

        return (
            <div>
                <Button onClick={this.showModal} type="primary">
                    <Icon type="plus-circle"/>
                    Қўшиш
                </Button>
                <Table
                    style={{minHeight: 400, marginTop: 10}}
                    loading={this.state.loading}
                    dataSource={this.state.data}
                    columns={columns}
                    pagination={false}
                    rowKey="id"
                    scroll={{y: 400}}/>

                <RightSideAdModalForm
                    categories={this.state.categories}
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                    confirmLoading={this.state.confirmLoading}
                />
            </div>
        );
    }
}

export default RightSideAd;