import React, {Component} from 'react';

import {Layout, Menu, Icon, Avatar, Dropdown, Row, Col} from 'antd';

import {Link, Redirect, Route, Switch, withRouter, NavLink} from 'react-router-dom';

import "./AdmiPanelStyles.scss";
import Shops from "./Shops";
import RegionsFormTable from "./Regions";
import DistrictsFormTable from "./Districts";
import DeliveryPoints from "./DeliveryPoints";
import Carriers from "./Carriers";
import Categories from "./Categories";
import Profile from "./Profile";
import Goods from "./Goods";

import Slider from "./Slider";
import RightSideAd from "./RightSideAd";
import Recommended from "./Recommended";
import Recently from "./Recently";
import Logos from "./Logos";
import {ACCESS_TOKEN} from "../../constants";
import Orders from "./Orders";

const {Header, Content, Footer, Sider} = Layout;
const {SubMenu} = Menu;

class AdminPanel extends Component {

    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    handleLogout=()=>{
        localStorage.removeItem(ACCESS_TOKEN);
        window.location.reload();
    };

    render() {
        const menu = (
            <Menu>
                <Menu.Item key="1">
                    <NavLink to="/admin/profile">
                        Профиль
                    </NavLink>
                </Menu.Item>
                <Menu.Divider/>
                <Menu.Item key="2" onClick={this.handleLogout}><Icon type="logout"/>Выйти</Menu.Item>
            </Menu>
        );
        return (
            <Layout
                style={{minHeight: '100vh'}}
            >
                <Sider

                    theme={"light"}
                    trigger={null}
                    collapsible
                    collapsed={this.state.collapsed}>
                    <div style={{
                        display: this.state.collapsed ? "none" : "block",
                        backgroundImage: 'url(http://tanti.uz:6526/api/v1/downloadFile/203791logo.jpg)',
                        backgroundPositionX:'50%'
                    }} className="logo"/>
                    <div style={{
                        display: this.state.collapsed ? "block" : "none",
                        backgroundImage: 'url(http://tanti.uz:6526/api/v1/downloadFile/118966kvadrat.jpg)'
                    }} className="logo"/>
                    <Menu mode="inline" defaultSelectedKeys={['shop']}>

                        <Menu.Item
                            key="shop">
                            <NavLink to="/admin">
                                <span>
                                    <Icon type="shop"/>
                                    <span>Магазин</span>
                                </span></NavLink>
                        </Menu.Item>


                        <Menu.Item key="carrier">
                            <NavLink to="/admin/carriers">
                                <Icon type="car"/>
                                <span>Курьер</span>
                            </NavLink>
                        </Menu.Item>


                        <SubMenu
                            key="areas"
                            title={
                                <span>
                                    <Icon type="environment"/>
                                    <span>Ҳудудлар</span>
                                </span>}
                        >

                            <Menu.Item key="region">
                                <NavLink to="/admin/regions">
                                    <span>Вилоятлар</span>
                                </NavLink>
                            </Menu.Item>
                            <Menu.Item key="district"><NavLink to="/admin/districts">Туманлар</NavLink></Menu.Item>

                            <Menu.Item key="point"><NavLink to="/admin/points">Етказиш нуқталари</NavLink></Menu.Item>
                        </SubMenu>


                        <Menu.Item key="3">
                            <NavLink to="/admin/categories">
                                <Icon type="unordered-list"/>
                                <span>Категориялар</span>
                            </NavLink>
                        </Menu.Item>

                        <Menu.Item key="4">
                            <NavLink to="/admin/goods">
                                <Icon type="tag"/>
                                <span>Товарлар</span>
                            </NavLink>
                        </Menu.Item>

                        <SubMenu
                            key="main"
                            title={
                                <span>
                                    <Icon type="layout"/>
                                    <span>Бош саҳифа</span>
                                </span>}
                        >

                            <Menu.Item key="slider">
                                <NavLink to="/admin/slider">
                                    <span>Слайдер</span>
                                </NavLink>
                            </Menu.Item>
                            <Menu.Item key="sider">
                                <NavLink to="/admin/sider">
                                    <span>Реклама</span>
                                </NavLink>
                            </Menu.Item>
                            <Menu.Item key="logos">
                                <NavLink to="/admin/logos">
                                    <span>Логотиплар</span>
                                </NavLink>
                            </Menu.Item>
                            <Menu.Item key="recommended">
                                <NavLink to="/admin/recommended">
                                    <span>Тавсия этилган товарлар</span>
                                </NavLink>
                            </Menu.Item>
                            <Menu.Item key="recently">
                                <NavLink to="/admin/recently">
                                    <span>Янги қўшилган товарлар</span>
                                </NavLink>
                            </Menu.Item>
                        </SubMenu>
                        <Menu.Item key="orders">
                            <NavLink to="/admin/orders">
                                <Icon type="file-text" />
                                <span>Буюртмалар</span>
                            </NavLink>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout
                    style={{
                        height: '100vh',
                        overflowY: 'hidden'
                    }}
                >
                    <Header className="panel-header" style={{background: '#fff', padding: 0, zIndex: 1, width: '100%'}}>
                        <Icon
                            className="trigger"
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle}
                        />
                        <Dropdown
                            className="avatar"
                            placement="bottomCenter"
                            overlayStyle={{width: "200px"}}
                            overlay={menu}
                            trigger={['click']}>
                            <Avatar icon="user" style={{backgroundColor: '#87d068'}} size={"large"}/>
                        </Dropdown>


                    </Header>
                    <Content
                        style={{
                            // overflowY:"auto",
                            height: "100%",
                            margin: '10px 10px 10px 10px',
                            padding: 5,
                            background: '#fff',
                        }}
                    >
                        <div>
                            <Switch>
                                {/*<Redirect to="/admin/shops"/>*/}
                                <Route exact path="/admin/" render={() => <Shops/>}/>
                                <Route path="/admin/shops" render={() => <Shops/>}/>
                                <Route path="/admin/regions" render={() => <RegionsFormTable/>}/>
                                <Route path="/admin/districts" render={() => <DistrictsFormTable/>}/>
                                <Route path="/admin/points" render={() => <DeliveryPoints/>}/>
                                <Route path="/admin/carriers" render={() => <Carriers/>}/>
                                <Route path="/admin/categories" render={() => <Categories/>}/>
                                <Route path="/admin/profile" render={() => <Profile/>}/>
                                <Route path="/admin/goods" render={() => <Goods/>}/>
                                <Route path="/admin/slider" render={() => <Slider/>}/>
                                <Route path="/admin/sider" render={() => <RightSideAd/>}/>
                                <Route path="/admin/logos" render={() => <Logos/>}/>
                                <Route path="/admin/recommended" render={() => <Recommended/>}/>
                                <Route path="/admin/recently" render={() => <Recently/>}/>
                                <Route path="/admin/orders" render={() => <Orders/>}/>
                            </Switch>
                        </div>

                    </Content>
                </Layout>
            </Layout>
        );
    }
}

export default withRouter(AdminPanel);