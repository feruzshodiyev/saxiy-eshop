import React, {Component} from 'react';
import {Button, Col, Icon, notification, Popconfirm, Table} from "antd";

import CarrierModalForm from "../../components/CarrierModalForm"
import {ACCESS_TOKEN, API_BASE_URL} from "../../constants";
import axios from "axios";


const ButtonGroup = Button.Group;

class Carriers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: [],
            editRow: {},
            isEditing: false,
            visible: false,
            confirmLoading: false

        };
    }


    componentDidMount() {
        this.fetchCarriers()
    }

    fetchCarriers = () => {
        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "carrier", {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            console.log(res);
            this.setState({
                data: res.data,
                loading: false
            });
            console.log(res)
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };

    deleteOneById = (id) => {
        axios.delete(API_BASE_URL + "carrier/" + id, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            this.fetchCarriers();
            notification.success({
                message: "Курьерни ўчириш амалга оширилди!"
            });
        }).catch(err => {
            notification.error({
                message: "Курьерни ўчиришда хатолик юз берди!"
            });
            console.log(err)
        })
    };


    onEditModal = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            this.setState({
                confirmLoading: true
            });

            if (err) {
                this.setState({
                    confirmLoading: false
                });
                return;
            }

            this.handleEdit(values);

        });

        // form.resetFields();
    };

    handleEdit = (values) => {

        const {form} = this.formRef.props;

        console.log('Received values of form: ', values);

        axios.put(API_BASE_URL + "carrier/" + values.id, values, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {

            this.setState({
                confirmLoading: false,
                visible: false
            });
            console.log("res: ", res);
            this.setState({
                editRow: {},
                isEditing: false,
            });
            notification.success({
                message: "Курьер таҳрирлаш амалга оширилди!"
            });

            this.fetchCarriers()
            form.resetFields();
        }).catch(err => {
            this.setState({
                confirmLoading: false
            });
            notification.error({
                message: "Курьер таҳрирлашда хатолик юз берди!"
            });
            console.log("err: ", err)
        });

    };


    showModalForEdit = (row) => {
        this.setState({
            visible: true,
            editRow: row,
            isEditing: true
        });
        const {form} = this.formRef.props;
        form.resetFields();
    };


    showModal = () => {
        this.setState({visible: true});
    };

    handleCancel = () => {

        this.setState({
            editRow: {},
            isEditing: false,
            visible: false
        });

    };

    handleCreate = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);
            console.log('token: ', localStorage.getItem(ACCESS_TOKEN));

            this.setState({
                confirmLoading: true
            });
            axios.post(API_BASE_URL + "carrier", values, {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
                }
            }).then(res => {

                this.setState({
                    confirmLoading: false,
                    visible: false
                });
                console.log("res: ", res);

                form.resetFields();
                notification.success({
                    message: "Курьер қўшиш амалга оширилди!"
                });

                this.fetchCarriers()

            }).catch(err => {
                this.setState({
                    confirmLoading: false
                });
                notification.error({
                    message: "Курьер қўшишда хатолик юз берди!"
                });
                console.log("err: ", err)
            });


        });
    };

    saveFormRef = formRef => {
        this.formRef = formRef;
    };


    render() {

        const columns = [
            {
                title: 'Номи',
                dataIndex: 'name',
                key: 'name'
            },
            {
                title: 'Телефон рақами',
                dataIndex: 'phoneNumber',
                key: 'phoneNumber',
            },
            {
                title: 'Операция',
                key: 'id',
                dataIndex: 'id',
                render: (id, record) => (
                    <div>
                        <ButtonGroup>
                            <Button onClick={() => this.showModalForEdit(record)} type="primary" icon="edit"/>
                            <Popconfirm placement="top"
                                        title="Дўконни ўчирасизми?"
                                        onConfirm={() => this.deleteOneById(id)} okText="Ҳа" cancelText="Йўқ">
                                <Button
                                    type="danger" icon="delete"/>
                            </Popconfirm>
                        </ButtonGroup>
                    </div>

                ),
            }
        ];

        return (
            <div>
                <Button onClick={this.showModal} type="primary">
                    <Icon type="plus-circle"/>
                    Қўшиш
                </Button>
                <Table
                    style={{minHeight: 400, marginTop: 10}}
                    loading={this.state.loading}
                    dataSource={this.state.data}
                    columns={columns}
                    pagination={false}
                    rowKey="id"
                    scroll={{y: 400}}/>

                <CarrierModalForm
                    isEditing={this.state.isEditing}
                    editRow={this.state.editRow}
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.state.isEditing ? this.onEditModal : this.handleCreate}
                    confirmLoading={this.state.confirmLoading}
                />
            </div>
        );
    }
}

export default Carriers;