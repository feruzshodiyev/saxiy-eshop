import React, {Component} from 'react';
import {Avatar, Button,  Col, Icon, notification, Popconfirm, Row, Select, Table, Tag} from "antd";
import axios from "axios";
import {ACCESS_TOKEN, API_BASE_URL} from "../../constants";
import DeliveryPointModalForm from "../../components/DeliveryPointModalForm";
import './DeliveryPoints.scss';

const { Option } = Select;
const ButtonGroup = Button.Group;

class DeliveryPoints extends Component {
    constructor(props){
        super(props);
        this.state={
            loading: false,
            points: [],
            districts: [],
            regions: [],
            defaultRegion: {},
            selectedRegionId: null,
            defaultDistrict: {},
            selectedDistrictId: null,
            editRow: {},
            isEditing: false,
            visible: false,
            confirmLoading: false
        };
    }


    componentDidMount() {
        this.fetchRegions();
    }


    fetchRegions = () => {

        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "region",{
            headers:{
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {

            const firstReg = res.data[0];
            this.setState({
                regions: res.data,
                defaultRegion: firstReg,
                selectedRegionId: firstReg.id,
                loading: false
            },()=>this.fetchDistricts());

            console.log(res)
        }).catch(err => {
            this.setState({
                regions: [],
                defaultRegion: {},
                selectedRegionId: 0,
                loading: false
            });
            console.log(err)
        })
    };



    fetchDistricts=()=>{
        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "district/ap/dist-id/"+this.state.selectedRegionId,{
            headers:{
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            const firstdist = res.data[0];
            this.setState({
                districts: res.data,
                defaultDistrict: firstdist,
                selectedDistrictId: firstdist.id,
                loading: false
            }, ()=>this.fetchPoints());
            console.log(res)
        }).catch(err => {
            this.setState({
                districts: [],
                defaultDistrict: {},
                selectedDistrictId: 0,
                loading: false
            });
            console.log(err)
        })
    };


    fetchPoints=()=>{
        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "delivery-point/district/ap/"+this.state.selectedDistrictId,{
            headers:{
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            this.setState({
                points: res.data,
                loading: false
            });
            console.log(res)
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };


    deleteOneById = (id) => {
        axios.delete(API_BASE_URL + "delivery-point/" + id, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            this.fetchPoints();
            notification.success({
                message: "Етказиш нуқтасини ўчириш амалга оширилди!"
            });
        }).catch(err => {
            notification.error({
                message: "Етказиш нуқтасини ўчиришда хатолик юз берди!"
            });
        })
    };


    onEditModal = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            this.setState({
                confirmLoading: true
            });

            if (err) {
                this.setState({
                    confirmLoading: false
                });
                return;
            }

            this.handleEdit(values);

            form.resetFields();
        });


    };



    handleEdit = (values) => {

        // const values = props.values;

        console.log('Received values of form: ', values);

        axios.put(API_BASE_URL + "delivery-point/"+values.id, {
            "deliveryMessageRu": values.deliveryMessageRu,
            "deliveryMessageUz": values.deliveryMessageUz,
            "distance": values.distance,
            "districtId": this.state.selectedDistrictId,
            "nameRu": values.nameRu,
            "nameUz": values.nameUz,
            "price": values.price
        }, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {

            this.setState({
                confirmLoading: false,
                visible: false
            });
            console.log("res: ", res);

            notification.success({
                message: "Етказиш нуқтасини таҳрирлаш амалга оширилди!"
            });

            this.fetchPoints()

        }).catch(err => {
            this.setState({
                confirmLoading: false
            });
            notification.error({
                message: "Етказиш нуқтасини таҳрирлашда хатолик юз берди!"
            });
            console.log("err: ", err)
        });

    };



    handleChangeDistrict=(value)=>{
this.setSelectedDistrict(value)
    };

    setSelectedDistrict=(value)=>{

        this.setState({
            selectedDistrictId: value
        }, ()=>{
            this.fetchPoints();
        })

    };


    handleChangeRegion=(value)=>{
        this.setState({
            selectedRegionId: value
        },()=>this.fetchDistricts());

    };


    showModalForEdit = (row) => {
        this.setState({
            visible: true,
            editRow: row,
            isEditing: true
        });
        const {form} = this.formRef.props;
        form.resetFields();
    };

    showModal = () => {
        if (this.state.selectedDistrictId) {
            this.setState({visible: true});
        }else {
            notification.error({
                message: "Туман мавжуд емас!",
                description: "Илтимос, аввал туман танланг."
            })
        }

    };

    handleCancel = () => {

        this.setState({
            editRow: {},
            isEditing: false,
            visible: false});

    };

    handleCreate = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);
            console.log('token: ', localStorage.getItem(ACCESS_TOKEN));

            this.setState({
                confirmLoading: true
            });
            axios.post(API_BASE_URL + "delivery-point", {
                "deliveryMessageRu": values.deliveryMessageRu,
                "deliveryMessageUz": values.deliveryMessageUz,
                "distance": values.distance,
                "districtId": this.state.selectedDistrictId,
                "nameRu": values.nameRu,
                "nameUz": values.nameUz,
                "price": values.price
            }, {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
                }
            }).then(res => {

                this.setState({
                    confirmLoading: false,
                    visible: false
                });
                console.log("res: ", res);

                form.resetFields();
                notification.success({
                    message: "Етказиш нуқтаси қўшиш амалга оширилди!"
                });

                this.fetchPoints()

            }).catch(err => {
                this.setState({
                    confirmLoading: false
                });
                notification.error({
                    message: "Етказиш нуқтаси қўшишда хатолик юз берди!"
                });
                console.log("err: ", err)
            });


        });
    };




    saveFormRef = formRef => {
        this.formRef = formRef;
    };



    render() {

        const columns = [
            {
                title: 'Номи (русча)',
                dataIndex: 'nameRu',
                key: 'nameRu'
            },
            {
                title: 'Номи (ўзбекча)',
                dataIndex: 'nameUz',
                key: 'nameUz',
            },
            {
                title: 'Масофа (км)',
                dataIndex: 'distance',
                key: 'distance',
            },
            {
                title: 'Етказиш нархи',
                dataIndex: 'price',
                key: 'price',
            },
            {
                title: 'Хабар (русча)',
                dataIndex: 'deliveryMessageRu',
                key: 'deliveryMessageRu',
                className: 'text-area-column',
            },
            {
                title: 'Хабар (ўзбекча)',
                dataIndex: 'deliveryMessageUz',
                key: 'deliveryMessageUz',
                className: 'text-area-column',
            },
            {
                title: 'Операция',
                key: 'id',
                dataIndex: 'id',
                render: (id, record) => (
                    <div>
                        <ButtonGroup>
                        <Button onClick={() => this.showModalForEdit(record)} type="primary" icon="edit"/>
                        <Popconfirm placement="top"
                                    title="Дўконни ўчирасизми?"
                                    onConfirm={() => this.deleteOneById(id)} okText="Ҳа" cancelText="Йўқ">
                            <Button
                                    type="danger" icon="delete"/>
                        </Popconfirm>
                        </ButtonGroup>
                    </div>

                ),
            }
        ];

        return (
            <div>
                <Row>
                    <Col xs={{ span: 5 }} lg={{ span: 6 }}>
                        <Button onClick={this.showModal} type="primary" style={{marginBottom: 16}}>
                            <Icon type="plus-circle" />
                            Қўшиш
                        </Button>
                    </Col>
                    <Col xs={{ span: 11, offset: 1 }} lg={{ span: 6, offset: 2 }}>


                        <Select
                            // labelInValue
                            onChange={this.handleChangeRegion}
                            defaultValue={"def"}
                            style={{ width: "100%" }}
                            // onChange={handleChange}
                        >
                            <Option style={{display: this.state.regions.length > 0 ? "none" : "contents"}} key={"def"}>{this.state.regions.length > 0 ?this.state.defaultRegion.name : "Вилоят топилмади"}</Option>
                            { this.state.regions.map(item=>(
                                <Option key={item.id}>{item.name}</Option>
                            ))}
                        </Select>



                    </Col>
                    <Col xs={{ span: 5, offset: 1 }} lg={{ span: 6, offset: 2 }}>
                        <Select
                            // labelInValue
                            onChange={this.handleChangeDistrict}
                            defaultValue={"def"}
                            style={{ width: "100%" }}
                            // onChange={handleChange}
                        >
                            <Option style={{display: this.state.districts.length > 0 ? "none" : "contents"}} key={"def"}>{this.state.districts.length > 0 ?this.state.defaultDistrict.nameUz : "Туман топилмади"}</Option>
                            { this.state.districts.map(item=>(
                                <Option key={item.id}>{item.nameUz}</Option>
                            ))}
                        </Select>
                    </Col>
                </Row>

                <Table
                    style={{minHeight: 400}}
                    loading={this.state.loading}
                    dataSource={this.state.points}
                    columns={columns}
                    pagination={false}
                    rowKey="id"
                    scroll={{y: 400}}/>

                <DeliveryPointModalForm
                    isEditing={this.state.isEditing}
                    editRow={this.state.editRow}
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.state.isEditing ? this.onEditModal : this.handleCreate}
                    confirmLoading={this.state.confirmLoading}
                />

            </div>
        );
    }
}

export default DeliveryPoints;