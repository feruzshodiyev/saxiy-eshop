import React, {Component} from 'react';
import {Table, Badge, Menu, Dropdown, Icon, notification, Button, Popconfirm, Avatar} from 'antd';
import CategoryModalForm from "../../components/CategoryModalForm";
import {ACCESS_TOKEN, API_BASE_URL} from "../../constants";
import axios from "axios";
import ExpandTable from "../../components/ExpandTable";

const ButtonGroup = Button.Group;

class Categories extends Component {
    constructor(props){
        super(props);
        this.state={
            data: [],
            loading: false,
            editRow: {},
            isEditing: false,
            visible: false,
            confirmLoading: false,
        }
    }

    componentDidMount() {

        this.fetchCatigories();
    }


    fetchCatigories = () => {
        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "category/ap", {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            console.log(res);
            this.setState({
                data: res.data,
                loading: false
            });
            console.log(res)
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };




    handleCreate = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);

            let formData = new FormData();

            formData.append("nameRu", values.nameRu);
            formData.append("nameUz", values.nameUz);
            formData.append("parentId", values.parentId);
            formData.append("photo", values.photo[values.photo.length-1].originFileObj);



            this.setState({
                confirmLoading: true
            });
            axios.post(API_BASE_URL + "category", formData, {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
                }
            }).then(res => {

                this.setState({
                    confirmLoading: false,
                    visible: false
                });
                console.log("res: ", res);

                form.resetFields();
                notification.success({
                    message: "Категория қўшиш амалга оширилди!"
                });

                this.fetchCatigories()

            }).catch(err => {
                this.setState({
                    confirmLoading: false
                });
                notification.error({
                    message: "Категория қўшишда хатолик юз берди!"
                });
                console.log("err: ", err)
            });


        });
    };


    showModalForEdit = (row) => {
        this.setState({
            visible: true,
            editRow: row,
            isEditing: true
        });
    };

    showModal = () => {
        this.setState({visible: true});
    };

    handleCancel = () => {

        this.setState({
            editRow: {},
            isEditing: false,
            visible: false
        });

    };


    deleteOneById = (id) => {
        axios.delete(API_BASE_URL + "category/" + id, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {
            this.fetchCatigories();
            notification.success({
                message: "Категорияни ўчириш амалга оширилди!"
            });
        }).catch(err => {
            notification.error({
                message: "Категорияни ўчиришда хатолик юз берди!"
            });
            console.log(err)
        })
    };

    onEditModal = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            this.setState({
                confirmLoading: true
            });

            if (err) {
                this.setState({
                    confirmLoading: false
                });
                return;
            }

            this.handleEdit(values);

        });

        // form.resetFields();
    };

    handleEdit = (values) => {

        const {form} = this.formRef.props;

        console.log('Received values of form: ', values);

        let formData = new FormData();

        formData.append("nameRu", values.nameRu);
        formData.append("nameUz", values.nameUz);

        if (values.parentId){
            formData.append("parentId", values.parentId);
        }
        if (values.photo){
            formData.append("photo", values.photo[values.photo.length-1].originFileObj);
        }


        axios.put(API_BASE_URL + "category/" + values.id, formData, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN)
            }
        }).then(res => {

            this.setState({
                confirmLoading: false,
                visible: false
            });
            console.log("res: ", res);
            this.setState({
                editRow: {},
                isEditing: false,
            });
            notification.success({
                message: "Категория таҳрирлаш амалга оширилди!"
            });

            this.fetchCatigories();
            form.resetFields();
        }).catch(err => {
            this.setState({
                confirmLoading: false
            });
            notification.error({
                message: "Категория таҳрирлашда хатолик юз берди!"
            });
            console.log("err: ", err)
        });

    };


    saveFormRef = formRef => {
        this.formRef = formRef;
    };

    render() {

        const columns = [
            { title: 'ID', dataIndex: 'id', key: 'id' },
            { title: 'Номи (русча)', dataIndex: 'nameRu', key: 'nameRu' },
            { title: 'Номи (ўзбекча)', dataIndex: 'nameUz', key: 'nameUz' },
            { title: 'Иконка', dataIndex: 'icon', key: 'icon', render: (icon, record) => ( <Avatar src={icon} shape={"square"}/> )  },
            { title: 'Операция', key: 'operation',
                dataIndex: 'id',
                render: (id, record) => (
                    <div>
                        <ButtonGroup>
                            <Button onClick={() => this.showModalForEdit(record)} type="primary" icon="edit"/>
                            <Popconfirm placement="top"
                                        title="Категорияни ўчирасизми?"
                                        onConfirm={() => this.deleteOneById(id)} okText="Ҳа" cancelText="Йўқ">
                                <Button
                                    type="danger" icon="delete"/>
                            </Popconfirm>
                        </ButtonGroup>
                    </div>) },
        ];


        return (
            <div>
                <Button size="large" onClick={this.showModal} type="primary" >
                    <Icon type="plus-circle"/>
                    Янги категория қўшиш
                </Button>
                <Table
                    pagination={false}
                    style={{minHeight: 400, marginTop: 10}}
                    rowKey="id"
                    className="components-table-demo-nested"
                    columns={columns}
                    expandedRowRender={((record, index, indent, expanded) => <ExpandTable
                        index={index}
                        indent={indent}
                        record={record}
                    />)}
                    dataSource={this.state.data}
                    loading={this.state.loading}
                    scroll={{y: 400}}
                />

                <CategoryModalForm
                    isEditing={this.state.isEditing}
                    editRow={this.state.editRow}
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.state.isEditing ? this.onEditModal : this.handleCreate}
                    confirmLoading={this.state.confirmLoading}/>
            </div>
        );
    }
}

export default Categories;