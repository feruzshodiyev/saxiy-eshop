import React, {Component} from 'react';
import {Avatar, Button, Col, Icon, InputNumber, notification, Popconfirm, Row, Select, Table, Tag} from "antd";
import axios from "axios";
import {ACCESS_TOKEN_SHOP, API_BASE_URL} from "../../constants";
import ProductModalForm from './components/ProductModalForm';

const {Option} = Select;
const ButtonGroup = Button.Group;

class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            parentCategories: [],
            defaultParent: {},
            selectedParentId: null,
            subCategories1: [],
            defaultSub1: {},
            selectedSub1Id: null,
            subCategories2: [],
            defaultSub2: {},
            currentName: '',
            currentId: null,
            currentIcon: '',

            products: [],

            editRow: {},
            isEditing: false,
            visible: false,
            confirmLoading: false,
            increasePercent: 0
        }
    }

    componentDidMount() {
        this.fetchParents();
    }


    fetchProducts = (id) => {
        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "product/personal/" + id, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_SHOP)
            }
        }).then(res => {
            this.setState({
                products: res.data,
                loading: false
            });
            console.log('products', res)
        }).catch(err => {
            this.setState({
                loading: false
            });
            console.log(err)
        })
    };


    fetchParents = () => {
        this.setState({
            loading: true
        });
        axios.get(API_BASE_URL + "category/ap", {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_SHOP)
            }
        }).then(res => {
            console.log(res);
            this.setState({
                parentCategories: res.data,
                defaultParent: res.data[0],
                selectedParentId: res.data[0].id,
                loading: false
            }, () => {
                this.getSubCategories1()
            });
            console.log(res)
        }).catch(err => {

            console.log(err);

            this.setState({
                loading: false
            });
        })
    };


    getSubCategories1 = () => {
        this.setState({
            loading: true
        });

        axios.get(API_BASE_URL + "category/ap", {
            params: {parentId: this.state.selectedParentId},
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_SHOP)
            }
        }).then(res => {
            console.log('sub ', res);
            if (res.data.length > 0) {
                this.setState({
                    subCategories1: res.data,
                    defaultSub1: res.data[0],
                    selectedSub1Id: res.data[0].id,

                }, () => {
                    this.getSubCategories2()
                })
            } else {
                this.setState({
                    subCategories1: [],
                    defaultSub1: {},
                    selectedSub1Id: null,
                    subCategories2: [],
                    defaultSub2: {},
                    selectedSub2Id: null,
                });
                this.setCurrent(this.state.defaultParent)
            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading: false
            });
        });
    };

    getSubCategories2 = () => {
        this.setState({
            loading: true
        });
        axios.get(API_BASE_URL + "category/ap", {
            params: {parentId: this.state.selectedSub1Id},
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_SHOP)
            }
        }).then(res => {
            console.log('sub ', res);
            if (res.data.length > 0) {
                this.setState({
                    subCategories2: res.data,
                    defaultSub2: res.data[0],
                    selectedSub2Id: res.data[0].id,
                }, () => this.setCurrent(this.state.defaultSub2))
            } else {

                this.setState({
                    subCategories2: [],
                    defaultSub2: {},
                    selectedSub2Id: null,
                });
                this.setCurrent(this.state.defaultSub1)
            }
        }).catch(err => {
            console.log(err)
            this.setState({
                loading: false
            });
        });
    };


    setCurrent = (current) => {
        this.setState({
            currentName: current.nameUz,
            currentId: current.id,
            currentIcon: current.icon
        });
        this.fetchProducts(current.id)
    };

    handleChangeParent = (value) => {
        console.log('sel: ', value);
        this.state.parentCategories.map(item => {
            console.log('item ', item);
            if (item.id.toString() === value) {
                console.log('eq ', item);
                this.setState({
                    defaultParent: item,
                    selectedParentId: value,

                }, () => this.getSubCategories1())
            }
        })
    };

    handleChangeSub1 = (value) => {
        this.state.subCategories1.map(item => {
            console.log('item ', item);
            if (item.id.toString() === value) {
                console.log('eq ', item);
                this.setState({
                    defaultSub1: item,
                    selectedSub1Id: value,

                }, () => this.getSubCategories2())
            }
        })
    };
    handleChangeSub2 = (value) => {
        this.state.subCategories2.map(item => {
            console.log('item ', item);
            if (item.id.toString() === value) {
                console.log('eq ', item);
                this.setState({
                    defaultSub2: item,
                    selectedSub2Id: value,
                }, () => this.setCurrent(this.state.defaultSub2))
            }
        })
    };

    showModal = () => {
        if (this.state.currentId) {
            this.setState({visible: true});
        } else {
            notification.error({
                message: "Категория мавжуд емас!",
                description: "Илтимос, аввал категория танланг."
            })
        }

    };

    handleCancel = () => {
        this.setState({
            editRow: {},
            isEditing: false,
            visible: false
        });
        const {form} = this.formRef.props;
        form.resetFields();
    };

    handleCreate = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('vals: ', values);

            this.setState({
                confirmLoading: true
            });

            let formData = new FormData();

            formData.append("nameUz", values.nameUz);
            formData.append("nameRu", values.nameRu);
            formData.append("infoUz", values.infoUz);
            formData.append("infoRu", values.infoRu);
            formData.append("amount", values.amount);
            formData.append("height", values.height);
            formData.append("photo", values.photo[values.photo.length - 1].originFileObj);
            formData.append("length", values.length);
            formData.append("price", values.price);
            formData.append("weight", values.weight);
            formData.append("width", values.width);
            formData.append("discount", values.discount);
            formData.append("discountPrice", values.discountPrice);
            formData.append("category", this.state.currentId);
            formData.append("active", values.active);
            formData.append("productKey", values.productKey);
            formData.append("deliveryDay", values.deliveryDay);
            formData.append("deliveryPrice", values.deliveryPrice);


            axios.post(API_BASE_URL + "product", formData, {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_SHOP)
                }
            }).then(res => {

                this.setState({
                    confirmLoading: false,
                    visible: false
                });
                console.log("res: ", res);

                form.resetFields();
                notification.success({
                    message: "Товар қўшиш амалга оширилди!"
                });

                this.fetchProducts(this.state.currentId)

            }).catch(err => {
                this.setState({
                    confirmLoading: false
                });
                notification.error({
                    message: "Товар қўшишда хатолик юз берди!"
                });
                console.log("err: ", err)
            });
        });
    };


    showModalForEdit = (row) => {
        this.setState({
            visible: true,
            editRow: row,
            isEditing: true
        });
        const {form} = this.formRef.props;
        form.resetFields();
    };


    onEditModal = () => {
        const {form} = this.formRef.props;
        form.validateFields((err, values) => {
            this.setState({
                confirmLoading: true
            });

            if (err) {
                this.setState({
                    confirmLoading: false
                });
                return;
            }

            this.handleEdit(values);

            form.resetFields();
        });


    };

    handleEdit = (values) => {

        console.log('values', values);
        let formData = new FormData();

        formData.append("id", values.id);
        formData.append("nameUz", values.nameUz);
        formData.append("nameRu", values.nameRu);
        formData.append("infoUz", values.infoUz);
        formData.append("infoRu", values.infoRu);
        formData.append("amount", values.amount);
        formData.append("height", values.height);
        formData.append("length", values.length);
        formData.append("price", values.price);
        formData.append("weight", values.weight);
        formData.append("width", values.width);
        formData.append("discount", values.discount);
        formData.append("discountPrice", values.discountPrice);
        formData.append("category", this.state.currentId);
        formData.append("active", values.active);
        formData.append("productKey", values.productKey);
        formData.append("deliveryPrice", values.deliveryPrice);
        formData.append("deliveryDay", values.deliveryDay);
        if (values.photo) {
            formData.append("photo", values.photo[values.photo.length - 1].originFileObj);
        }

        axios.put(API_BASE_URL + "product/" + values.id, formData, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_SHOP)
            }
        }).then(res => {

            this.setState({
                confirmLoading: false,
                visible: false
            });
            console.log("res: ", res);

            notification.success({
                message: "Товарни таҳрирлаш амалга оширилди!"
            });

            this.fetchProducts(this.state.currentId)

        }).catch(err => {
            this.setState({
                confirmLoading: false
            });
            notification.error({
                message: "Товарни таҳрирлашда хатолик юз берди!"
            });
            console.log("err: ", err)
        });

    };


    saveFormRef = formRef => {
        this.formRef = formRef;
    };


    deleteOneById = (id) => {
        axios.delete(API_BASE_URL + "product/" + id, {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(ACCESS_TOKEN_SHOP)
            }
        }).then(res => {
            this.fetchProducts(this.state.currentId);
            notification.success({
                message: "Товарни ўчириш амалга оширилди!"
            });
        }).catch(err => {
            console.log(err);
            notification.error({
                message: "Товарни ўчиришда хатолик юз берди!"
            });
        })
    };
    inputChange = (value) => {
        this.setState({
            increasePercent: value
        })
    };

    increasePrice = () => {
        axios.post(API_BASE_URL + "product/increase-price", {},{

            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem(ACCESS_TOKEN_SHOP)
            },
            params:{
                "category": this.state.currentId,
                "percentage": this.state.increasePercent
            }
        }).then(res => {
            console.log(res);
            this.fetchProducts(this.state.currentId);
            notification.success({
                message: "Товарлар нархини ошириш амалга оширилди!"
            });
        }).catch(err => {
            console.log(err);
            notification.error({
                message: "Товарлар нархини оширишда хатолик юз берди!"
            });
        })
    };


    render() {

        const columns = [
            {
                title: 'Товар',
                dataIndex: 'img',
                key: 'productImage',
                render: (id, record) => (
                    <div>
                        <Avatar src={record.img} shape={"square"}/>
                    </div>
                )
            },
            {
                title: 'Номи (русча)',
                dataIndex: 'nameRu',
                key: 'nameRu',
                width: 200,
                ellipsis: true
            },
            {
                title: 'Номи (ўзбекча)',
                dataIndex: 'nameUz',
                key: 'nameUz',
                width: 200,
                ellipsis: true
            },
            {
                title: 'Миқдори (дона)',
                dataIndex: 'amount',
                key: 'amount',
                width: 100,
            },
            {
                title: 'Нархи',
                dataIndex: 'price',
                key: 'price',
            },
            {
                title: 'Скидка',
                dataIndex: 'discountPrice',
                key: 'discountPrice',
            },
            {
                title: 'Узунлиги',
                dataIndex: 'length',
                key: 'length',
                width: 100,
            },
            {
                title: 'Баландлиги',
                dataIndex: 'height',
                key: 'height',
                width: 120,
            },
            {
                title: 'Эни',
                dataIndex: 'width',
                key: 'width',
            },
            {
                title: 'Оғирлиги',
                dataIndex: 'weight',
                key: 'weight',

            },
            {
                title: 'Махсус ID',
                dataIndex: 'productKey',
                key: 'productKey',
                width: 100,
            },
            {
                title: 'Статус',
                dataIndex: 'active',
                key: 'active',
                render: (active) => (
                    <Tag color={active ? "green" : "red"}>
                        {active ? "Актив" : "Ўчирилган"}
                    </Tag>
                )
            },
            {
                title: 'Операция',
                key: 'id',
                dataIndex: 'id',
                fixed: 'right',
                render: (id, record) => (
                    <div>
                        <ButtonGroup>
                            <Button onClick={() => this.showModalForEdit(record)} type="primary" icon="edit"/>
                            <Popconfirm placement="top"
                                        title="Товарни ўчирасизми?"
                                        onConfirm={() => this.deleteOneById(id)} okText="Ҳа" cancelText="Йўқ">
                                <Button
                                    type="danger" icon="delete"/>
                            </Popconfirm>
                        </ButtonGroup>
                    </div>

                ),
            }
        ];
        return (
            <div>
                <Row gutter={{xs: 8, sm: 16, md: 24, lg: 32}}>

                    <Col className="gutter-row" span={6}>
                        <Select onChange={this.handleChangeParent} defaultValue={"def"} style={{width: '100%'}}>
                            <Option style={{display: this.state.parentCategories.length > 0 ? "none" : "block"}}
                                    key={"def"}>{this.state.parentCategories.length > 0 ? this.state.defaultParent.nameUz : "Категегория топилмади"}</Option>
                            {this.state.parentCategories.map(item => (
                                <Option key={item.id}>{item.nameUz}</Option>
                            ))}
                        </Select>
                    </Col>
                    <Col className="gutter-row" span={6}>
                        {this.state.subCategories1.length > 0 ?
                            <Select onChange={this.handleChangeSub1} defaultValue="def" style={{width: '100%'}}>
                                <Option style={{display: this.state.subCategories1.length > 0 ? "none" : "block"}}
                                        key={"def"}>{this.state.subCategories1.length > 0 ? this.state.defaultSub1.nameUz : "Категегория топилмади"}</Option>
                                {this.state.subCategories1.map(item => (
                                    <Option key={item.id}>{item.nameUz}</Option>
                                ))}
                            </Select> : ""}
                    </Col>
                    <Col className="gutter-row" span={6}>
                        {this.state.subCategories2.length > 0 ?
                            <Select onChange={this.handleChangeSub2} defaultValue="def" style={{width: '100%'}}>
                                <Option style={{display: this.state.subCategories2.length > 0 ? "none" : "block"}}
                                        key={"def"}>{this.state.subCategories2.length > 0 ? this.state.defaultSub2.nameUz : "Категегория топилмади"}</Option>
                                {this.state.subCategories2.map(item => (
                                    <Option key={item.id}>{item.nameUz}</Option>
                                ))}
                            </Select> : ""}
                    </Col>
                    <Col className="gutter-row" span={6}>
                        <Button onClick={this.showModal} icon="plus" type="primary">Товар қўшиш</Button>
                    </Col>
                </Row>
                <Row style={{marginTop: 10}}>
                    <Col span={6}>
                        <p><span style={{fontWeight: "bold", color: '#ff6600'}}>Категория:</span> <Icon
                            type={this.state.currentIcon}/> {this.state.currentName}</p>
                    </Col>
                    <Col span={8}>
                        <p>Kategoriyadagi mahsuotlar narxini % da oshirish:</p>
                    </Col>
                    <Col span={3}>
                        <InputNumber min={0} onChange={this.inputChange}/>
                    </Col>
                    <Col span={6}>
                        <Button
                            onClick={this.increasePrice}
                            disabled={this.state.increasePercent === 0}
                            type={"primary"}>Yuborish</Button>
                    </Col>
                </Row>
                <Table
                    style={{minHeight: 400}}
                    loading={this.state.loading}
                    dataSource={this.state.products}
                    columns={columns}
                    pagination={false}
                    rowKey="id"
                    scroll={{x: 'calc(900px + 60%)', y: 400}}/>

                <ProductModalForm
                    isEditing={this.state.isEditing}
                    editRow={this.state.editRow}
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.state.isEditing ? this.onEditModal : this.handleCreate}
                    confirmLoading={this.state.confirmLoading}
                />
            </div>
        );
    }
}

export default Products;