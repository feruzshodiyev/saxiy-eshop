import React, {Component} from 'react';
import AntLoginFormShop from './AuthShop';
import {Switch, Route, Redirect} from 'react-router-dom';
import {ACCESS_TOKEN, ACCESS_TOKEN_SHOP, API_BASE_URL} from "../../constants";
import axios from "axios";
import ShopAdminPanel from "./ShopAdminPanel";
import {notification} from "antd";
import {Sugar} from "react-preloaders";

class ShopAdminWrapper extends Component {
    constructor(props){
        super(props);

        this.state={
            isAuthenticated: false,
            isModerator: false,
            loading: true
        }
    }


    componentDidMount() {
        this.checkAuth();
    }


    checkAuth = () => {
        const token = localStorage.getItem(ACCESS_TOKEN_SHOP);

        if (token) {
            axios.get(API_BASE_URL + "user/details", {
                headers: {
                    'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_SHOP)
                }
            }).then(res => {
                console.log(res);
                if (res.data.role ==='MODERATOR'){
                    this.setState({
                        token: token,
                        isAuthenticated: true,
                        isModerator: true,
                        loading: false,
                        user: res.data.fullName
                    })
                } else {
                    notification.error({message: "Фойдаланувчи дўкон емас!"})
                    localStorage.removeItem(ACCESS_TOKEN_SHOP)
                    this.setState({
                        loading:false
                    })
                }

            }).catch(err => {

                if (err.response &&err.response.status === 401) {
                        localStorage.removeItem(ACCESS_TOKEN_SHOP)
                    }


                this.setState({
                    loading:false
                });
            });

        }else {
            this.setState({
                loading:false
            });
        }
    };


    handleLogin = (values) => {
        this.setState({
            loading:true
        });
        console.log("call!", values);

        axios.post("http://saxiysavdo.uz:6526/oauth/token", {}, {
            headers: {
                'Authorization': "Basic d2ViOll5NWpydUJtbVdUakREajY=",
            },
            params: {
                "username": values.username,
                "password": values.password,
                "grant_type": values.grant_type
            }
        }).then(res => {
            console.log("res: ", res.data.access_token);
            localStorage.setItem(ACCESS_TOKEN_SHOP, res.data.access_token);
            this.checkAuth()
        }).catch(err => {

            console.log("err", err);
            this.setState({
                loading:false
            });
        })

    };



    render() {
        return (
            <div>
                <Sugar
                    customLoading={this.state.loading}
                    color={'#4000ff'}/>
                <Switch>

                    <Route path="/shop/auth"
                           render={(props) => !this.state.isAuthenticated || !this.state.isModerator ? <AntLoginFormShop
                                   handleLogin={(values) => this.handleLogin(values)}
                                   loading={this.state.loading}
                                   {...props}
                               /> :
                               <Redirect to="/shop"/>}/>



                    {this.state.isAuthenticated && this.state.isModerator ?
                        <Route exact path="/shop" render={(props) => <ShopAdminPanel
                            token={this.state.token}
                            user={this.state.user}
                            {...props}
                        />}/> :
                        <Redirect to="/shop/auth"/>}

                    {this.state.isAuthenticated && this.state.isModerator ?
                        <Route exact path="/shop/orders/" render={(props) => <ShopAdminPanel
                            token={this.state.token}
                            user={this.state.user}
                            {...props}
                        />}/> :
                        <Redirect to="/shop/auth"/>}


                </Switch>
            </div>
        );
    }
}

export default ShopAdminWrapper;