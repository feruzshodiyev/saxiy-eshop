import React, {Component} from 'react';

import axios from 'axios';
import {ACCESS_TOKEN, ACCESS_TOKEN_SHOP, API_BASE_URL} from "../../constants";
import {Avatar, Button, Icon, Input, Modal, notification, Popconfirm, Table, Tag, Tooltip} from "antd";
import NumberFormat from "react-number-format";
import OrderModal from "../../components/OrderModal";

import "../admin/Orders.scss"
import moment from "moment";
import Highlighter from "react-highlight-words";

const ButtonGroup = Button.Group;

class OrdersShop extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orders: [],
            modalVisible: false,
            viewProducts: [],
            loading: false,

            filteredInfo: {},
            sortedInfo: null,

            searchText: '',
            searchedColumn: '',
        }
    }

    openModal = (products) => {
        this.setState({
            modalVisible: true,
            viewProducts: products
        })
    };
    closeModal = () => {
        this.setState({
            modalVisible: false
        })
    };

    componentDidMount() {
        this.fetchOrders();
    }

    fetchOrders = () => {
        this.setState({loading: true});
        axios.get(API_BASE_URL + "order/shop", {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem(ACCESS_TOKEN_SHOP),
            }
        }).then(res => {
            console.log(res);
            this.setState({
                orders: res.data,
                loading: false
            })
        }).catch(err => {
            console.log(err);
            this.setState({loading: false})
        })
    };

    cancelOreder = (id) => {
        axios.put(API_BASE_URL + "order/cancel/" + id).then(res => {
            notification.success({
                message: "Буюртма бекор қилинди!"
            });
            this.fetchOrders();
        }).catch(err=>{
            notification.error({
                message: "Хатолик юз берди!"
            })
        })
    };

    completeOreder = (id) => {
        axios.put(API_BASE_URL + "order/complete/" + id).then(res => {
            notification.success({
                message: "Буюртма бажарилди!"
            });
            this.fetchOrders();
        }).catch(err=>{
            notification.error({
                message: "Хатолик юз берди!"
            })
        })
    };

    deleteOrder = (id) => {
        axios.delete(API_BASE_URL + "order/delete/" + id).then(res => {
            notification.success({
                message: "Буюртма ўчирилди!"
            });
            this.fetchOrders()
        }).catch(err=>{
            notification.error({
                message: "Хатолик юз берди!"
            })
        })
    };


    checkOrder=(paymentId)=>{
        this.setState({
            loading: true
        })
        axios.get(API_BASE_URL+"payment/payment/status",{
            params:{
                "paymentId" : paymentId
            }
        }).then(res=>{
            this.setState({
                loading: false
            })
            Modal.info({
                title: res.data.error_note,
                content: (
                    <div>
                        <p>Payment-ID: {res.data.payment_id}</p>
                        <p>Payment-Status: {res.data.payment_status}</p>
                    </div>
                ),
                onOk() {},
            });
        }).catch(err=>{
            this.setState({
                loading: false
            })
            notification.error({
                message:"Маълумот келмади!"
            })
        })
    };



    handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter);
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    clearFilters = () => {
        this.setState({ filteredInfo: null });
    };

    clearAll = () => {
        this.setState({
            filteredInfo: null,
            sortedInfo: null,
        });
    };



    getColumnSearchProps = (dataIndex, dataItem,searchName) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`${searchName} бўйича излаш`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex, dataItem)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex, dataItem)}
                    icon="search"
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Излаш
                </Button>
                <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                    Тозалаш
                </Button>
            </div>
        ),
        filterIcon: filtered => (
            <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
        ),
        onFilter: (value, record) =>
            record[dataIndex][dataItem]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: (text,record) =>
            this.state.searchedColumn === dataIndex+dataItem ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={record[dataIndex][dataItem].toString()}
                />
            ) : (
                record[dataIndex][dataItem]
            ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex, dataItem) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex+dataItem,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: ''});
    };




    render() {

        let { sortedInfo, filteredInfo } = this.state;
        sortedInfo = sortedInfo || {};
        filteredInfo = filteredInfo || {};

        const columns = [
            // {
            //     title: 'id',
            //     dataIndex: 'id',
            //     key: 'id'
            // },
            {
                title: 'Жами нархи',
                dataIndex: 'totalCost',
                key: 'totalCost',
                render: (id, record) => (
                    <NumberFormat style={{marginLeft: 10}} value={record.totalCost.totalCost} displayType={'text'}
                                  thousandSeparator={' '} suffix={" Сум"}/>
                )
            },
            {
                title: 'Доставка нархи',
                dataIndex: 'totalDiscountCost',
                key: 'totalDiscountCost',
                render: (id, record) => (
                    <NumberFormat style={{marginLeft: 10}} value={record.deliveryCost}
                                  displayType={'text'}
                                  thousandSeparator={' '} suffix={" Сум"}/>
                )
            },
            {
                title: 'Тўлов тури',
                dataIndex: 'paymentType',
                key: 'paymentType',
                render: (id, record) => (
                    record.paymentType === 0 ? <p>Нақд</p> : <p>Click (id: <Tooltip title={"Тўловни текшириш"}><Button
                        onClick={() => this.checkOrder(record.paymentId)} type={"link"}
                        size={"small"}>{record.paymentId}</Button></Tooltip>)</p>
                )

            },
            {
                title: 'Санаси',
                dataIndex: 'createdDate',
                key: 'createdDate',
                render: (createdDate, record) => (
                    <p>
                        {moment(createdDate).format("DD.MM.YYYY HH:mm")}
                    </p>

                ),
            },
            {
                title: 'Дўкон',
                dataIndex: 'market',
                key: 'market'
            },
            {
                title: 'Сотувчи',
                children: [
                    {
                        title: 'Исми',
                        dataIndex: 'seller',
                        key: 'fullName',
                        render: (seller, record) => (
                            <p>{seller.fullName}</p>
                        )

                    },
                    {
                        title: 'Телефон рақами',
                        dataIndex: 'seller',
                        key: 'phoneNumber',
                        render: (seller, record) => (
                            <p>{seller.phoneNumber}</p>
                        )

                    }
                ]
            },
            {
                title: 'Харидор',
                children: [
                    {
                        title: 'Исми',
                        dataIndex: 'buyer',
                        key: 'buyerfullName',
                        render: (buyer, record) => (
                            <p>{buyer.fullName}</p>
                        ),
                        ...this.getColumnSearchProps('buyer', "fullName", "Исми"),

                    },
                    {
                        title: 'Телефон рақами',
                        dataIndex: 'buyer',
                        key: 'buyerphoneNumber',
                        render: (buyer, record) => (
                            <p>{buyer.phoneNumber}</p>
                        ),
                        ...this.getColumnSearchProps('buyer', "phoneNumber", "Рақами"),

                    }
                ]
            },

            {
                title: 'Статус',
                dataIndex: 'status',
                key: 'status',
                filters:[
                    { text: 'OPEN', value: 'OPEN' },
                    { text: 'CANCELLED', value: 'CANCELLED' },
                    { text: 'COMPLETED', value: 'COMPLETED' },
                ],
                filteredValue: filteredInfo.status || null,
                onFilter: (value, record) => record.status.includes(value),
                render: (status, record) => (
                    <Tag color={status === "OPEN" ? "green" : "red"}>
                        {status}
                    </Tag>

                ),
            },
            {
                title: 'Операция',
                key: 'operation',
                dataIndex: 'id',
                width: 150,
                fixed: 'right',
                render: (id, record) => (
                    <div>
                        <ButtonGroup>
                            <Button onClick={() => this.openModal(record.orderedProductsDto)} icon="eye"/>

                            {
                                record.status === "OPEN" ?
                                    <Popconfirm placement="top"
                                                title="Буюртмани бажарилдими?"
                                                onConfirm={() => this.completeOreder(id)} okText="Ҳа" cancelText="Йўқ">
                                        <Button style={{color: "#1fff5e"}}
                                            icon="check-circle"/>
                                    </Popconfirm> : ""
                            }

                            {
                                record.status === "OPEN" ?
                                    <Popconfirm placement="top"
                                                title="Буюртмани бекор қиласизми?"
                                                onConfirm={() => this.cancelOreder(id)} okText="Ҳа" cancelText="Йўқ">
                                        <Button
                                            type="primary" icon="close-circle"/>
                                    </Popconfirm> : ""
                            }


                            {/*<Popconfirm placement="top"*/}
                            {/*            title="Буюртмани ўчирасизми?"*/}
                            {/*            onConfirm={() => this.deleteOrder(id)} okText="Ҳа" cancelText="Йўқ">*/}
                            {/*    <Button*/}
                            {/*        type="danger" icon="delete"/>*/}
                            {/*</Popconfirm>*/}
                        </ButtonGroup>
                    </div>

                ),
            }
        ];
        return (
            <div>
                <Table
                    rowClassName={record => record.status === "OPEN" ? "status-open":record.status === "COMPLETED" ?"status-completed":"status-canceled"}
                    style={{minHeight: 350}}
                    loading={this.state.loading}
                    dataSource={this.state.orders}
                    columns={columns}
                    onChange={this.handleChange}
                    rowKey="id"
                    bordered
                    size="small"
                    scroll={{x: 'calc(900px + 60%)', y: 400}}/>
                <OrderModal
                    visible={this.state.modalVisible}
                    handleClose={this.closeModal}
                    products={this.state.viewProducts}
                />
            </div>
        );
    }
}

export default OrdersShop;