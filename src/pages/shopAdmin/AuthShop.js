import React, {Component} from 'react';
import {Form, Icon, Input, Button, Checkbox, Row, Col} from 'antd';

import "./AuthShopStyles.scss"
import MaskedInput from "antd-mask-input";

class AuthShop extends Component {

    constructor(props) {
        super(props);
        this.state = {
            values: {}
        }
    }


    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({
                    values:{
                        ...values,
                        'username': values.username.replace("(", "").replace(")","").replace(/\s/g, "")
                    }
                },()=>{

                    this.props.handleLogin(this.state.values)
                });
            }
        });
    };


    render() {
        const {loading} = this.props;
        const {getFieldDecorator} = this.props.form;
        return (
            <div>
                <div style={{backgroundImage: `url(http://tanti.uz:6526/api/v1/downloadFile/203791logo.jpg)`}} className="logo-auth"/>
                <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                        {getFieldDecorator('username', {
                            rules: [{required: true, message: 'Телефон рақамини киритинг!'}],
                        })(
                            <MaskedInput
                                mask="(11) 111 11 11"
                                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{required: true, message: 'Парол киритинг!'}],
                        })(
                            <Input
                                prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                type="password"
                                placeholder="Парол"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item style={{display: "none"}}>
                        {getFieldDecorator('grant_type', {
                            initialValue: "password",
                            rules: [{required: true}],
                        })(
                            <Input/>,
                        )}
                    </Form.Item>
                    <Form.Item>

                        <Button loading={loading} type="primary" htmlType="submit" className="login-form-button">
                            Кириш
                        </Button>

                    </Form.Item>
                </Form>
            </div>
        );
    }
}

const AntLoginFormShop = Form.create()(AuthShop);


export default AntLoginFormShop;