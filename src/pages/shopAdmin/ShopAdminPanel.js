import React, {Component} from 'react';
import {Avatar, Dropdown, Icon, Layout, Menu} from "antd";
import {NavLink, Switch, Route} from "react-router-dom";
import Products from "./Products";
import Offers from  "./Offers";
import "../admin/AdmiPanelStyles.scss";
import {ACCESS_TOKEN_SHOP} from "../../constants";
import OrdersShop from "./OrdersShop";



const {Header, Content, Footer, Sider} = Layout;
const {SubMenu} = Menu;

class ShopAdminPanel extends Component {

    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    handleLogout=()=>{
        localStorage.removeItem(ACCESS_TOKEN_SHOP);
        window.location.reload();
    };

    render() {
        const menu = (
            <Menu>
                <Menu.Item key="1">
                    <NavLink to="/admin/profile">
                        {this.props.user}
                    </NavLink>
                </Menu.Item>
                <Menu.Divider/>
                <Menu.Item key="2" onClick={this.handleLogout}><Icon type="logout"/>Выйти</Menu.Item>
            </Menu>
        );
        return (
            <Layout
                style={{minHeight: '100vh'}}
            >
                <Sider
                    theme={"light"}
                    trigger={null}
                    collapsible
                    collapsed={this.state.collapsed}>
                    <div style={{
                        display: this.state.collapsed ? "none" : "block",
                        backgroundImage: 'url(http://tanti.uz:6526/api/v1/downloadFile/203791logo.jpg)',
                        backgroundPositionX:'50%'
                    }} className="logo"/>
                    <div style={{
                        display: this.state.collapsed ? "block" : "none",
                        backgroundImage: 'url(http://tanti.uz:6526/api/v1/downloadFile/118966kvadrat.jpg)'
                    }} className="logo"/>
                    <Menu mode="inline" defaultSelectedKeys={['shop']}>

                        <Menu.Item
                            key="shop">
                            <NavLink to="/shop">
                                <span>
                                    <Icon type="tag"/>
                                    <span>Товарлар</span>
                                </span></NavLink>
                        </Menu.Item>
                        <Menu.Item
                            key="offers">
                            <NavLink to="/shop/orders/">
                                <span>
                                    <Icon type="file-text"/>
                                    <span>Буюртмалар</span>
                                </span>
                            </NavLink>
                        </Menu.Item>

                    </Menu>
                </Sider>
                <Layout
                    style={{
                        height: '100vh',
                        overflowY: 'hidden'
                    }}
                >
                    <Header className="panel-header" style={{background: '#fff', padding: 0, zIndex: 1, width: '100%'}}>
                        <Icon
                            className="trigger"
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle}
                        />

                        <Dropdown
                            className="avatar"
                            placement="bottomLeft"
                            overlayStyle={{width: "200px"}}
                            overlay={menu}
                            trigger={['click']}>
                            <Avatar icon="user" style={{backgroundColor: '#87d068'}} size={"large"}/>
                        </Dropdown>


                    </Header>
                    <Content
                        style={{
                            // overflowY:"auto",
                            height: "100%",
                            margin: '16px 16px 10px 16px',
                            padding: 20,
                            background: '#fff',
                        }}
                    >
                        <div>
                            <Switch>
                                {/*<Redirect to="/admin/shops"/>*/}
                                <Route exact path="/shop/" render={() => <Products/>}/>
                                <Route path="/shop/orders/" render={() => <OrdersShop/>}/>
                            </Switch>
                        </div>

                    </Content>
                </Layout>
            </Layout>
        );
    }
}

export default ShopAdminPanel;