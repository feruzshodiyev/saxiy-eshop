import React, {Component} from 'react';

import {Layout, Form, Input, Icon, Button, Row, Col} from 'antd';
import MaskedInput from "antd-mask-input";

import "./SignIn.scss";
import {LANGUAGE} from "../../constants";

import {Link} from 'react-router-dom';

const {Content} = Layout;

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            values: {}
        }
    }


    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({
                    values: {
                        ...values,
                        'username': values.username.replace("(", "").replace(")", "").replace(/\s/g, "")
                    }
                }, () => {

                    this.props.handleLogin(this.state.values)
                });
            }
        });
    };

    render() {
        const lang = localStorage.getItem(LANGUAGE);

        const {loading} = this.props;
        const {getFieldDecorator} = this.props.form;

        return (

            <Layout className="sign-in-layout">
                <Content className="sign-in-content">
                    <h2 className={"sign-in-title"}>{lang === "ru" ? "Авторизация" : "Kirish"}</h2>
                    <Form onSubmit={this.handleSubmit} className="sign-in-form">
                        <Form.Item label={lang === "ru" ? "Введите номер телефона" : "Telefon raqamini kiriting"}>
                            {getFieldDecorator('username', {
                                rules: [{
                                    required: true,
                                    message: lang === "ru" ? "Введите номер телефона!" : "Telefon raqamini kiriting!"
                                }],
                            })(
                                <MaskedInput
                                    mask="(11) 111 11 11"
                                    prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                />,
                            )}
                        </Form.Item>
                        <Form.Item label={lang === "ru" ? "Введите пароль" : "Parol kiriting"}>
                            {getFieldDecorator('password', {
                                rules: [{required: true, message: lang === "ru" ? "Введите пароль" : "Parol kiriting"}],
                            })(
                                <Input
                                    prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                    type="password"
                                    placeholder={lang === "ru" ? "Пароль" : "Parol"}
                                />,
                            )}
                        </Form.Item>
                        <Form.Item style={{display: "none"}}>
                            {getFieldDecorator('grant_type', {
                                initialValue: "password",
                                rules: [{required: true}],
                            })(
                                <Input/>,
                            )}
                        </Form.Item>
                        <Form.Item>

                            <Button loading={loading} type="primary" htmlType="submit" className="login-form-button">
                                {lang === "ru" ? "Отправить" : "Yuborish"}
                            </Button>
                        </Form.Item>
                        <Row>
                            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                                <Button type={"link"} block>
                                    <Link to={"/sign-up"}>
                                        {lang === "ru" ? "Зарегистрироваться" : "Ro'yxatdan o'tish"}
                                    </Link>
                                </Button>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                                <Button type={"link"} block>
                                    <Link to={"/forgot-password"}>
                                        {lang === "ru" ? "Забыли пароль?" : "Parolni unutdingizmi?"}
                                    </Link>
                                </Button>
                            </Col>
                        </Row>
                    </Form>

                </Content>
            </Layout>

        );
    }
}

const SignInForm = Form.create({name: 'normal_login'})(SignIn);


export default SignInForm;






