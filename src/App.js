import React from 'react';
import {withTranslation, Trans} from 'react-i18next';
import {Redirect, BrowserRouter as Router, HashRouter, Route, Switch, Link} from 'react-router-dom';

import './App.css';
import {Button} from "antd";
import WrapAdmin from "./pages/admin/WrapAdmin";
import Home from "./pages/main/Home";
import ShopAdminWrapper from "./pages/shopAdmin/ShopAdminWrapper";
import Wrapper from "./pages/main/Wrapper";
import CartPage from "./pages/main/CartPage";

function App() {
    return (
        <Router>
            <div className="App">
                <Switch>

                    <Route exact path="/" render={(props) => <Wrapper
                        {...props}
                    />}/>
                    <Route  path="/cart" render={(props) => <Wrapper
                        {...props}
                    />}/>
                    <Route  path="/sign-in" render={(props) => <Wrapper
                        {...props}
                    />}/>
                    <Route  path="/sign-up" render={(props) => <Wrapper
                        {...props}
                    />}/>
                    <Route  path="/forgot-password" render={(props) => <Wrapper
                        {...props}
                    />}/>
                    <Route  path="/my-orders" render={(props) => <Wrapper
                        {...props}
                    />}/>
                    <Route  path="/my-profile" render={(props) => <Wrapper
                        {...props}
                    />}/>
                    <Route  path="/category/:id" render={(props) => <Wrapper
                        {...props}
                    />}/>
                    <Route  path="/p-category/:id" render={(props) => <Wrapper
                        {...props}
                    />}/>
                    <Route  path="/product/:id" render={(props) => <Wrapper
                        {...props}
                    />}/>
                    <Route  path="/about-us" render={(props) => <Wrapper
                        {...props}
                    />}/>
                    <Route  path="/using-agreement" render={(props) => <Wrapper
                        {...props}
                    />}/>


                    {/*<Route exact path="/" render={ (props)=> <Redirect to={"/admin"}/>}/>*/}

                    <Route path="/admin" render={(props) =>
                        <WrapAdmin
                            {...props}
                        />
                    }/>
                    <Route path="/shop" render={(props) =>
                        <ShopAdminWrapper
                            {...props}
                        />
                    }/>


                </Switch>
            </div>
        </Router>

    );
}

export default withTranslation('common')(App);
