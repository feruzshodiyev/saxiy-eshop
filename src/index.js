import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from "react-router-dom";
import {I18nextProvider} from 'react-i18next';
import i18next from 'i18next';

import common_uz from './pages/main/translations/uz/common.json'
import common_ru from './pages/main/translations/ru/common.json'

import cartReducer from './pages/main/redux/reducers/cartReducer';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

i18next.init({
    interpolation: { escapeValue: false },  // React already does escaping
    lng: 'uz',                              // language to use
    resources: {
        uz: {
            common: common_uz             // 'common' is our custom namespace
        },
        ru: {
            common: common_ru
        },
    },
});

const store = createStore(cartReducer);

ReactDOM.render(
    <BrowserRouter>
        <I18nextProvider i18n={i18next}>
            <Provider store={store}>
        <App/>
            </Provider>
        </I18nextProvider>
    </BrowserRouter>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
